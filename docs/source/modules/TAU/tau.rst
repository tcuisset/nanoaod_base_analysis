TAU Corrections
===============

.. currentmodule:: Corrections.TAU.tauCorrections

.. contents::

.. _Tau_tauSFRDF:

``tauSFRDF``
------------

.. autoclass:: tauSFRDF
   :members:

