electron SF
===========

.. currentmodule:: Corrections.EGM.eleCorrections

.. contents::

.. _Electron_eleSFRDF:

``eleSFRDF``
------------

.. autoclass:: eleSFRDF
   :members:

