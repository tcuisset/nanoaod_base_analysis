muon SF using GEMethod
======================

.. currentmodule:: Corrections.MUO.muonSF_GEMethod

.. contents::

.. _Muon_GEM_muSFRDF:

``MuonSF``
------------------------------

.. autoclass:: MuonSF
   :members:

