# coding: utf-8

"""
Preprocessing tasks.
"""

__all__ = []


import abc
import contextlib
import itertools
from collections import OrderedDict, defaultdict
import os
import sys
from subprocess import call
import json
import math
import gc

import law
from law.contrib.root import GuardedTFile
from law.util import flatten
import luigi
import numpy as np

from analysis_tools.utils import import_root, create_file_dir, DotDict, join_root_selection as jrs
from analysis_tools import Dataset

from cmt.base_tasks.base import (
    DatasetTaskWithCategory, DatasetWrapperTask, HTCondorWorkflow, SGEWorkflow, SlurmWorkflow,
    InputData, ConfigTaskWithCategory, SplittedTask, DatasetTask, RDFModuleTask, get_n_files_after_merging
)
from cmt.util import systematic_names_to_variations, chunk_list

directions = ["up", "down"]


class DatasetSuperWrapperTask(DatasetWrapperTask, law.WrapperTask):

    exclude_index = True

    def __init__(self, *args, **kwargs):
        super(DatasetSuperWrapperTask, self).__init__(*args, **kwargs)

    @abc.abstractmethod
    def atomic_requires(self, dataset):
        return None

    def requires(self):
        return OrderedDict(
            (dataset.name, self.atomic_requires(dataset))
            for dataset in self.datasets
        )


class DatasetSystWrapperTask(DatasetSuperWrapperTask):
    systematic_names = law.CSVParameter(default=("central",), description="names of systematics "
        "to run, default: central only (empty string)")

    exclude_index = True

    @abc.abstractmethod
    def atomic_requires(self, dataset, systematic, direction):
        return None

    def requires(self):
        return OrderedDict(
            ((dataset.name, syst, d),
                self.atomic_requires(dataset, syst, d))
            for dataset, (syst, d) in itertools.product(self.datasets, systematic_names_to_variations(self.systematic_names))
        )


class DatasetCategoryWrapperTask(DatasetWrapperTask, law.WrapperTask):
    category_names = law.CSVParameter(default=("baseline_even",), description="names of categories "
        "to run, default: (baseline_even,)")

    exclude_index = True

    def __init__(self, *args, **kwargs):
        super(DatasetCategoryWrapperTask, self).__init__(*args, **kwargs)

        # tasks wrapped by this class do not allow composite categories, so split them here
        self.categories = []
        for name in self.category_names:
            category = self.config.categories.get(name)
            if category.subcategories:
                self.categories.extend(category.subcategories)
            else:
                self.categories.append(category)

    @abc.abstractmethod
    def atomic_requires(self, dataset, category):
        return None

    def requires(self):
        return OrderedDict(
            ((dataset.name, category.name), self.atomic_requires(dataset, category))
            for dataset, category in itertools.product(self.datasets, self.categories)
            if not dataset.process.name in category.get_aux("skip_processes", [])
        )


class DatasetCategoryWrapperTask(DatasetWrapperTask):
    category_names = law.CSVParameter(default=("baseline_even",), description="names of categories "
        "to run, default: (baseline_even,)")

    exclude_index = True

    def __init__(self, *args, **kwargs):
        super(DatasetCategoryWrapperTask, self).__init__(*args, **kwargs)

        # tasks wrapped by this class do not allow composite categories, so split them here
        self.categories = []
        for name in self.category_names:
            category = self.config.categories.get(name)
            if category.subcategories:
                self.categories.extend(category.subcategories)
            else:
                self.categories.append(category)

    @abc.abstractmethod
    def atomic_requires(self, dataset, category):
        return None

    def requires(self):
        return OrderedDict(
            ((dataset.name, category.name), self.atomic_requires(dataset, category))
            for dataset, category in itertools.product(self.datasets, self.categories)
            if not dataset.process.name in category.get_aux("skip_processes", [])
        )


class DatasetCategorySystWrapperTask(DatasetCategoryWrapperTask, law.WrapperTask):
    systematic_names = law.CSVParameter(default=("central",), description="names of systematics "
        "to run, default: central only (empty string)")

    exclude_index = True

    @abc.abstractmethod
    def atomic_requires(self, dataset, category, systematic, direction):
        return None

    def requires(self):
        return OrderedDict(
            ((dataset.name, category.name, syst, d),
                self.atomic_requires(dataset, category, syst, d))
            for dataset, category, (syst, d) in itertools.product(
                self.datasets, self.categories, systematic_names_to_variations(self.systematic_names))
                if not dataset.process.name in category.get_aux("skip_processes", [])
        )


def preprocess_branch_map(config_name:str, dataset:Dataset):
    """
    :return: number of files for the selected dataset
    :rtype: int
    """
    threshold = dataset.get_aux("event_threshold", None)
    merging_factor = dataset.get_aux("preprocess_merging_factor", None)
    if not threshold and not merging_factor:
        return len(dataset.get_files(
            os.path.expandvars("$CMT_TMP_DIR/%s/" % config_name), add_prefix=False,
            check_empty=False))
    elif threshold and not merging_factor:
        return len(dataset.get_file_groups(
            path_to_look=os.path.expandvars("$CMT_TMP_DIR/%s/" % config_name),
            threshold=threshold))
    elif not threshold and merging_factor:
        nfiles = len(dataset.get_files(
            os.path.expandvars("$CMT_TMP_DIR/%s/" % config_name), add_prefix=False,
            check_empty=False))
        nbranches = nfiles // dataset.get_aux("preprocess_merging_factor")
        if nfiles % dataset.get_aux("preprocess_merging_factor"):
            nbranches += 1
        return nbranches
    else:
        raise ValueError("Both event_threshold and preprocess_merging_factor "
            "can't be set at once")

class InputDataTask:
    """ Task that uses as input NanoAOD made outside the framework
    Dataset settings: 
     - preprocess_merging_factor : if set, will run one task using N input data files (with TChain)
     - event_threshold : if set, will split each input data files into different tasks, with N events maximum per task
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.threshold = self.dataset.get_aux("event_threshold", None)
        self.merging_factor = self.dataset.get_aux("preprocess_merging_factor", None)

    def workflow_requires(self):
        return {"data": InputData.req(self)}

    def requires(self):
        """
        Each branch requires one input file
        """
        if not self.threshold and not self.merging_factor:
            return InputData.req(self, file_index=self.branch)
        elif self.threshold and not self.merging_factor:
            reqs = {}
            for i in self.dataset.get_file_groups(
                    path_to_look=os.path.expandvars("$CMT_TMP_DIR/%s/" % self.config_name),
                    threshold=self.threshold)[self.branch]:
                reqs[str(i)] = InputData.req(self, file_index=i)
            return reqs
        elif not self.threshold and self.merging_factor:
            nfiles = len(self.dataset.get_files(
                os.path.expandvars("$CMT_TMP_DIR/%s/" % self.config_name), add_prefix=False,
                check_empty=False))
            reqs = {}
            for i in range(self.merging_factor * self.branch, self.merging_factor * (self.branch + 1)):
                if i >= nfiles:
                    break
                reqs[str(i)] = InputData.req(self, file_index=i)
            return reqs
        else:
            raise ValueError("Both event_threshold and preprocess_merging_factor "
                "can't be set at once")
    
    def create_branch_map(self):
        """
        :return: number of files for the selected dataset
        :rtype: int
        """
        return preprocess_branch_map(self.config_name, self.dataset)

# note : InputDataTask must be before DatasetTask, so that in the __init__ chain self.dataset gets set in DatasetTask.__init__ before the main body of InputDataTask.__init__
class PreCounter(RDFModuleTask, InputDataTask, DatasetTask, law.LocalWorkflow, HTCondorWorkflow, SGEWorkflow, SlurmWorkflow,
        SplittedTask):
    """
    Performs a counting of the events with and without applying the necessary weights.
    Weights are read from the config file.
    In case they have to be computed, RDF modules can be run.

    Example command:

    ``law run PreCounter --version test  --config-name base_config --dataset-name ggf_sm \
--workflow htcondor --weights-file weight_file``

    :param weights_file: filename inside ``cmt/config/`` (w/o extension) with the RDF modules to run
    :type weights_file: str

    :param systematic: systematic to use for categorization.
    :type systematic: str

    :param systematic_direction: systematic direction to use for categorization.
    :type systematic_direction: str
    """

    weights_file = luigi.Parameter(description="filename with modules to run RDataFrame",
        default=law.NO_STR)
    systematic = luigi.Parameter(default="central", description="systematic to use for categorization, "
        "default: central")
    systematic_direction = luigi.ChoiceParameter(default="", choices=("central", "up", "down"), 
        description="systematic direction to use for categorization, default: central")

    # regions not supported
    region_name = None

    default_store = "$CMT_STORE_EOS_CATEGORIZATION"
    default_wlcg_fs = "wlcg_fs_categorization"

    def __init__(self, *args, **kwargs):
        super(PreCounter, self).__init__(*args, **kwargs)
        self.addendum = self.get_addendum()
        self.custom_output_tag = "_%s" % self.addendum

    def get_addendum(self):
        if self.systematic != "central":
            weights = self.config.weights.total_events_weights
            for weight in weights:
                try:
                    feature = self.config.features.get(weight)
                    if self.systematic in feature.systematics:
                        return f"{self.systematic}_{self.systematic_direction}_"
                except:
                    continue
        return ""

    def output(self):
        """
        :return: One file per input file
        :rtype: `.json`
        """
        return self.local_target(f"data_{self.addendum}{self.branch}.json")

    def get_weight(self, weights, syst_name, syst_direction, **kwargs):
        """
        Obtains the product of all weights depending on the category/channel applied.
        Returns "1" if it's a data sample.

        :return: Product of all weights to be applied
        :rtype: str
        """
        if self.config.processes.get(self.dataset.process.name).isData:
            return "1"
        return self.config.get_weights_expression(weights, syst_name, syst_direction)

    @law.decorator.notify
    @law.decorator.localize(input=False)
    def run(self):
        """
        Creates one RDataFrame per input file, runs the desired RDFModules
        and counts the number of events w/ and w/o additional weights
        """

        from shutil import copy
        ROOT = import_root()
        ROOT.ROOT.EnableImplicitMT(self.request_cpus)

        # create RDataFrame
        inp = self.get_input()
        if not self.dataset.friend_datasets:
            df = self.RDataFrame(self.tree_name, self.get_path(inp),
                allow_redefinition=self.allow_redefinition)

        # friend tree
        else:
            tchain = ROOT.TChain()
            for elem in self.get_path(inp):
                tchain.Add("{}/{}".format(elem, self.tree_name))
            friend_tchain = ROOT.TChain()
            for elem in self.get_path(inp, 1):
                friend_tchain.Add("{}/{}".format(elem, self.tree_name))
            tchain.AddFriend(friend_tchain, "friend")
            df = self.RDataFrame(tchain, allow_redefinition=self.allow_redefinition)

        # Dataset selection for PreCounter. Not present in original NBA framework
        # Code was added but then commented out as we decided not to use it
        # dataset_selection = self.dataset.get_aux("selection")
        # if dataset_selection is not None and dataset_selection != "":
        #     df = df.Define("dataset_selection", dataset_selection).Filter("dataset_selection", "dataset_selection")
        # else:
        #     df = df

        weight_modules = self.get_feature_modules(self.weights_file)
        if len(weight_modules) > 0:
            for module in weight_modules:
                df, _ = module.run(df)

        weight = self.get_weight(
            self.config.weights.total_events_weights, self.systematic, self.systematic_direction)

        hmodel = ("", "", 1, 1, 2)
        histo_noweight = df.Define("var", "1.").Histo1D(hmodel, "var")
        if not self.dataset.process.isData:
            histo_weight = df.Define("var", "1.").Define("weight", weight).Histo1D(
                hmodel, "var", "weight")
        else:
            histo_weight = df.Define("var", "1.").Histo1D(hmodel, "var")

        d = {
            "nevents": histo_noweight.Integral(),
            "nweightedevents": histo_weight.Integral(),
            "filenames": [str(self.get_path(inp))]
        }

        with open(create_file_dir(self.output().path), "w+") as f:
            json.dump(d, f, indent=4)


class PreCounterWrapper(DatasetSystWrapperTask):
    """
    Wrapper task to run the PreCounter task over several datasets in parallel.

    Example command:

    ``law run PreCounterWrapper --version test  --config-name base_config \
--dataset-names tt_dl,tt_sl --PreCounter-weights-file weight_file --workers 2``
    """
    def atomic_requires(self, dataset, systematic, direction):
        return PreCounter.req(self, dataset_name=dataset.name,
            systematic=systematic, systematic_direction=direction)


class PreprocessRDF(RDFModuleTask, InputDataTask, DatasetTaskWithCategory,
        law.LocalWorkflow, HTCondorWorkflow, SGEWorkflow, SlurmWorkflow, SplittedTask):
    """
    Performs the preprocessing step applying a preselection + running RDF modules

    See requirements in :class:`.PreCounter`.

    Example command:

    ``law run PreprocessRDF --version test  --category-name base_selection \
--config-name base_config --dataset-name ggf_sm --workflow htcondor \
--modules-file modulesrdf --workers 10 --max-runtime 12h``

    :param modules_file: filename inside ``cmt/config/`` or "../config/" (w/o extension)
        with the RDF modules to run
    :type modules_file: str

    :param keep_and_drop_file: filename inside ``cmt/config/`` or "../config/" (w/o extension)
        with the RDF columns to save in the output file
    :type keep_and_drop_file: str

    :param compute_filter_efficiency: compute efficiency of each filter applied
    :type compute_filter_efficiency: bool
    """

    modules_file = luigi.Parameter(description="filename with RDF modules", default=law.NO_STR)
    keep_and_drop_file = luigi.Parameter(description="filename with branches to save, empty: all",
        default="")
    compute_filter_efficiency = luigi.BoolParameter(description="compute efficiency of each filter "
        "applied, default: False", default=False)
    
    # using empty strings in CSVParameters is a bad idea, as serializing-then-parsing makes the empty string disappear (ie. (("central", ""), ("jec_4", "up")) -> central,:jec_4,up -> (('central',), ('jec_4', 'up')))
    systematic_variations = law.MultiCSVParameter(default=(("central", "central"),), description="systematics+directions to run")

    default_store = "$CMT_STORE_EOS_PREPROCESSING"
    default_wlcg_fs = "wlcg_fs_categorization"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if len(self.systematic_variations) == 0 or any(len(y) == 0 for x in self.systematic_variations for y in x):
            raise ValueError("PreprocessRDF : invalid format of systematic_variations : " + str(self.systematic_variations))
        if not self.dataset.process.isMC and self.systematic_variations != (("central", "central"),):
            raise ValueError("PreprocessRDF : trying to run systematic variations on data ! Systematics : " + str(self.systematic_variations))
        try:
            self.custom_output_tag = "_"+"_".join(f"{syst}_{syst_dir}" for syst,syst_dir in self.systematic_variations[:5])
        except ValueError as e:
            raise ValueError("Error whilst considering PreprocessRDF systematic_variations : " + str(self.systematic_variations)) from e
        if len(self.systematic_variations) > 5: # we can run into file name limit of 255 characters
            self.custom_output_tag += "_etc"

    def output(self):
        """
        Output structure : SiblingFileCollection(dict
        syst_name -> syst_direction -> {
            "root" -> file
            "cutflow" -> file
        }
        The central is in ["central"]["central"]
        """
        out = {}
        for syst_name, syst_dir in self.systematic_variations:
            if syst_name == "central":
                addendum = ""
            else:
                addendum = f"{syst_name}_{syst_dir}_"
            out.setdefault(syst_name, dict())[syst_dir] = {"root" : self.local_target(f"data_{addendum}{self.branch}.root")}
            #out = {"root" : self.dynamic_target(f"data_{self.addendum}{self.branch}.root")}
            if self.compute_filter_efficiency:
                out[syst_name][syst_dir]["cut_flow"] = self.local_target(f"cutflow_{addendum}{self.branch}.json")
        return law.SiblingFileCollection(out)
    
    use_workflow_output_in_proxy = True
    def workflow_output(self):
        """ Performance improvement to avoid instantiating all the branches """
        out = {i : dict() for i in self.branch_map}
        for syst_name, syst_dir in self.systematic_variations:
            if syst_name == "central":
                addendum = ""
            else:
                addendum = f"{syst_name}_{syst_dir}_"
            for branch in self.branch_map:

                out[branch].setdefault(syst_name, dict())[syst_dir] = {"root" : self.local_target(f"data_{addendum}{branch}.root")}
                #out = {"root" : self.dynamic_target(f"data_{self.addendum}{self.branch}.root")}
                if self.compute_filter_efficiency:
                    out[branch][syst_name][syst_dir]["cut_flow"] = self.local_target(f"cutflow_{addendum}{branch}.json")
            collection = law.SiblingFileCollection(out)
        return DotDict([("collection", collection)])

    def workflow_complete(self):
        """ Performance improvement to avoid instantiating all the branches """
        return self.workflow_output()["collection"].complete()

    @law.decorator.notify
    @law.decorator.localize(input=False)
    def run(self):
        """
        Creates one RDataFrame per input file, applies a preselection
        and runs the desired RDFModules
        """
        if self.no_run: raise RuntimeError("PreprocessRDF.no_run was set to True, yet PreprocessRDF was being run. Cancelling task")
        ROOT = import_root()
        ROOT.ROOT.EnableImplicitMT(self.request_cpus)

        outp = self.output()

        # create RDataFrame
        inp = self.get_input()
        if not self.dataset.friend_datasets:
            df = self.RDataFrame(self.tree_name, self.get_path(inp),
                allow_redefinition=self.allow_redefinition)

        # friend tree
        else:
            tchain = ROOT.TChain()
            for elem in self.get_path(inp):
                tchain.Add("{}/{}".format(elem, self.tree_name))
            friend_tchain = ROOT.TChain()
            for elem in self.get_path(inp, 1):
                friend_tchain.Add("{}/{}".format(elem, self.tree_name))
            tchain.AddFriend(friend_tchain, "friend")
            df = self.RDataFrame(tchain, allow_redefinition=self.allow_redefinition)

        selection = self.category.selection
        if self.dataset.has_tag("hpsData") and self.category.get_aux("selection_HPS") is not None:
            selection = self.category.get_aux("selection_HPS")
        # dataset_selection = self.dataset.get_aux("selection")
        # if dataset_selection and dataset_selection != "1":
            # selection = jrs(dataset_selection, selection, op="and")

        input_branches = list(df.GetColumnNames())

        if selection != "":
            filtered_df = df.Define("selection", selection).Filter("selection", self.category.name)
        else:
            filtered_df = df
        
        snapshot_options = ROOT.RDF.RSnapshotOptions()
        snapshot_options.fLazy = True
        snapshots = {}
        reports = {}
        for syst_name, syst_dir in self.systematic_variations:
            assert syst_dir in ["central", "up", "down"]
            syst_df = filtered_df
            modules = self.get_feature_modules(self.modules_file, syst_name, syst_dir)
            new_branches = []
            if len(modules) > 0:
                for module in modules:
                    # should not use sys.exit here as it causes localize_targets to try to move the file because SystemExit does not derive from Exception or KeyboardInterrupt
                    syst_df, add_branches = module.run(syst_df)
                    new_branches += add_branches
            branches_to_keep = self.get_branches_to_save(input_branches+new_branches, self.keep_and_drop_file, systematic=syst_name, systematic_direction=syst_dir)
            if self.compute_filter_efficiency == True:
                reports[(syst_name, syst_dir)] = syst_df.Report()
            assert (syst_name, syst_dir) not in snapshots
            snapshots[(syst_name, syst_dir)] = syst_df.Snapshot(self.tree_name, create_file_dir(outp[syst_name][syst_dir]["root"].path), tuple(branches_to_keep), snapshot_options)
        
        print("Starting event loop")
        ROOT.RDF.RunGraphs([next(iter(snapshots.values()))])
        # ROOT.RDF.RunGraphs(snapshots.values())
        # running the snapshots
        #next(iter(snapshots.values())).Count().GetValue()

        if self.compute_filter_efficiency:
            for syst_name, syst_dir in self.systematic_variations:
                report = reports[(syst_name, syst_dir)]
                json_res = {cutReport.GetName() : {
                    "pass": cutReport.GetPass(), "all": cutReport.GetAll()}
                    for cutReport in report.GetValue()
                }
                with open(create_file_dir(outp[syst_name][syst_dir]["cut_flow"].path), "w+") as f:
                    json.dump(json_res, f, indent=4)



class PreprocessRDFWrapper(DatasetCategoryWrapperTask, law.WrapperTask):
    """
    Wrapper task to run the PreprocessRDF task over several datasets in parallel.

    Example command:

    ``law run PreprocessRDFWrapper --version test  --category-name base_selection \
--config-name ul_2018 --dataset-names tt_dl,tt_sl --PreprocessRDF-workflow htcondor \
--PreprocessRDF-max-runtime 48h --PreprocessRDF-modules-file modulesrdf  --workers 10``
    """
    systematic_names = law.CSVParameter(default=("central",), description="names of systematics "
        "to run, default: central only")
    split_syst_group = luigi.IntParameter(default=None, description="Instead of running one PreprocessRDF job with all systematics, will make batches of this number of tasks ")

    def atomic_requires(self, dataset, category):
        raise NotImplementedError()

    def requires(self):
        systematic_variations = systematic_names_to_variations(self.systematic_names)
        systematic_variations_batches = list(law.util.iter_chunks(tuple(systematic_variations), self.split_syst_group if self.split_syst_group else 0))
        return {
            (dataset.name, category.name, syst_variations_batch) : PreprocessRDF.vreq(self, dataset_name=dataset.name, category_name=category.name,
                systematic_variations=syst_variations_batch)
            
            for syst_variations_batch in systematic_variations_batches
            for dataset, category in itertools.product(
                self.datasets, self.categories)
            if not dataset.process.name in category.get_aux("skip_processes", [])
        }


class Preprocess(DatasetTaskWithCategory, law.LocalWorkflow, HTCondorWorkflow, SlurmWorkflow, SplittedTask):

    modules = luigi.DictParameter(default=None)
    modules_file = luigi.Parameter(description="filename with modules to run on nanoAOD tools",
        default="")
    max_events = luigi.IntParameter(description="maximum number of input events per file, "
        " -1 for all events", default=50000)
    keep_and_drop_file = luigi.Parameter(description="filename with output branches to "
        "keep and drop", default="$CMT_BASE/cmt/files/keep_and_drop_branches.txt")

    # regions not supported
    region_name = None

    default_store = "$CMT_STORE_EOS_CATEGORIZATION"
    default_wlcg_fs = "wlcg_fs_categorization"

    def __init__(self, *args, **kwargs):
        super(Preprocess, self).__init__(*args, **kwargs)
        if not self.keep_and_drop_file:
            self.keep_and_drop_file = None
        else:
            if "$" in self.keep_and_drop_file:
                self.keep_and_drop_file = os.path.expandvars(self.keep_and_drop_file)
        if self.dataset.get_aux("splitting") and self.max_events != -1:
            self.max_events = self.dataset.get_aux("splitting")
        if self.max_events != -1:
            if not hasattr(self, "splitted_branches") and self.is_workflow():
                self.splitted_branches = self.build_splitted_branches()
            elif not hasattr(self, "splitted_branches"):
                self.splitted_branches = self.get_splitted_branches

    def get_n_events(self, fil):
        ROOT = import_root()
        for trial in range(10):
            try:
                f = ROOT.TFile.Open(fil)
                tree = f.Get(self.tree_name)
                nevents = tree.GetEntries()
                f.Close()
                return nevents
            except:
                print("Failed opening %s, %s/10 trials" % (fil, trial + 1))
        raise RuntimeError("Failed opening %s" % fil)

    def build_splitted_branches(self):
        if self.dataset.get_aux("splitting"):
            self.max_events = self.dataset.get_aux("splitting")
        if not os.path.exists(
                os.path.expandvars("$CMT_TMP_DIR/%s/splitted_branches_%s/%s.json" % (
                    self.config_name, self.max_events, self.dataset.name))):
            ROOT = import_root()
            files = self.dataset.get_files(
                os.path.expandvars("$CMT_TMP_DIR/%s/" % self.config_name), add_prefix=False,
                check_empty=True)
            branches = []
            for ifil, fil in enumerate(files):
                nevents = -1
                fil = self.dataset.get_files(
                    os.path.expandvars("$CMT_TMP_DIR/%s/" % self.config_name), index=ifil)
                print("Analyzing file %s" % fil)
                nevents = self.get_n_events(fil)
                initial_event = 0
                isplit = 0
                while initial_event < nevents:
                    max_events = min(initial_event + self.max_events, int(nevents))
                    branches.append({
                        "filenumber": ifil,
                        "split": isplit,
                        "initial_event": initial_event,
                        "max_events": max_events,
                    })
                    initial_event += self.max_events
                    isplit += 1
            with open(create_file_dir(os.path.expandvars(
                    "$CMT_TMP_DIR/%s/splitted_branches_%s/%s.json" % (
                    self.config_name, self.max_events, self.dataset.name))), "w+") as f:
                json.dump(branches, f, indent=4)
        else:
             with open(create_file_dir(os.path.expandvars(
                    "$CMT_TMP_DIR/%s/splitted_branches_%s/%s.json" % (
                    self.config_name, self.max_events, self.dataset.name)))) as f:
                branches = json.load(f)
        return branches

    @law.workflow_property
    def get_splitted_branches(self):
        return self.splitted_branches

    def create_branch_map(self):
        if self.max_events != -1:
            return len(self.splitted_branches)
        else:
            return len(self.dataset.get_files(
                os.path.expandvars("$CMT_TMP_DIR/%s/" % self.config_name), add_prefix=False))

    def workflow_requires(self):
        return {"data": InputData.req(self)}

    def requires(self):
        if self.max_events == -1:
            return InputData.req(self, file_index=self.branch)
        else:
            return InputData.req(self, file_index=self.splitted_branches[self.branch]["filenumber"])

    def output(self):
        return {"data": self.local_target("data_%s.root" % self.branch),
            "stats": self.local_target("data_%s.json" % self.branch)}
        # return self.local_target("{}".format(self.input()["data"].path.split("/")[-1]))

    def get_modules(self):
        module_params = None
        if self.modules_file:
            import yaml
            from cmt.utils.yaml_utils import ordered_load
            with open(self.retrieve_file("config/{}.yaml".format(self.modules_file))) as f:
                module_params = ordered_load(f, yaml.SafeLoader)
        else:
            return []

        def _args(*_nargs, **_kwargs):
            return _nargs, _kwargs

        modules = []
        if not module_params:
            return modules
        for tag in module_params.keys():
            parameter_str = ""
            assert "name" in module_params[tag] and "path" in module_params[tag]
            name = module_params[tag]["name"]
            if "parameters" in module_params[tag]:
                for param, value in module_params[tag]["parameters"].items():
                    if isinstance(value, str):
                        if "self" in value:
                            value = eval(value)
                    if isinstance(value, str):
                        parameter_str += param + " = '{}', ".format(value)
                    else:
                        parameter_str += param + " = {}, ".format(value)
            mod = module_params[tag]["path"]
            mod = __import__(mod, fromlist=[name])
            nargs, kwargs = eval('_args(%s)' % parameter_str)
            modules.append(getattr(mod, name)(**kwargs)())
        return modules

    @law.decorator.notify
    @law.decorator.localize(input=False)
    def run(self):
        from shutil import move
        from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor
        from analysis_tools.utils import import_root

        ROOT = import_root()

        # prepare inputs and outputs
        # inp = self.input()["data"].path
        inp = self.input().path
        outp = self.output()
        d = {}
        # count events
        if self.max_events == -1:
            d["nevents"] = self.get_n_events(inp)
        else:
            d["nevents"] = (self.splitted_branches[self.branch]["max_events"]
                - self.splitted_branches[self.branch]["initial_event"])

        with open(outp["stats"].path, "w+") as f:
            json.dump(d, f, indent = 4)

        # build the full selection
        selection = self.category.get_aux("nt_selection", self.category.selection)
        # dataset_selection = self.dataset.get_aux("selection")
        # if dataset_selection and dataset_selection != "1":
            # selection = jrs(dataset_selection, selection, op="and")
        # selection = "Jet_pt > 500" # hard-coded to reduce the number of events for testing
        # selection = "(event == 265939)"
        modules = self.get_modules()

        if self.max_events == -1:
            maxEntries = None
            firstEntry = 0
            postfix = ""
            output_file = inp.split("/")[-1]
        else:
            maxEntries = self.max_events
            # maxEntries = 1
            # firstEntry = 228
            firstEntry = self.splitted_branches[self.branch]["initial_event"]
            postfix = "_%s" % self.splitted_branches[self.branch]["split"]
            output_file = ("%s." % postfix).join(inp.split("/")[-1].split("."))

        p = PostProcessor(".", [inp],
                      cut=selection,
                      modules=modules,
                      postfix=postfix,
                      outputbranchsel=self.keep_and_drop_file,
                      maxEntries=maxEntries,
                      firstEntry=firstEntry)
        p.run()
        move(output_file, outp["data"].path)


class PreprocessWrapper(DatasetCategoryWrapperTask):

    def atomic_requires(self, dataset, category):
        return Preprocess.req(self, dataset_name=dataset.name, category_name=category.name)


# outside class as we may want to run it either in workflow or in branch
def get_categorization_addendum(branch_data):
    if branch_data['systematic'] != "central":
        return f"{branch_data['systematic']}_{branch_data['systematic_direction']}_"
    return ""

def get_categorization_branch_addendum(branch_data):
    """ Computes for ex 'jec_1_up_5'  or '6' (for central)"""
    return f"{get_categorization_addendum(branch_data)}{branch_data['reduced_branch_nb']}"


def categorization_branch_map(config_name, dataset, syst_variations, merging_factor):
    """ 
    Returns dict(branch_nb=dict(
        systematic="...", # systematic name
        systematic_direction="up", # direction
        reduced_branch_nb=..., # what the numeric branch number would be with only one systematic, for output file nb. in case no preprocess merging this is the parent branch, otherwise its maximum is lower
        parent_branches=[], # list of branch numbers of parent task (ie PreProcess branch numbers)
        part_single_file=True/False, # if True, then we use a subset of a single file using RDataframe.Range (only for len(parent_branches)==1) 
        initial_event=, # if part_single_file=True, first event to process
        max_events=,
        )
    )
    merging_factor can be None in which case no merging will be performed
    TODO merging_factor should be called n_files_after_merging or something
    """
    branch_datas = []
    
    preproc_n_files = preprocess_branch_map(config_name, dataset)
    for syst_name, syst_dir in syst_variations:
        if merging_factor is not None and merging_factor >= 1:
            if merging_factor > preproc_n_files:
                print(f"## WARNING : Categorization({dataset.name}) : requested {merging_factor} output files, but there are only {preproc_n_files} PreprocessRDF output files. Setting merge_factor to {preproc_n_files}")
                merging_factor = preproc_n_files
            branch_datas.extend(
                dict(systematic=syst_name, systematic_direction=syst_dir, reduced_branch_nb=i, parent_branches=chunk, part_single_file=False)
                for i, chunk in enumerate(chunk_list(range(preproc_n_files), merging_factor)) 
            )
        else:
            branch_datas.extend(
                dict(systematic=syst_name, systematic_direction=syst_dir, reduced_branch_nb=i, parent_branches=[i], part_single_file=False)
                for i in range(preproc_n_files)
            )
    return {i : data for i, data in enumerate(branch_datas)}

def get_categorization_merging_factor(dataset, category):
    return get_n_files_after_merging(dataset, category, dataset_key="categorization_merging", default=0)

class Categorization(RDFModuleTask, DatasetTaskWithCategory,
        law.LocalWorkflow, HTCondorWorkflow, SGEWorkflow, SlurmWorkflow):
    """
    Performs the categorization step running RDF modules and applying a post-selection. Usually run after PreprocessRDF.
    If PreprocessRDF has already been run, it is usually necessary to specify `--PreprocessRDF-version` & `--PreprocessRDF-category-name` to make sure PreprocessRDF is picked up.

    A Categorization workflow is run for one dataset and one category, but possibly multiple systematic variations. One branch task processes only one systematic, but possibly more than one input files.
    In the default configuration, there will thus be (nb of systematic variations)*(nb of preprocess branches) branch tasks. However this can be changed through the `categorization_merging` argument of the dataset object
    `categorization_merging` can be set to a dict(category_name/pattern->nb of Categorization branches per systematic variation) (see `cmt.base_tasks.base.get_n_files_after_merging` for possible values), 
    in which case the value found in config will be used to determine the nb of branch tasks per systematic variation, leasing to (nb of systematic variations)*(value of categorization_merging) branch tasks. TChain is used to process multiple input files
    Ability to have one PreprocessRDF output file branching into multiple Categorization is not implemented yet.

    Example command:

    ``law run Categorization --version test --category-name etau --config-name base_config \
--dataset-name tt_dl --workflow local --PreprocessRDF-category-name base_selection \
--workers 10 --feature-modules-file features --systematic-names central,jec,tes ``

    :param feature_modules_file: filename inside ``cmt/config/`` or ``../config/`` (w/o extension)
        with the RDF modules to run
    :type feature_modules_file: str

    :param skip_preprocess: whether to skip the PreprocessRDF task and run directly on the input data
    :type skip_preprocess: bool
    """

    feature_modules_file = luigi.Parameter(description="filename with RDataFrame modules to run",
        default=law.NO_STR)
    keep_and_drop_file = luigi.Parameter(description="filename with branches to save, empty: all",
        default="")
    skip_preprocess = luigi.BoolParameter(default=False, description="whether to skip the "
        " PreprocessRDF task, default: False")
    systematic_names = law.CSVParameter(default=("central",), description="names of systematics "
        "to run, default: central only")
    # systematic = law.WorkflowParameter(default="central") # tried to use WorkflowParameter but these need create_branch_map to be a class function which is difficult
    # systematic_direction = law.WorkflowParameter(default="central")
    compute_filter_efficiency = luigi.BoolParameter(description="compute efficiency of each filter "
        "applied, default: False", default=False)

    default_store = "$CMT_STORE_EOS_CATEGORIZATION"
    default_wlcg_fs = "wlcg_fs_categorization"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.custom_output_tag = f"_{len(self.systematic_names)}systs"
        self.max_events = self.dataset.get_aux("categorization_max_events", None)
        if sum((x is not None for x in [self.dataset.get_aux("categorization_max_events"), self.dataset.get_aux("preprocess_merging_factor"), self.dataset.get_aux("event_threshold")])) > 1:
            raise RuntimeError(f"Dataset {self.dataset.name} error : you can only specify one of categorization_max_events, preprocess_merging_factor, event_threshold a the same time")
        if self.dataset.get_aux("categorization_max_events") is not None and self.request_cpus > 1:
            raise RuntimeError(f"Dataset.categorization_max_events is not compatible with request_cpus > 1 (in dataset {self.dataset.name}), due to RDataFrame limitation") # RDataFrame.Range is not compatible with multithreading
        self.syst_variations = systematic_names_to_variations(self.systematic_names)


    # def build_splitted_branches(self):
    #     """ Constructs a map that makes each Categorization task process only part of each Preprocess input file,
    #     such that each Categorization does not process more than a certain number of events.
    #     The resulting map is cached.
    #     """
    #     if not os.path.exists(
    #             os.path.expandvars("$CMT_TMP_DIR/%s/splitted_branches_categorization_%s/%s.json" % (
    #                 self.config_name, self.max_events, self.dataset.name))):
    #         ROOT = import_root()
    #         files = [target["root"].path for target in self.workflow_requires()["data"]["collection"].targets.values()]
    #         branches = []
    #         for ifil, fil in enumerate(files):
    #             nevents = -1
    #             print("Analyzing file %s" % fil)
    #             nevents = self.get_n_events(fil)
    #             initial_event = 0
    #             isplit = 0
    #             while initial_event < nevents:
    #                 max_events = min(initial_event + self.max_events, int(nevents))
    #                 branches.append({
    #                     "parent_branches": [ifil],
    #                     "part_single_file" : True,
    #                     "split": isplit,
    #                     "initial_event": initial_event,
    #                     "max_events": max_events,
    #                 })
    #                 initial_event += self.max_events
    #                 isplit += 1
    #         with open(create_file_dir(os.path.expandvars(
    #                 "$CMT_TMP_DIR/%s/splitted_branches_categorization_%s/%s.json" % (
    #                 self.config_name, self.max_events, self.dataset.name))), "w+") as f:
    #             json.dump(branches, f, indent=4)
    #     else:
    #          with open(create_file_dir(os.path.expandvars(
    #                 "$CMT_TMP_DIR/%s/splitted_branches_categorization_%s/%s.json" % (
    #                 self.config_name, self.max_events, self.dataset.name)))) as f:
    #             branches = json.load(f)
    #     return branches

    #@classmethod
    def create_branch_map(self):
        """ 
        Returns dict(branch_nb=dict(
            systematic="...", # systematic name
            systematic_direction="up", # direction
            reduced_branch_nb=..., # what the numeric branch number would be with only one systematic, for output file nb. in case no preprocess merging this is the parent branch, otherwise its maximum is lower
            parent_branches=[], # list of branch numbers of parent task (ie PreProcess branch numbers)
            part_single_file=True/False, # if True, then we use a subset of a single file using RDataframe.Range (only for len(parent_branches)==1) 
            initial_event=, # if part_single_file=True, first event to process
            max_events=,
            )
        )
        """
        return categorization_branch_map(self.config_name, self.dataset, self.syst_variations, get_categorization_merging_factor(self.dataset, self.category))

        # if self.max_events is not None:
        #     {i : chunk for i, chunk in enumerate(self.build_splitted_branches())}
        # elif self.merging_factor >= 1:
        #     preprocess_branches = self.requires()["data"].branch_map.keys()
        #     return {i : dict(parent_branches=chunk, part_single_file=False) for i, chunk in enumerate(law.util.iter_chunks(preprocess_branches, math.ceil(len(preprocess_branches)/self.merging_factor)))}
        # else:
        #     return {i : dict(parent_branches=[i], part_single_file=False) for i in self.requires()["data"].branch_map}

    def workflow_requires(self):
        if not self.skip_preprocess:
            return {"data": PreprocessRDF.vreq(self, _prefer_cli=["category_name", "keep_and_drop_file"],
                    systematic_variations=self.syst_variations)} 
        else:
            return {"data": InputData.req(self)}

    def requires(self):
        parent_branches = self.branch_data["parent_branches"]
        if not self.skip_preprocess:
            if len(parent_branches) == 1:
                preprocess_kwargs = dict(branch=parent_branches[0])
            elif len(parent_branches) > 1:
                preprocess_kwargs = dict(branches=parent_branches)
            else: raise ValueError(f"Issue in Categorization.requires : invalid parent_branches. branch_data is {self.branch_data}")
            return PreprocessRDF.vreq(self, systematic_variations=self.syst_variations, # we could also use ((self.systematic, self.systematic_direction), but probably this would avoid instantiating new tasks. note the comma is important
                **preprocess_kwargs, _exclude=["branch"], _prefer_cli=["category_name", "keep_and_drop_file"])
        else:
            return [InputData.req(self, file_index=file_index) for file_index in parent_branches] 

    def output(self):
        """
        :return: One file per input file with the tree + additional branches
        :rtype: `.root`
        """
        branch_addendum = get_categorization_branch_addendum(self.branch_data)
        out = {"root" : self.local_target(f"data_{branch_addendum}.root")}
        if self.compute_filter_efficiency:
            out["cut_flow"] = self.local_target(f"cutflow_{branch_addendum}.json")
        out = law.SiblingFileCollection(out)
        return out
    
    use_workflow_output_in_proxy = True # this line will only have effect on a modified law version that speeds up scheduling of large workflows
    def workflow_output(self):
        """ Performance improvement to avoid instantiating all the branches.Has to return a DotDict(collection=SiblingFileCollection(targets=dict(branch->SiblingFileCollection for that branch)))"""
        out = {}
        compute_filter_efficiency = self.compute_filter_efficiency
        for branch, branch_data in self.branch_map.items():
            branch_addendum = get_categorization_branch_addendum(branch_data)
            out_branch = {"root" :self.local_target(f"data_{branch_addendum}.root")}
            if compute_filter_efficiency:
                out_branch["cut_flow"] = self.local_target(f"cutflow_{branch_addendum}.json")
            out[branch] = out_branch
        return DotDict([("collection", law.SiblingFileCollection(out))])

    def workflow_complete(self):
        """ Performance improvement to avoid instantiating all the branches """
        return self.workflow_output()["collection"].complete()

    @law.decorator.notify
    @law.decorator.localize(input=False)
    def run(self):
        """
        Creates one RDataFrame per input file, runs the desired RDFModules and applies a
        post-selection
        """
        if self.no_run: raise RuntimeError("Categorization.no_run was set to True, yet Categorization was being run. Cancelling task")
        import gc
        gc.collect()
        ROOT = import_root()
        if self.request_cpus > 1:
            ROOT.ROOT.EnableImplicitMT(self.request_cpus)
        branch_data = self.branch_data
        systematic, systematic_direction = branch_data["systematic"], branch_data["systematic_direction"]

        outp = self.output()
        if self.skip_preprocess:
            input_files = [t[0].path for t in self.input()]
        elif len(self.branch_data["parent_branches"]) == 1:
            input_files = [self.input()[systematic][systematic_direction]["root"].path]
        else:
            try:
                input_files = [x[systematic][systematic_direction]["root"].path for x in self.input()["collection"].targets.values()]
            except KeyError as e:
                raise ValueError(f'Categorization : did not find syst {systematic} - {systematic_direction} in task input collection {repr(self.input()["collection"].targets)}. branch_data={self.branch_data}') from e
        
        print(f"RUNNING : Categorization(syst={systematic}-{systematic_direction}), branch_data={branch_data}\n"
              f"Output file : {outp['root'].path}\n"
              f"Input files : {input_files}")
        
        non_empty_input_files = [] # having empty files in a TChain leads to some issues with list of branches being empty
        for inp_tree in input_files:
            with GuardedTFile(inp_tree) as f:
                if f.IsZombie():
                    raise RuntimeError("Categorization : corrupted input file " + inp_tree)
                evts = f.Get("Events")
                if not evts:
                    raise RuntimeError("Categorization : empty TFile as input : " + inp_tree)
                if evts.GetEntries() > 0:
                    non_empty_input_files.append(inp_tree)
                else:
                    print("Empty input file " + inp_tree) 
        
        #branches = list(df.GetColumnNames())
        if len(non_empty_input_files) == 0: # possible empty input file. RDataFrame behaviour when no events pass selection is to create a TTree with no branches (or sometimes a file with no tree, seems it depends....)
            # no corruption : just no events passed input selections. Create empty output
            print("CATEGORIZATION : no events in input files")
            df.Snapshot(self.tree_name, create_file_dir(outp["root"].path), [])
            if self.compute_filter_efficiency:
                with open(create_file_dir(self.output()["cut_flow"].path), "w+") as f:
                    json.dump({}, f, indent=4)
            return
            
        if len(non_empty_input_files) == 1 and not self.dataset.friend_datasets:
            # simple case : we don't need to amnually build a TTree/TChain
            df = self.RDataFrame(self.tree_name, non_empty_input_files[0],
                    allow_redefinition=self.allow_redefinition)
        else:
            tchain = ROOT.TChain()
            for inp_tree in non_empty_input_files:
                if tchain.AddFile(inp_tree + "?#" + self.tree_name) != 1:
                    raise RuntimeError("Categorization : TChain could not open file " + inp_tree + "?#" + self.tree_name)
            
            if self.dataset.friend_datasets:
                assert (len(non_empty_input_files) == len(input_files))
                friend_tchain = ROOT.TChain()
                for input_target in self.input():
                    assert (len(input_target) == 2) # only one friend dataset is supported
                    if friend_tchain.AddFile(input_target[1].path + "?#" + self.tree_name) != 1: # InputData output is a tuple, assume second element is the friend
                        raise RuntimeError("Categorization : TChain could not open file " + input_target[1].path + "?#" + self.tree_name)
            
                tchain.AddFriend(friend_tchain, "friend")
            df = self.RDataFrame(tchain, allow_redefinition=self.allow_redefinition)

        branches = list(df.GetColumnNames())

        # restricting number of events
        if self.branch_data["part_single_file"]:
            assert (len(self.branch_data["parent_branches"]) == 1)
            df = df.Range(self.branch_data["initial_event"], self.branch_data["max_events"])
        
        # df.Count().OnPartialResult(50000, ROOT.printRDFProgress) # prints every 50k input events processes

        selection = self.config.get_object_expression(self.category, self.dataset.process.isMC,
            systematic, systematic_direction)
        dataset_selection = self.dataset.get_aux("selection")
        if dataset_selection and dataset_selection != "1":
            selection = jrs(dataset_selection, selection, op="and")

        feature_modules = self.get_feature_modules(self.feature_modules_file, systematic, systematic_direction)
        if len(feature_modules) > 0:
            for module in feature_modules:
                df, add_branches = module.run(df)
                branches += add_branches
        else:
            print("## WARNING : no modules are being run for Categorization")
        branches = self.get_branches_to_save(branches, self.keep_and_drop_file, systematic=systematic, systematic_direction=systematic_direction)
        
        filtered_df = df.Filter(selection, self.category.name)
        # filtered_df.Count().OnPartialResult(50000, ROOT.printRDFOutputProgress) # print every 50k output event processed
        filtered_df.Snapshot(self.tree_name, create_file_dir(outp["root"].path), branches)

        if self.compute_filter_efficiency:
            report = filtered_df.Report()
            json_res = {cutReport.GetName():
                {"pass": cutReport.GetPass(), "all": cutReport.GetAll()}
                for cutReport in report.GetValue()}
            with open(create_file_dir(self.output()["cut_flow"].path), "w+") as f:
                json.dump(json_res, f, indent=4)


class CategorizationWrapper(DatasetCategoryWrapperTask, law.WrapperTask):
    """
    Wrapper task to run the Categorization task over several datasets in parallel.

    Example command:

    ``law run CategorizationWrapper --version test --category-names etau --config-name base_config \
--dataset-names tt_dl,tt_sl --Categorization-workflow htcondor --workers 20 \
--Categorization-base-category-name base_selection``
    """
    # not strictly necessary to add this parameter, just for convenience
    systematic_names = law.CSVParameter(default=("central",), description="names of systematics "
        "to run, default: central only")
    auto_tasks_per_job = luigi.IntParameter(default=0, description="If >0, automatically pick tasks_per_job according to dataset.categorization_tasks_per_job, using the value given here as fallback")

    def atomic_requires(self, dataset, category):
        kwargs = dict()
        if self.auto_tasks_per_job:
            kwargs["tasks_per_job"] = dataset.get_aux("categorization_tasks_per_job", self.auto_tasks_per_job)
        return Categorization.req(self, dataset_name=dataset.name, category_name=category.name,
            systematic_names=self.systematic_names, **kwargs)

class DNNInference(RDFModuleTask, DatasetTaskWithCategory,
        law.LocalWorkflow, HTCondorWorkflow, SGEWorkflow, SlurmWorkflow):
    """ Inference with DNN. Inputs is Categorization output with the input features already computed.
    Performs DNN inference using ONNXRuntime. Outputs a TTree with only the dnn outputs.
    Configuration is through the python config, through the Config.dnn parameter.
    config.dnn should be nested DotDict with :
    ``
    self.dnn = DotDict(
            my_nonresonant_dnn=DotDict(
                model_folder="/path/to/nonres/DNN/folder",
                out_branch="dnn_ZHbbtt_kl_1",
                systematics=["tes", "jer", "jec"]
            ),
            another_resonant_dnn=DotDict(
                model_folder="/path/to/res/DNN/folder",
                resonant_masses=resonant_masses_ZH,
                out_branch="dnn_ZHbbtt_kl_1_{mass}", # {mass} will be replaced by the resonant mass
                systematics=["tes", "jer", "jec"]
            ),
        )
    
    ``
    The names for the DNN currently have no use. One can run many different models in the same task, as long as they have different out_branch.
    Systematics up/down expressions will be fetched from config.systematics. The corresponding inputs 
    The resonant DNN mode is enabled by specifying resonant_masses in the config. The feature named "res_mass" will not be read from the input tree but rather picked from the list of masses in the config.
    """
    systematic_names = law.CSVParameter(default=("central",), description="names of systematics "
        "to run, default: central only")
    
    store_systematics = luigi.BoolParameter(default=True, description="whether to compute "
        "systematic variations (listed in config.dnn.MODEL_NAME.systematics), default: True")
    input_step_size = luigi.Parameter(default="0.7MB", description="batch size to use for inference. "
        "Passed to uproot.iterate as step_size, controls memory usage of inference. 100MB uses 36GB of memory (with 2 DNNs, incl one parametrized, depends on network architecture). 10MB->10GB memory, 3MB->4GB"
        " default: 10MB.")
    
    default_store = "$CMT_STORE_EOS_CATEGORIZATION"
    default_wlcg_fs = "wlcg_fs_categorization"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.custom_output_tag = f"_{len(self.systematic_names)}systs"
        self.parametrized_resonant = True

    def workflow_requires(self):
        # _prefer_cli is needed to allow for specifying --Categorization-workflow htcondor. Without it, it is only possible to set the workflow for both Categoriation and DNNInference and JoinDNNInference at the same time
        # with --JoinDNNInference-workflow htcondor 
        return {"data": Categorization.vreq(self)} # not necessary since DNNInference does not inherit from Categorization anymore :  _prefer_cli=["workflow", "htcondor_scheduler", "transfer_logs", "custom_condor_tag", "retries", "poll_interval", "tasks_per_job"]

    def requires(self):
        return Categorization.vreq(self, branch=self.branch)

    def create_branch_map(self):
        return self.requires()["data"].branch_map # will use the same branch map as Categorization
    
    @property
    def systematic(self):
        return self.branch_data["systematic"]
    @property
    def systematic_direction(self):
        return self.branch_data["systematic_direction"]

    def output(self):
        """
        :return: One file per input file with the tree + additional branches
        :rtype: `.root`
        """
        return self.local_target(f"data_{get_categorization_branch_addendum(self.branch_data)}.root")

    use_workflow_output_in_proxy = True
    def workflow_output(self):
        """ Performance improvement to avoid instantiating all the branches """
        return DotDict([("collection", law.SiblingFileCollection({
            branch : self.local_target(f"data_{get_categorization_branch_addendum(branch_data)}.root")
            for branch, branch_data in self.branch_map.items()
        }))])
    
    def workflow_complete(self):
        """ Performance improvement to avoid instantiating all the branches """
        return self.workflow_output()["collection"].complete()
    
    class InferenceHelper:
        """ Class dealing with inference of a single model """
        input_dtype = np.float32

        def __init__(self, task:"DNNInference", model_config:DotDict):
            self.model_config = model_config
            model_folder = model_config.model_folder
            self.systs_directions = [("", "")]
            if task.dataset.process.isMC and task.store_systematics:
                if task.systematic and task.systematic != "central": # case of systematic specified as law parameter, in which case we only compute that one systematic and not the nominal
                    systematic = task.config.systematics.get(task.systematic)
                    self.systs_directions = [(systematic.expression, getattr(systematic, task.systematic_direction))]
                
                for syst_name in model_config.get("systematics", []): # case of systematics specified
                    systematic = task.config.systematics.get(syst_name)
                    self.systs_directions += [(systematic.expression, systematic.up), (systematic.expression, systematic.down)]
            try:
                self.resonant_masses = model_config.resonant_masses
                self.parametrized_dnn = True
            except KeyError:
                self.resonant_masses = [None]
                self.parametrized_dnn = False
            
            with open(model_folder + "/features.txt", "r") as f:
                features = f.read().split("\n")
            self.features_dnn = list(filter(len, features)) # remove empty lines

            import onnxruntime
            sess_options = onnxruntime.SessionOptions()
            sess_options.intra_op_num_threads = 1 # Disable multithreading, for running on a batch system. Can be enabled (set to 0) when running interactively. TODO make this a setting.
            sess_options.enable_mem_pattern = False # attempt to avoid sudden memory doubling during inference
            sess_options.enable_cpu_mem_arena = False
            # TODO make providers choice also a setting (CUDAExecutionProvider and/or CPUExecutionProvider)
            self.sessions = [onnxruntime.InferenceSession(model_folder + f"/model_merged_{ensemble_nb}.onnx", sess_options=sess_options, providers=["CPUExecutionProvider"]) for ensemble_nb in range(2)]
        
        def get_input_branches(self):
            """ Returns the list of all branches needed for DNN inference (incl. systematic variations if needed) """
            features_inputTree = []
            for feature in self.features_dnn:
                if feature != "res_mass": # res_mass is a special feature that is not generated by HHDNNInputs RDF module
                    for syst_name, direction in self.systs_directions:
                        features_inputTree.append(f"{feature}{syst_name}{direction}")
            features_inputTree.append("event")
            return features_inputTree

        def get_output_branch_types(self) -> dict[str, np.dtype]:
            """ Returns output branch name and type for DNN output.
            As dict branch_name (str) -> branch type (np dtype) to be used as input to uproot.WritableDirectory.mktree
            """
            out_branch_types = {}
            for syst_name, direction in self.systs_directions:
                for res_mass in self.resonant_masses:
                    out_branch_prefix = self.model_config.out_branch.format(mass=res_mass) if self.parametrized_dnn else self.model_config.out_branch
                    out_branch_types[f"{out_branch_prefix}{syst_name}{direction}"] = float
            return out_branch_types
        
        def inference_on_batch(self, batch:dict[str, np.ndarray]):
            """ Performs the inference given model settings and the input tree.
            Parameters : 
            - model : DotDict of model settings
            - input_tree : an uproot TTree holding the DNN input features
            Returns : a dict with DNN_branch_name -> ndarray of DNN values (multiple branch names come from multiple systematics and multiple resonant mass points if resonant)
            """
        
            filterPerEnsemble = [batch["event"] % 2 == 1, batch["event"] % 2 == 0]
            """ Boolean arrays to choose which event goes to which model ensemble. Even events go to ensemble 1 (that was trained on odd events), and vice versa """

            # Creating output arrays now, to be filled later
            out = {}
            for syst_name, direction in self.systs_directions:
                for res_mass in self.resonant_masses:
                    out_branch_prefix = self.model_config.out_branch.format(mass=res_mass) if self.parametrized_dnn else self.model_config.out_branch
                    out[f"{out_branch_prefix}{syst_name}{direction}"] = np.empty_like(batch["event"], dtype=self.input_dtype)
            
            for ensemble_i, session in enumerate(self.sessions):
                dnn_input_features_list = []
                event_count_currentEnsemble = np.count_nonzero(filterPerEnsemble[ensemble_i])
                for syst_name, direction in self.systs_directions:
                    for res_mass in self.resonant_masses:
                        feats_vectors = []
                        for feature_name in self.features_dnn:
                            if self.parametrized_dnn and feature_name == "res_mass":
                                feats_vectors.append(np.full(shape=(event_count_currentEnsemble), fill_value=res_mass, dtype=self.input_dtype))
                            else:
                                in_branch_name = f"{feature_name}{syst_name}{direction}"
                                feats_vectors.append(batch[in_branch_name][filterPerEnsemble[ensemble_i]].astype(dtype=self.input_dtype, copy=False))
                        dnn_input_features_list.append(np.stack(feats_vectors, axis=1))
                
                dnn_input_features_list = np.concatenate(dnn_input_features_list)
                
                res = session.run(["output"], {"input":dnn_input_features_list})[0]

                currentIndexInOutput = 0
                for syst_name, direction in self.systs_directions:
                    for res_mass in self.resonant_masses:
                        out_branch_prefix = self.model_config.out_branch.format(mass=res_mass) if self.parametrized_dnn else self.model_config.out_branch
                        out[f"{out_branch_prefix}{syst_name}{direction}"][filterPerEnsemble[ensemble_i]] = res[currentIndexInOutput*event_count_currentEnsemble:(currentIndexInOutput+1)*event_count_currentEnsemble]
                        currentIndexInOutput += 1

            return out


    @law.decorator.notify
    @law.decorator.localize(input=False)
    def run(self):
        assert len(self.config.dnn) >= 1
        inferenceHelpers = {model_name : DNNInference.InferenceHelper(self, model_config) for model_name, model_config in self.config.dnn.items()}

        input_branches = set()
        output_branches_config = {}
        for model_name, helper in inferenceHelpers.items():
            input_branches = input_branches.union(helper.get_input_branches())
            output_config = helper.get_output_branch_types()
            if not set(output_config.keys()).isdisjoint(output_branches_config.keys()):
                raise RuntimeError(f"Two DNN configs are trying to write to a branch with the same name(s) : {set(output_config.keys()).intersection(output_branches_config.keys())}. One of the DNN config is {model_name}")
            output_branches_config.update(output_config)

        import uproot

        try:
            with uproot.open(self.input()["root"].path + ":" + self.tree_name) as input_file:
                with uproot.create(self.output().path) as file_out:
                    tree_out = file_out.mktree(self.tree_name, output_branches_config)

                    for batch_input in uproot.iterate(input_file, library="np", filter_name=list(input_branches), step_size=self.input_step_size):
                        if len(batch_input) < len(input_branches):
                            raise RuntimeError("Some features for the DNN were not found in the tree ! Features " + str(input_branches.difference(batch_input.keys())) + " are missing.")
                        print(f"Inference on batch with {len(batch_input['event'])} events")
                        out_dnn = {}
                        for model_name, helper in inferenceHelpers.items():
                            out_dnn.update(helper.inference_on_batch(batch_input))
                        tree_out.extend(out_dnn)
                        gc.collect() # trying to avoid memory spikes on batch jobs, not sure if it has any effect

        except uproot.exceptions.KeyInFileError:
            raise RuntimeError("DNNInference : WARNING : No Events tree in " + self.input()["root"].path)
            # Sometimes inputs have no "Events" tree
            # -> make an empty output (not sure if this is right)
            with uproot.create(self.output().path) as file_out:
                print("WARNING : Empty input file " + self.input()["root"].path)
                file_out.mktree(self.tree_name, output_branches_config)

        
class DNNInferenceWrapper(DatasetCategoryWrapperTask, law.WrapperTask):
    """ If systematics are wanted, --DNNInference-systematic-names should be set """
    def atomic_requires(self, dataset, category):
        return DNNInference.req(self, dataset_name=dataset.name, category_name=category.name)

class JoinDNNInference(RDFModuleTask, DatasetTaskWithCategory,
        law.LocalWorkflow, HTCondorWorkflow, SGEWorkflow, SlurmWorkflow):
    """
    Joins the DNN inference output with Categorization output
    Relies on DNNInference keeping events in the same order in the TTree
    """
    systematic_names = law.CSVParameter(default=("central",), description="names of systematics "
        "to run, default: central only")
    
    default_store = "$CMT_STORE_EOS_MERGECATEGORIZATION"
    default_wlcg_fs = "wlcg_fs_categorization"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.custom_output_tag = f"_{len(self.systematic_names)}systs"

    def workflow_requires(self):
        return {
            "categorization_data": Categorization.vreq(self), #  _prefer_cli=["workflow", "htcondor_scheduler", "transfer_logs", "custom_condor_tag", "retries", "poll_interval", "tasks_per_job"]
            "dnn_data": DNNInference.vreq(self)}

    def requires(self):
        return {"categorization_data": Categorization.vreq(self, branch=self.branch), "dnn_data": DNNInference.vreq(self, branch=self.branch)}

    def create_branch_map(self):
        return self.requires()["categorization_data"].branch_map
    
    def output(self):
        """
        :return: One file per input file with the tree + additional branches
        :rtype: `.root`
        We follow the same structure of output as Categorization so we can exchange Categorization and JoinDNNInference as dependency easily 
        """
        return law.SiblingFileCollection({"root" : self.local_target(f"data_{get_categorization_branch_addendum(self.branch_data)}.root")})

    use_workflow_output_in_proxy = True
    def workflow_output(self):
        """ Performance improvement to avoid instantiating all the branches.
         We follow the same structure of output as Categorization so we can exchange Categorization and JoinDNNInference as dependency easily """
        return DotDict([("collection", law.SiblingFileCollection({
            branch : {"root": self.local_target(f"data_{get_categorization_branch_addendum(branch_data)}.root")}
            for branch, branch_data in self.branch_map.items()
        }))])

    def workflow_complete(self):
        """ Performance improvement to avoid instantiating all the branches """
        return self.workflow_output()["collection"].complete()

    @law.decorator.notify
    @law.decorator.localize(input=False)
    def run(self):
        ROOT = import_root()
        try:
            f_categorization = ROOT.TFile.Open(self.input()["categorization_data"]["root"].path)
            tree = f_categorization.Get(self.tree_name)
            if not tree:
                raise RuntimeError("Could not open input tree " + self.input()["categorization_data"]["root"].path + " as no Events tree was found") 
            else:
                tree.AddFriend(self.tree_name, self.input()["dnn_data"].path)
                if not tree.GetListOfFriends().First(): # AddFriend can silently not add the friend in case of failure. It just calls Error() and returns a TFriendElement anyway. So we check that our tree has a friend after AddFriend
                    # os.remove(self.input()["dnn_data"].path)
                    # os.remove(self.input()["categorization_data"]["root"].path)
                    raise RuntimeError(f"JoinDNNInference AddFriend failed (maybe one of the trees is shuffled, or invalid file).\n Categorization : {self.input()['categorization_data']['root'].path}\nDNN : {self.input()['dnn_data'].path}")

                df = ROOT.RDataFrame(tree)
                df.Snapshot(self.tree_name, create_file_dir(self.output()["root"].path))
        finally:
            # del df
            # del tree
            try:
                f_categorization.Close()
            except: pass

class JoinDNNInferenceWrapper(DatasetCategoryWrapperTask, law.WrapperTask):
    def atomic_requires(self, dataset, category):
        return JoinDNNInference.req(self, dataset_name=dataset.name, category_name=category.name)


class DNNInferenceBatchRunner(DatasetTask, law.LocalWorkflow, HTCondorWorkflow, SGEWorkflow): # adding SGEWorkflow is necessary to get the custom-condor-tag parameter.....
    """ Runs DNNInference and JoinDNNInference as one remote job, eventually batching systematics as one remote job as well 
    Requires : Categorization output
    Done when : JoinDNNInference is done
    """
    category_names = law.CSVParameter(description="names of categories to run")
    systematic_names = law.CSVParameter(default=("central",), description="names of systematics "
        "to run, default: central only (empty string)")
    branch_chunking = luigi.IntParameter(default=20, description="Run this many categorization branches (counting systematics in nb of branches) in one job (systs are already batched). 0 is automatic")

    output_collection_cls = law.FileCollection

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # tasks wrapped by this class do not allow composite categories, so split them here
        self.categories = []
        for name in self.category_names:
            category = self.config.categories.get(name)
            if category.subcategories:
                self.categories.extend(category.subcategories)
            else:
                self.categories.append(category)
    
    def create_branch_map(self):
        """ Individual branch data should be : {category->(branches...), ...} where branches can be an empty tuple meaning run workflow"""
        reqs = self.requires()
        branch_datas = []
        common_branch_categories = {} # categories that are very small hence they are all bunched up in one single job
        syst_variations_count = len(systematic_names_to_variations(self.systematic_names))
        branch_chunking = self.branch_chunking
        for category in self.categories:
            cat_branch_map = reqs[category.name].branch_map
            if len(cat_branch_map) == syst_variations_count: # wrap all such categories and all systs in one job
                common_branch_categories[category] = tuple()
            else: # make chunks
                for cat_branch_chunk in law.util.iter_chunks(cat_branch_map.keys(), branch_chunking):
                    branch_datas.append({category : tuple(cat_branch_chunk)})
        if len(common_branch_categories) > 0:
            branch_datas.append(common_branch_categories)
        if len(branch_datas) == 0: raise ValueError()
        return {i : branch_data for i, branch_data in enumerate(branch_datas)}

    def workflow_requires(self):
        return {category.name : Categorization.vreq(self, category_name=category.name) for category in self.categories}

    @law.decorator.notify
    def run(self):
        # we first run DNNInference because otherwise running JoinDNNInference would trigger the whole DNNInference workflow and not just the branch we want
        dnn_tasks = []
        for category, branches_toRun in self.branch_data.items():
            if len(branches_toRun) == 0:
                dnn_tasks.append(DNNInference.vreq(self, category_name=category.name, branch=-1, workflow="local", _exclude=["branch", "branches", "workflow"]))
            else:
                dnn_tasks.extend(DNNInference.vreq(self, category_name=category.name, branch=branch, workflow="local", _exclude=["branch", "branches", "workflow"]) for branch in branches_toRun)
        yield dnn_tasks

        join_dnn_tasks = []
        for category, branches_toRun in self.branch_data.items():
            if len(branches_toRun) == 0:
                join_dnn_tasks.append(JoinDNNInference.vreq(self, category_name=category.name, branch=-1, workflow="local", _exclude=["branch", "branches", "workflow"]))
            else:
                join_dnn_tasks.extend(JoinDNNInference.vreq(self, category_name=category.name, branch=branch, workflow="local", _exclude=["branch", "branches", "workflow"]) for branch in branches_toRun)
        yield join_dnn_tasks

    def workflow_complete(self):
        return all(JoinDNNInference.vreq(self, category_name=category.name, _exclude=["branch", "branches", "workflow"]).complete() for category in self.categories)
    
    def complete(self):
        for category, branches_toRun in self.branch_data.items():
            if len(branches_toRun) == 0:
                if not JoinDNNInference.vreq(self, category_name=category.name, branch=-1, workflow="local", _exclude=["branch", "branches", "workflow"]).complete(): return False
            else:
                if not JoinDNNInference.vreq(self, category_name=category.name, branches=branches_toRun, workflow="local", _exclude=["branch", "branches", "workflow"]).complete(): return False
        return True

    # use_workflow_output_in_proxy = True # don't use default workflow.output as that tries to make an empty SiblingFileCOll
    # def workflow_output(self):
    #     return []
    def output(self):
        # Workflows need to have an output
        outp = {}
        for category, branches_toRun in self.branch_data.items():
            if len(branches_toRun) == 0:
                outp[category] = JoinDNNInference.vreq(self, category_name=category.name, branch=-1, workflow="local", _exclude=["branch", "branches", "workflow"]).output()
            else:
                outp[category] = JoinDNNInference.vreq(self, category_name=category.name, branches=branches_toRun, workflow="local", _exclude=["branch", "branches", "workflow"]).output()
        return outp

class DNNInferenceBatchWrapper(DatasetSuperWrapperTask):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def atomic_requires(self, dataset):
        return DNNInferenceBatchRunner.vreq(self, dataset_name=dataset.name)


class MergeCategorization(DatasetTaskWithCategory, law.LocalWorkflow):
    """
    Merges the output from the Categorization or PreprocessRDF tasks in order to reduce the
    parallelization entering the plotting tasks. By default it merges into one output file,
    although a bigger number can be set with the `merging` parameter inside the dataset
    definition.

    In simulated samples, ``hadd`` is used to perform the merging. In data samples, to avoid
    skipping events due to different branches between them, ``haddnano.py`` (safer but slower)
    is used instead. In any case, the use of one method or the other can be forced by specifying
    the parameters ``--force-hadd`` and ``--force-haddnano`` respectively.

    Example command:

    ``law run MergeCategorization --version test --category-name etau \
--config-name base_config --dataset-name tt_sl --workflow local --workers 4``

    :param from_preprocess: whether it merges the output from the PreprocessRDF task (True)
        or Categorization (False, default)
    :type from_preprocess: bool

    :param force_hadd: whether to force ``hadd`` as tool to do the merging.
    :type force_hadd: bool

    :param force_haddnano: whether to force ``haddnano.py`` as tool to do the merging.
    :type force_haddnano: bool

    :param systematic: systematic to use for categorization.
    :type systematic: str

    :param systematic_direction: systematic direction to use for categorization.
    :type systematic_direction: str
    """

    systematic_names = law.CSVParameter(default=("central",), description="names of systematics "
        "to run, default: central only")

    from_preprocess = luigi.BoolParameter(default=False, description="whether to use as input "
        "PreprocessRDF, default: False")
    from_DNN_inference = luigi.BoolParameter(default=False, description="whether to add the output from DNN inference step, default: False")
    force_hadd = luigi.BoolParameter(default=False, description="whether to force hadd "
        "as tool to do the merging, default: False")
    force_haddnano = luigi.BoolParameter(default=False, description="whether to force haddnano.py "
        "as tool to do the merging, default: False")
    
    default_store = "$CMT_STORE_EOS_MERGECATEGORIZATION"
    default_wlcg_fs = "wlcg_fs_categorization"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.from_preprocess:
            if self.from_DNN_inference:
                self.input_task_cls = JoinDNNInference
            else:
                self.input_task_cls = Categorization
            self.input_task_kwargs = dict()
        else:
            assert not self.from_DNN_inference, "Cannot combine from_preprocess && from_DNN_inference"
            self.input_task_cls = PreprocessRDF
            self.input_task_kwargs = dict(systematic_variations=systematic_names_to_variations(self.systematic_names))
    

    def workflow_requires(self):
        return {"data" : self.input_task_cls.vreq(self, **self.input_task_kwargs, _prefer_cli=["workflow"])} # has to be a dict
    
    use_workflow_output_in_proxy = True
    def workflow_output(self):
        """ Performance improvement to avoid instantiating all the branches """
        return DotDict([("collection", law.SiblingFileCollection({
            branch : self.local_target(f"data_{get_categorization_addendum(branch_data)}{branch_data['reduced_branch_nb']}.root")
            for branch, branch_data in self.branch_map.items()
        }))])

    def output_grouped_systematics(self):
        """ Computes a SiblingFileCollection as a dict : systematic->syst_dir->reduced_branch->file"""
        out = {}
        for branch_data in self.branch_map.values():
            out.setdefault(branch_data["systematic"], dict()).setdefault(branch_data["systematic_direction"], dict())[branch_data["reduced_branch_nb"]] = self.output_target(branch_data)
        return law.SiblingFileCollection(out)
    
    def workflow_complete(self):
        """ Performance improvement to avoid instantiating all the branches """
        return self.workflow_output()["collection"].complete()
    
    def create_branch_map(self):
        """ One branch per "file_after_merging". Branch data is : dict(
            systematic=...,
            systematic_direction=...,
            parent_cat_branches=[...], # list of branches from Categorization/JoinDNNInference (as appropriate) that are used to make up this MergeCategorization branch (meaningless if from_preprocess)
            parent_reduced_branches=[...], # list of "reduced" branches from Categorization/JoinDNNINference/PreprocessRDF. It is the index in the input file name. For PreprocessRDF it is also the branch number of Preprocess
            parent_task_branches=[...], # "parent_reduced_branches" if from_preprocess else "parent_cat_branches"
            reduced_branch_nb=..., # branch nb for the output
        )
        """
        # TODO check that "data" is applicable also to 
        #return self.n_files_after_merging * len(systematic_names_to_variations(self.systematic_names))
        from_preprocess = self.from_preprocess
        branch_datas = []
        cat_branch_map = categorization_branch_map(self.config_name, self.dataset, systematic_names_to_variations(self.systematic_names), None if from_preprocess else get_categorization_merging_factor(self.dataset, self.category))
        # we partition the categorization branches per systematic, and then we merge among these partitions
        syst_to_cat = {}
        for cat_branch, cat_branch_data in cat_branch_map.items():
            syst_to_cat.setdefault((cat_branch_data["systematic"], cat_branch_data["systematic_direction"]), dict())[cat_branch] = cat_branch_data
        warn_more_files_after_than_before = True
        for (syst, syst_dir), cat_sub_branch_map in syst_to_cat.items():
            if warn_more_files_after_than_before and self.n_files_after_merging > len(cat_sub_branch_map):
                warn_more_files_after_than_before = False
                print(f"## WARNING : MergeCategorization({self.dataset}, {self.category.name}) has higher n_files_after_merging ({self.n_files_after_merging}) than at Categorization ({len(cat_sub_branch_map)}). MergeCategorization will just copy inputs.")
            
            for i, chunk in enumerate(chunk_list(cat_sub_branch_map.items(), min(self.n_files_after_merging, len(cat_sub_branch_map)))):  # make sure we don't create more files after MergCategorization than before 
                # chunk will be of the form [ (cat_branch, dict(reduced_branch_nb=....)), (cat_branch2, dict(...)), ...]
                if len(chunk) == 0: raise ValueError(f"MergeCategorization branch_map error : empty chunk. Categorization sub branch map for current syst ({syst}-{syst_dir}): {cat_sub_branch_map}")
                cat_branch_chunk, cat_branch_data_chunk = zip(*chunk)
                d = dict(
                    systematic=syst, systematic_direction=syst_dir,
                    parent_cat_branches=tuple(cat_branch_chunk),
                    parent_reduced_branches=tuple(cat_branch_data["reduced_branch_nb"] for cat_branch_data in cat_branch_data_chunk),
                    reduced_branch_nb=i
                )
                d["parent_task_branches"] = d["parent_reduced_branches"] if from_preprocess else d["parent_cat_branches"]
                branch_datas.append(d)
        assert len(branch_datas)>0
        return {i : branch_data for i, branch_data in enumerate(branch_datas)}

    def requires(self):
        parent_branches = self.branch_data["parent_task_branches"]
        if len(parent_branches) == 1:
            parent_kwargs = dict(branch=parent_branches[0])
        elif len(parent_branches) > 1:
            parent_kwargs = dict(branches=parent_branches)
        else:
            raise ValueError()
        return self.input_task_cls.vreq(self, **self.input_task_kwargs, **parent_kwargs, _exclude=["branch"])

    def output_target(self, branch_data):
        return self.local_target(f"data_{get_categorization_addendum(branch_data)}{branch_data['reduced_branch_nb']}.root")
    def output(self):
        return self.output_target(self.branch_data)

    @law.decorator.localize(input=False)
    def run(self):
        ROOT = import_root()
        out_target = self.output()
        branch_data = self.branch_data
        try:
            if self.from_preprocess:
                # preprocess output is syst_name->syst_dir->root
                if len(branch_data["parent_task_branches"]) == 1: # input is a branch task of Preprocess/Categorization/JoinDNNInference
                    input_files = [self.input()[branch_data["systematic"]][branch_data["systematic_direction"]]["root"]]
                else:
                    input_files = [x[branch_data["systematic"]][branch_data["systematic_direction"]]["root"] for x in self.input()["collection"].targets.values()]
            else:
                if len(self.branch_data["parent_task_branches"]) == 1: # input is a branch task of Preprocess/Categorization/JoinDNNInference
                    input_files = [self.input()["root"]]
                else:
                    input_files = [x["root"] for x in self.input()["collection"].targets.values()]
        except KeyError as e:
            raise ValueError(f"Categorization : did not find syst {branch_data['systematic']} - {branch_data['systematic_direction']} in task input collection {repr(self.input())}. branch_data={self.branch_data}") from e

        good_inputs = []
        for inp_target in input_files:  # merge only files with a filled tree
            inp_path = inp_target.path         
            with GuardedTFile(inp_path) as f:
                if f.IsZombie():
                    raise RuntimeError("MergeCategorization : invalid input file " + inp_path)
                if not "Events" in f.GetListOfKeys():
                    # empty input file (no events passed selections in PreprocessRDF)
                    raise RuntimeError("####### WARNING : no Events tree in MergeCategorization input " + inp_path)
                    print("####### WARNING : no Events tree in MergeCategorization input " + inp_path)
                else:
                    tree = f.Get(self.tree_name)
                    if tree:
                        if tree.GetEntries() > 0:
                            good_inputs.append(inp_target)
                        else:
                            print("# Info : no entries were present in in MergeCategorization input " + inp_path)
                    else:
                        raise RuntimeError("MergeCategorization : could not open tree from " + inp_path)
                        
        if len(good_inputs) != 0:
            use_hadd = self.dataset.process.isMC
            assert not(self.force_haddnano and self.force_hadd)
            if self.force_haddnano:
                use_hadd = False
            elif self.force_hadd:
                use_hadd = True
            if use_hadd:
                print("Merging with hadd...")
                law.root.hadd_task(self, good_inputs, out_target, local=True)
            else:
                print("Merging with haddnano.py...")
                cmd = "python3 %s/bin/%s/haddnano.py %s %s" % (
                    os.environ["CMSSW_BASE"], os.environ["SCRAM_ARCH"],
                    create_file_dir(out_target.path), " ".join([f.path for f in good_inputs]))
                rc = law.util.interruptable_popen(cmd, shell=True)[0]
                if rc != 0:
                    raise RuntimeError("haddnano.py failed with return code " + rc)
        else:  # if all input files are empty, create an empty file as output
            # this creates an RDataFrame with 0 events, which will create a TTree with no branches
            ROOT.RDataFrame(0).Snapshot(self.tree_name, create_file_dir(out_target.path), [])


class MergeCategorizationWrapper(DatasetCategoryWrapperTask, law.WrapperTask):
    """
    Wrapper task to run the MergeCategorizationWrapper task over several datasets in parallel.

    Example command:

    ``law run MergeCategorizationWrapper --version test --category-names etau \
--config-name base_config --dataset-names tt_dl,tt_sl --workers 10``
    """
    # not strictly necessary to add this parameter, just for convenience
    systematic_names = law.CSVParameter(default=("central",), description="names of systematics "
        "to run, default: central only")
    
    def atomic_requires(self, dataset, category):
        return MergeCategorization.vreq(self, dataset_name=dataset.name,
            category_name=category.name, systematic_names=self.systematic_names)


class MergeCategorizationStats(DatasetTask, law.tasks.ForestMerge):
    """
    Merges the output from the PreCounter task in order to reduce the
    parallelization entering the plotting tasks.

    :param systematic: systematic to use for categorization.
    :type systematic: str

    :param systematic_direction: systematic direction to use for categorization.
    :type systematic_direction: str

    Example command:

    ``law run MergeCategorizationStats --version test --config-name base_config \
--dataset-name dy_high --workers 10``
    """

    systematic = luigi.Parameter(default="", description="systematic to use for categorization, "
        "default: None")
    systematic_direction = luigi.Parameter(default="", description="systematic direction to use "
        "for categorization, default: None")

    # regions not supported
    region_name = None

    merge_factor = 16

    default_store = "$CMT_STORE_EOS_CATEGORIZATION"
    default_wlcg_fs = "wlcg_fs_categorization"

    def merge_workflow_requires(self):
        return PreCounter.vreq(self, _prefer_cli=["workflow"])

    def merge_requires(self, start_leaf, end_leaf):
        return PreCounter.vreq(self, workflow="local", branches=((start_leaf, end_leaf),),
            _exclude={"branch"})

    def trace_merge_inputs(self, inputs):
        return [inp for inp in inputs["collection"].targets.values()]

    def merge_output(self):
        addendum = PreCounter.get_addendum(self)
        return self.local_target(f"stats{addendum}.json")

    def merge(self, inputs, output):
        # output content
        stats = dict(nevents=0, nweightedevents=0, filenames=[])

        # merge
        for inp in inputs:
            try:
                if "json" in inp.path:
                    _stats = inp.load(formatter="json")
                elif "root" in inp.path:
                    _stats = inp.load(formatter="root")
            except:
                print("error leading input target {}".format(inp))
                raise

            # add nevents
            if "json" in inp.path:
                stats["nevents"] += _stats["nevents"]
                stats["nweightedevents"] += _stats["nweightedevents"]
                stats["filenames"] += _stats["filenames"]
            else:
                try:
                    histo = _stats.Get("histos/events")
                    stats["nevents"] += histo.GetBinContent(1)
                    stats["nweightedevents"] += histo.GetBinContent(2)
                except:
                    stats["nevents"] += 0
                    stats["nweightedevents"] += 0

        output.parent.touch()
        output.dump(stats, indent=4, formatter="json")


class MergeCategorizationStatsWrapper(DatasetSystWrapperTask):
    """
    Wrapper task to run the MergeCategorizationStatsWrapper task over several datasets in parallel.

    Example command:

    ``law run MergeCategorizationStatsWrapper --version test --config-name base_config \
--dataset-names tt_dl,tt_sl --workers 10``
    """
    def atomic_requires(self, dataset, systematic, direction):
        return MergeCategorizationStats.req(self, dataset_name=dataset.name,
            systematic=systematic, systematic_direction=direction)


class EventCounterDAS(DatasetTask):
    """
    Performs a counting of the events with and without applying the necessary weights.
    Weights are read from the config file.
    In case they have to be computed, RDF modules can be run.

    Example command:

    ``law run EventCounterDAS --version test  --config-name base_config --dataset-name ggf_sm``

    :param use_secondary_dataset: whether to use the dataset included in the secondary_dataset
        parameter from the dataset instead of the actual dataset
    :type use_secondary_dataset: bool
    """
    use_secondary_dataset = luigi.BoolParameter(default=False, description="whether to use "
        "secondary_dataset instead of the actual dataset or folder, default: False")

    def requires(self):
        """
        No requirements needed
        """
        return {}

    def output(self):
        """
        :return: One file for the whole dataset
        :rtype: `.json`
        """
        return self.local_target("stats.json")

    def run(self):
        """
        Asks for the numbers of events using dasgoclient and stores them in the output json file
        """
        from analysis_tools.utils import randomize
        from subprocess import call

        tmpname = randomize("tmp")
        cmd = 'dasgoclient --query=="summary dataset={}" > {}'
        if not self.use_secondary_dataset:
            assert self.dataset.dataset
            rc = call(cmd.format(self.dataset.dataset, tmpname), shell = True)
        else:
            assert self.dataset.get_aux("secondary_dataset", None)
            rc = call(cmd.format(self.dataset.get_aux("secondary_dataset"), tmpname), shell = True)
        if rc == 0:
            with open(tmpname) as f:
                d = json.load(f)
            output_d = {
                "nevents": d[0]["nevents"],
                "nweightedevents": d[0]["nevents"],
            }
        with open(create_file_dir(self.output().path), "w+") as f:
            json.dump(output_d, f, indent=4)
        os.remove(tmpname)


class EventCounterDASWrapper(DatasetSuperWrapperTask):
    """
    Wrapper task to run the EventCounterDAS task over several datasets in parallel.

    Example command:

    ``law run EventCounterDASWrapper --version test  --config-name base_config \
--dataset-names tt_dl,tt_sl --workers 2``
    """

    def atomic_requires(self, dataset):
        return EventCounterDAS.req(self, dataset_name=dataset.name)
