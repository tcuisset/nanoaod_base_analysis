# coding: utf-8

"""
Plotting tasks.
"""

__all__ = []

import os
from copy import deepcopy as copy
import json
import math
import itertools
import functools
import array
from collections import OrderedDict, defaultdict
from dataclasses import dataclass
import numpy as np
import warnings
with warnings.catch_warnings():
    warnings.filterwarnings('ignore')
    import cppyy

import law
from law.util import flatten
from law.contrib.root import GuardedTFile
import luigi
from cmt.util import hist_to_array, hist_to_graph, get_graph_maximum, update_graph_values, make_cpp_identifier, root_setIgnoreLevel

from ctypes import c_double

from analysis_tools.utils import (
    import_root, create_file_dir, join_root_selection, randomize
)
from analysis_tools import Category, Feature
from cmt.base_tasks.base import (
    ConfigTask, DatasetTask, DatasetWrapperTask, ProcessGroupNameTask, HTCondorWorkflow, SGEWorkflow, SlurmWorkflow,
    DatasetTaskWithCategory, ConfigTaskWithCategory, ConfigTaskWithRegion, RDFModuleTask, InputData, FitBase, QCDABCDTask, get_n_files_after_merging
)

from cmt.base_tasks.preprocessing import (
    Categorization, MergeCategorization, MergeCategorizationStats, PreprocessRDF
)

ROOT = import_root()

EMPTY = -1.e5

directions = ["up", "down"]

class BasePlotTask(ConfigTaskWithRegion):
    """
    Task that wraps parameters used in all plotting tasks. Can't be run.

    :param feature_names: names of features to plot. Uses all features when empty.
    :type feature_names: csv string

    :param feature_tags: list of tags for filtering features selected via feature names.
    :type feature_tags: csv string

    :param skip_feature_names: names or name pattern of features to skip
    :type skip_feature_names: csv string

    :param skip_feature_tags: list of tags of features to skip
    :type skip_feature_tags: csv string

    :param apply_weights: whether to apply weights and scaling to all histograms.
    :type apply_weights: bool

    :param n_bins: Custom number of bins for plotting,
        defaults to the value configured by the feature when empty.
    :type n_bins: int

    :param systematics: NOT YET IMPLEMENTED. List of custom systematics to be considered.
    :type systematics: csv list

    :param store_systematics: whether to store systematic templates inside the output root files.
    :type store_systematics: bool

    :param remove_horns: NOT YET IMPLEMENTED. Whether to remove the eta horns present in 2017
    :type remove_horns: bool

    :param optimization_method: Optimization method to be used. Only `bayesian_blocks` available.
    :type optimization_method: str

    """

    feature_names = law.CSVParameter(default=(), description="names of features to plot, uses all "
        "features when empty, default: ()")
    feature_tags = law.CSVParameter(default=(), description="list of tags for filtering features "
        "selected via feature_names, default: ()")
    skip_feature_names = law.CSVParameter(default=(), description="names or name pattern of "
        "features to skip, default: ()")
    skip_feature_tags = law.CSVParameter(default=("multiclass_dnn",), description="list of tags of "
        "features to skip, default: (multiclass_dnn,)")
    apply_weights = luigi.BoolParameter(default=True, description="whether to apply "
        "mc weights to histograms, default: True")
    n_bins = luigi.IntParameter(default=law.NO_INT, description="custom number of bins for "
        "plotting, defaults to the value configured by the feature when empty, default: empty")
    # systematics = law.CSVParameter(default=(), description="list of systematics, default: empty")
    store_systematics = luigi.BoolParameter(default=True, description="whether to store "
        "systematic templates inside root files, default: True")
    remove_horns = luigi.BoolParameter(default=False, description="whether to remove horns "
        "from distributions, default: False")
    tree_name = luigi.Parameter(default="Events", description="name of the tree inside "
        "the root file, default: Events (nanoAOD)")
    optimization_method = luigi.ChoiceParameter(default="", choices=("", "bayesian_blocks"),
        significant=False, description="optimization method to be used, default: none")

    def __init__(self, *args, **kwargs):
        super(BasePlotTask, self).__init__(*args, **kwargs)
        # select features
        self.features = self.get_features()

    def get_features(self):
        features = []
        for feature in self.config.features:
            if self.feature_names and not law.util.multi_match(feature.name, self.feature_names):
                continue
            if self.feature_tags and not any([feature.has_tag(tag) for tag in self.feature_tags]):
                continue
            if self.skip_feature_names and \
                    law.util.multi_match(feature.name, self.skip_feature_names):
                continue
            if self.skip_feature_tags and feature.has_tag(self.skip_feature_tags):
                continue
            features.append(feature)
        if len(features) == 0:
            raise ValueError("No features were included. Did you spell them correctly?")
        return features

    def round(self, number):
        if abs(number - round(number)) < 0.0001:
            return number
        if number > 10.:
            return round(number)
        elif number > 1.:
            return round(number, 1)
        i = 1
        while True:
            if number > (1 / (10 ** i)):
                return round(number, i + 1)
            i += 1

    def get_binning(self, feature, ifeat=0):
        if self.optimization_method:
            y_axis_adendum = ""
            binning = self.input()["bin_opt"].collection.targets[ifeat].load(formatter="json")
            n_bins = len(binning) - 1
            binning_args = n_bins, array.array("f", binning)
        elif isinstance(feature.binning, tuple) and len(feature.binning) == 3: # feature.binning is tuple(nbins, min, max)
            if self.n_bins == law.NO_INT:
                nbins = feature.binning[0]
            else:
                nbins = self.n_bins
            y_axis_adendum = (" / %s %s" % (
                self.round((feature.binning[2] - feature.binning[1]) / nbins),
                    feature.get_aux("units")) if feature.get_aux("units") else "")
            binning = (nbins, feature.binning[1], feature.binning[2])
            binning_args = tuple(binning)
        elif feature.binning is not None: # feature.binning is arrayOfBinEdges
            # NB : we ignore self.n_bins in this case
            y_axis_adendum = ""
            binning_args = len(feature.binning)-1, array.array("f", feature.binning)
        else:
            y_axis_adendum = ""
            if self.n_bins == law.NO_INT:
                n_bins = len(feature.binning) - 1
                binning_args = n_bins, array.array("f", feature.binning)
            else:
                n_bins = self.n_bins
                binning_args = tuple(n_bins, feature.binning[0], feature.binning[-1])

        return binning_args, y_axis_adendum

    def get_output_postfix(self):
        postfix = ""
        if self.region:
            postfix += "__" + self.region.name
        if not self.apply_weights:
            postfix += "__noweights"
        if self.optimization_method == "bayesian_blocks":
            postfix += "__opt_bb"
        if self.n_bins != law.NO_INT:
            postfix += "__%s_bins" % self.n_bins
        return postfix

def get_systs_plotting(config, feature:Feature, isMC:bool, category:Category, region:Category=None) -> list[str]:
    """ Compute the list of systematics impacting the given feature, in the given category (and eventually region)"""
    if not isMC:
            return []
    systs = set( # use a set to remove duplicates
        feature.systematics
        + config.get_weights_systematics(config.weights[category.name], isMC, category=category)
        + config.get_systematics_from_expression(category.selection))
    if region:
        systs.update(config.get_systematics_from_expression(region.selection))
    return list(systs)

class PrePlot(RDFModuleTask, ConfigTaskWithCategory, DatasetTask, BasePlotTask, law.LocalWorkflow,
        HTCondorWorkflow, SGEWorkflow, SlurmWorkflow):
    """
    Performs the filling of histograms for all features considered. If systematics are considered,
    it also produces the same histograms after applying those.

    :param skip_processing: whether to skip the preprocessing and categorization steps.
    :type skip_processing: bool
    :param skip_merging: whether to skip the MergeCategorization task.
    :type skip_merging: bool
    :param preplot_modules_file: filename inside ``cmt/config/`` or ``../config/`` (w/o extension)
        with the RDF modules to run.
    :type preplot_modules_file: str
    """

    region_names = law.CSVParameter(description="list of regions to plot in")

    skip_processing = luigi.BoolParameter(default=False, description="whether to skip"
        " preprocessing and categorization steps, default: False")
    skip_categorization = luigi.BoolParameter(default=False, description="whether to skip"
        " categorization steps (input=Preprocess), default: False")
    skip_merging = luigi.BoolParameter(default=False, description="whether to skip"
        " MergeCategorization task, default: False")
    preplot_modules_file = luigi.Parameter(description="filename with modules to run RDataFrame",
        default=law.NO_STR)
    weights_to_skip = law.CSVParameter(description="list of weights to not use for this plotting", default=())
    # dataset_names = law.CSVParameter(description="dataset_names to use for optimization", # seems never used ???
    #     default=())

    def __init__(self, *args, **kwargs):
        super(PrePlot, self).__init__(*args, **kwargs)
        self.custom_output_tag = "_" + "_".join(self.region_names)
        self.threshold = self.dataset.get_aux("event_threshold", None)
        self.merging_factor = self.dataset.get_aux("preprocess_merging_factor", None)
        self.n_files_after_merging = get_n_files_after_merging(self.dataset, self.category)
        self.regions = []
        for region_name in self.region_names:
            if region_name == law.NO_STR or region_name == "":
                self.regions.append(None)
            else:
                self.regions.append(self.config.regions.get(region_name))

        self.syst_list = self.get_syst_list()
        """ List of names of event-changing systematics (those that need dedicated input ntuples) """

    def get_syst_list(self):
        """
        Returns a list of systematic names that affect present or past selections, so dedicated input ntuples are needed as requirements
        Checks:
         - systematics from Feature.systematics 
         - systs from the category selection string (variables in brackets {})
         - systs from the region selection string (variables in brackets)
        Systematics are only considered if current category is in Systematic.affected_categories
        Returns list of systs combining all regions
        """
        if not self.dataset.process.isMC or self.skip_processing:
            return []
        possible_systs = set(
            syst  for feature in self.features for syst in feature.systematics
        )
        possible_systs.update(
            self.config.get_systematics_from_expression(self.category.selection)
        )

        def filter_affected_categories(possible_systs:list[str]):
            l = []
            for syst in possible_systs:
                try:
                    systematic = self.config.systematics.get(syst)
                except: continue
                if self.category.name in systematic.get_aux("affected_categories", []):
                    l.append(syst)
            return l

        non_region_systs = filter_affected_categories(possible_systs)
        total_systs = set(non_region_systs)
        #systs_perRegion = dict()
        for region in self.regions:
            if region is not None:
                systs_region = filter_affected_categories(set(self.config.get_systematics_from_expression(region.selection)))
            else:
                systs_region = set()
            #systs_perRegion[region] = set(non_region_systs)
            #systs_perRegion[region].update(systs_region)
            total_systs.update(systs_region)
        
        return total_systs


    def create_branch_map(self):
        """
        :return: number of files after merging (usually 1) unless skip_processing == True
        :rtype: int
        """
        # number of files of InputData. In a lambda to not compute i unless needed
        input_data_count = lambda : len(self.dataset.get_files(
                os.path.expandvars("$CMT_TMP_DIR/%s/" % self.config_name), add_prefix=False,
                check_empty=True))
        if self.skip_processing:
            return input_data_count()
        elif self.skip_categorization:
            return self.workflow_requires()["data"].branch_map
        elif self.skip_merging:
            # TODO fix this
            categorization_max_events = self.dataset.get_aux("categorization_max_events", None)
            if categorization_max_events is None:
                return input_data_count()
            else:
                # in case we have used the Categorization splitting output
                with open(create_file_dir(os.path.expandvars(
                        "$CMT_TMP_DIR/%s/splitted_branches_categorization_%s/%s.json" % (
                        self.config_name, categorization_max_events, self.dataset.name)))) as f:
                    return len(json.load(f))
        return self.n_files_after_merging # in case we use MergeCategorization

    def workflow_requires(self):
        """
        """
        if self.skip_merging:
            # TODO fix this
            reqs = {"data": {"central": Categorization.vreq(self, workflow="local")}}
            if self.store_systematics:
                for syst, d in itertools.product(self.syst_list, directions):
                    reqs["data"][f"{syst}_{d}"] = Categorization.vreq(self, workflow="local",
                        systematic=syst, systematic_direction=d)
        elif self.skip_categorization:
            systematic_variations = [("central", "central")]
            if self.store_systematics:
                systematic_variations.extend(itertools.product(self.syst_list, directions))
            reqs = {"data" : PreprocessRDF.vreq(self, workflow="local",
                systematic_variations=tuple(systematic_variations))}
        elif self.skip_processing:
            reqs= {"data": {"central": InputData.req(self)}}
        else:
            reqs = {"data": MergeCategorization.vreq(self, systematic_names=["central"] + (list(self.syst_list) if self.store_systematics else []), _exclude=["workflow", "branch"])}
        if self.optimization_method == "bayesian_blocks":
            from cmt.base_tasks.optimization import BayesianBlocksOptimization
            reqs["bin_opt"] = BayesianBlocksOptimization.vreq(self, plot_systematics=False,
                _exclude=["branch", "branches", "custom_output_tag", "plot_systematics", "workflow"])
        return reqs

    def requires(self):
        """
        Each branch requires one input file for the central value and two per systematic considered
        """
        if self.skip_merging:
            reqs = {"central": Categorization.vreq(self, workflow="local", branch=self.branch)} # TODO fix this
            if self.store_systematics:
                for syst, d in itertools.product(self.syst_list, directions):
                    reqs[f"{syst}_{d}"] = Categorization.vreq(self, workflow="local",
                        systematic=syst, systematic_direction=d, branch=self.branch)
        elif self.skip_categorization:
            systematic_variations = [("central", "central")]
            if self.store_systematics:
                systematic_variations.extend(itertools.product(self.syst_list, directions))
            reqs = PreprocessRDF.vreq(self, workflow="local", branch=self.branch,
                systematic_variations=tuple(systematic_variations))
        elif self.skip_processing:
            reqs= {"central": InputData.req(self, file_index=self.branch)}
        else:
            # we just require all branches for simplicity, we could only require the needed  branches
            reqs = {"all_systs" : MergeCategorization.vreq(self, systematic_names=["central"] + (list(self.syst_list) if self.store_systematics else []), _exclude=["workflow", "branch"])}
            # mergeCat_workflow.output_grouped_systematics()
            # reqs = {"central": MergeCategorization.vreq(self, workflow="local", branch=self.branch)}
            # if self.store_systematics:
            #     for syst, d in itertools.product(self.syst_list, directions):
            #         reqs[f"{syst}_{d}"] = MergeCategorization.vreq(self, workflow="local",
            #             systematic=syst, systematic_direction=d, branch=self.branch)
        if self.optimization_method:
            from cmt.base_tasks.optimization import BayesianBlocksOptimization
            reqs["bin_opt"] = BayesianBlocksOptimization.vreq(self, plot_systematics=False,
                _exclude=["branch", "branches", "custom_output_tag", "plot_systematics", "workflow"])
        return reqs

    def get_output_postfix(self, region):
        postfix = ""
        if region is not None:
            postfix += "__" + region.name
        if not self.apply_weights:
            postfix += "__noweights"
        if self.weights_to_skip:
            postfix += "__removeWeights_" + "_".join(self.weights_to_skip)
        if self.optimization_method == "bayesian_blocks":
            postfix += "__opt_bb"
        if self.n_bins != law.NO_INT:
            postfix += "__%s_bins" % self.n_bins
        return postfix
    
    def output(self):
        """
        :return: For each region, one file per input file with all histograms to be plotted for each feature. List [data_etau_os_iso_4.root, data_mutau_os_iso_4.root, ...] if branch=4 for example.
                In case of workflow, will be [[data_etau_os_iso_0.root, data_mutau_os_iso_0.root], [data_etau_os_iso_1.root, data_mutau_os_iso_1.root], ...]
        :rtype: `.root`
        """
        return [self.local_target("data{}_{}.root".format(self.get_output_postfix(region), self.branch)) for region in self.regions]

    def get_weight(self, category, syst_name, syst_direction, **kwargs):
        """
        Obtains the product of all weights depending on the category/channel applied.
        Returns "1" if it's a data sample or the apply_weights parameter is set to False.

        :return: Product of all weights to be applied
        :rtype: str
        """
        if self.config.processes.get(self.dataset.process.name).isData or not self.apply_weights:
            return "1"
        else:
            return self.config.get_weights_expression(
                [w for w in self.config.weights[category] if w not in self.weights_to_skip], syst_name, syst_direction)

    def make_feature_histogram_template(self, feature:Feature, ifeat, syst_tag):
        ROOT = import_root()
        binning_args, y_axis_adendum = self.get_binning(feature, ifeat)
        x_title = (str(feature.get_aux("x_title"))
            + (" [%s]" % feature.get_aux("units") if feature.get_aux("units") else ""))
        y_title = "Events" + y_axis_adendum
        title = f"{feature.name}{syst_tag}; {x_title}; {y_title}" 

        h = ROOT.TH1D(feature.name + syst_tag, title, *binning_args)
        h.SetDirectory(0)
        return h

    @dataclass
    class FeatRegionSysts:
        feature:Feature
        ifeat:int
        region:Category
        syst_name:str
        syst_dir:str
        df_key:str
        feat_expression:str = None
        feat_selection_expression:str = None
    def define_histograms(self, dfs, nentries):
        """ Book the histogram computations
        dfs is Dict syst_key -> region -> RDF node 
        Output is dict(region -> feature name -> list of histograms)
        
        RDF process : 
         - Filter on dataset selection
         - Filter on OR of all regions
         - Define weights : compute all weights needed for all regions 
         - Define region selections
         - Filter feature selection
         - Define feature expressions
         - Filter on region
         - Define histograms
        """
        ROOT = import_root()
        isMC = self.dataset.process.isMC

        def get_df_key(syst_name, syst_dir):
            """ If available, find the input file dedicated to the systematic. Otherwise use the central dataframe"""
            if f"{syst_name}_{syst_dir}" in dfs:
                return f"{syst_name}_{syst_dir}"
            else:
                return "central"

        feat_region_systs = [] # list of FeatRegionSysts, for each histogram to be built ( loop over systematics and up/down variations (both event-changing and weight))
        all_systematics = set() # list of all the systematics that are used for all the features (incl. category and region)
        for ifeat, feature in enumerate(self.features):
            if not isMC and feature.get_aux("noData", False):
                continue

            for region in self.regions:
                systs_directions = [("central", "central")]
                if isMC and self.store_systematics:
                    systs = get_systs_plotting(self.config, feature, isMC, category=self.category, region=region)
                    all_systematics.update(systs)
                else:
                    systs = []
                for syst_name, syst_dir in itertools.chain(systs_directions, itertools.product(systs, directions)):
                    feat_region_systs.append(self.FeatRegionSysts(feature=feature, ifeat=ifeat, region=region, syst_name=syst_name, syst_dir=syst_dir, df_key=get_df_key(syst_name, syst_dir)))


        ### Define weights
        print("Defining weights")
        for syst_name, syst_dir in set((x.syst_name, x.syst_dir) for x in feat_region_systs):
            # define the weight to use. Only compute it once per input file per region per systematic variation
            #if nentries[key] > 0 and f"weight_{key}_{region.name}" not in dfs[key].GetDefinedColumnNames():
            df_key = get_df_key(syst_name, syst_dir)
            if nentries[df_key] > 0:
                dfs[df_key] = dfs[df_key].DefineIfNotAlready(
                    f"weight_{syst_name}_{syst_dir}",
                    self.get_weight(self.category.name, syst_name, syst_dir)
                )
        
        ### Define region selections
        for region in self.regions:
            if region is None: continue
            # we need to define the region selection for every input file, not just for those from the systematics of this region definition
            for syst_name, syst_dir in itertools.chain([("central", "central")], itertools.product(all_systematics, directions)):
                df_key = get_df_key(syst_name, syst_dir)

                if nentries[df_key] > 0:
                    region_selection = self.config.get_object_expression(
                            region.selection,
                            self.dataset.process.isMC, syst_name, syst_dir
                    )
                    dfs[df_key] = dfs[df_key].DefineIfNotAlready(
                        f"region_{region.name}",
                        region_selection
                    )

        dfs_filterFeature = defaultdict(dict)
        """ syst_key -> featureFilter string -> df node
        We can use just feature.selection because for each dataframe there will be only one event changing systematic so no clash
        """
        print("Defining features")
        for x in feat_region_systs:
            if nentries[x.df_key] > 0:
                if x.feature.selection: #  and nentries[key] > 0
                    feat_selection_expression = self.config.get_object_expression(
                        x.feature.selection, isMC, x.syst_name, x.syst_dir)
                    feat_selection_name = f"feature_filter_{make_cpp_identifier(feat_selection_expression)}"
                    if feat_selection_expression not in dfs_filterFeature[x.df_key]:
                        dfs_filterFeature[x.df_key][feat_selection_expression] = dfs[x.df_key].Filter(feat_selection_expression, feat_selection_name)
                    x.feat_selection_expression = feat_selection_expression
                elif None not in dfs_filterFeature[x.df_key]:
                    dfs_filterFeature[x.df_key][None] = dfs[x.df_key] # feature.selection = None
                
                x.feat_expression = self.config.get_object_expression(x.feature, isMC, x.syst_name, x.syst_dir)
                #print(f"Defining feature {x.df_key}-{x.feature.selection}")
                try:
                    dfs_filterFeature[x.df_key][x.feature.selection] = dfs_filterFeature[x.df_key][x.feature.selection].DefineIfNotAlready(
                        f"feature_{x.feature.name}_{make_cpp_identifier(x.feat_expression)}",
                        x.feat_expression)
                except RuntimeError as e: # C++ error
                    raise RuntimeError(f"An error occurred during definition of feature {x.feature.name} with systematics {x.syst_name}-{x.syst_dir}, being accessed from {x.df_key} input.") from e

        print("Defining regions and histos")
        dfs_filterRegion = defaultdict(lambda: defaultdict(dict))
        """ syst_key -> featureFilter string -> region -> """
        histos = defaultdict(lambda: defaultdict(list))
        """ dict region -> feature name -> list of histograms (for saving in output) """
        for x in feat_region_systs:
            if nentries[x.df_key] > 0:
                if x.region not in dfs_filterRegion[x.df_key][x.feat_selection_expression]:
                    if region is not None:
                        dfs_filterRegion[x.df_key][x.feat_selection_expression][x.region] = dfs_filterFeature[x.df_key][x.feature.selection].Filter(
                            f"region_{x.region.name}",
                            f"region_{x.region.name}",
                        )
                    else:
                        dfs_filterRegion[x.df_key][x.feat_selection_expression][x.region] = dfs_filterFeature[x.df_key][x.feature.selection]

            # define tag just for the histogram's name
            if x.syst_name != "central" and isMC:
                tag = "_%s" % x.syst_name
                if x.syst_dir != "":
                    tag += "_%s" % x.syst_dir
            else:
                tag = ""
            hist_base = self.make_feature_histogram_template(x.feature, x.ifeat, tag)

            if nentries[x.df_key] > 0:
                hmodel = ROOT.RDF.TH1DModel(hist_base)
                try:
                    histos[x.region][x.feature.name].append(
                        dfs_filterRegion[x.df_key][x.feat_selection_expression][x.region].rdf._OriginalHisto1D["float"](
                            hmodel,
                            f"feature_{x.feature.name}_{make_cpp_identifier(x.feat_expression)}",
                            f"weight_{x.syst_name}_{x.syst_dir}"
                        )
                    )
                except cppyy.gbl.std.runtime_error as e:
                    raise RuntimeError(f"An error occured during the definition of feature {x.feature.name}, sytematic {x.syst_name}_{x.syst_dir}, input Dataframe syst {x.df_key}.") from e

            else:  # no entries available, append empty histogram
                histos[x.region][x.feature.name].append(hist_base)
        return dict(histos)

    @law.decorator.notify
    @law.decorator.localize(input=False)
    def run(self):
        """
        Creates one RDataFrame per input file, runs the desired RDFModules
        and produces a set of plots per each feature, one for the nominal value
        and others (if available) for all systematics.
        """
        ROOT = import_root()

        # prepare inputs and outputs
        if self.skip_processing:
            inp = self.get_input()
        else:
            inp = self.input()
            reqs = self.requires()
            if "all_systs" in reqs:
                merge_cat_output = reqs["all_systs"].output_grouped_systematics()
        #verbosity = ROOT.Experimental.RLogScopedVerbosity(ROOT.Detail.RDF.RDFLogChannel(), ROOT.Experimental.ELogLevel.kDebug+11)
        ROOT.ROOT.EnableImplicitMT(self.request_cpus)

        dfs = {}
        """ Dict syst_key -> RDataFrame """

        nentries = {}
        for syst_name, syst_dir in itertools.chain([("central", "central")], itertools.product(self.syst_list, ("up", "down"))):
            syst_key = "central" if syst_name == "central" else f"{syst_name}_{syst_dir}"
            #print(syst_key)
            if self.skip_processing:
                inp_to_consider = self.get_path(inp[syst_key])[0]
                if not self.dataset.friend_datasets:
                    dfs[syst_key] = self.RDataFrame(self.tree_name, self.get_path(inp[syst_key]),
                        allow_redefinition=self.allow_redefinition)
                # friend tree
                else:
                    tchain = ROOT.TChain()
                    for f in self.get_path(inp[syst_key]):
                        tchain.Add("{}/{}".format(f, self.tree_name))
                    friend_tchain = ROOT.TChain()
                    for f in self.get_path(inp[syst_key], 1):
                        friend_tchain.Add("{}/{}".format(f, self.tree_name))
                    tchain.AddFriend(friend_tchain, "friend")
                    dfs[syst_key] = self.RDataFrame(tchain, allow_redefinition=self.allow_redefinition)
            elif self.skip_merging or self.skip_categorization:
                if self.skip_merging:
                    inp_to_consider = inp[syst_key]["root"].path
                else:
                    inp_to_consider = inp[syst_name][syst_dir]["root"].path
                dfs[syst_key] = self.RDataFrame(self.tree_name, inp_to_consider,
                    allow_redefinition=self.allow_redefinition)
            else:
                try:
                    inp_to_consider = merge_cat_output.targets[syst_name][syst_dir][self.branch].path
                except KeyError as e:
                    raise RuntimeError(f"Failed finding correct file from MergeCategorization task output. Trying to get [{syst_name}][{syst_dir}][{self.branch}] from {merge_cat_output.targets}") from e
                dfs[syst_key] = self.RDataFrame(self.tree_name, inp_to_consider,
                    allow_redefinition=self.allow_redefinition)

            empty_file = False
            with GuardedTFile(inp_to_consider) as f:
                if f.IsZombie():
                    raise RuntimeError("Categorization : invalid input file " + inp)
                if not self.tree_name in f.GetListOfKeys():
                    # empty input file (no events passed selections in Catgeorization)
                    print("####### WARNING : no events passed selections in Categorization, copying empty input to PrePlot output")
                    nentries[syst_key] = 0
                    empty_file = True
                else:
                    nentries[syst_key] = f.Get(self.tree_name).GetEntries()
                    empty_file = (nentries[syst_key] == 0)

            # applying modules according to the systematic considered
            if not empty_file:
                modules = self.get_feature_modules(self.preplot_modules_file,
                    systematic=syst_name, systematic_direction=syst_dir)
                if len(modules) > 0:
                    for module in modules:
                        dfs[syst_key], _ = module.run(dfs[syst_key])

                selection = "1"
                dataset_selection = self.config.get_object_expression(
                    self.dataset.get_aux("selection", "1"), self.dataset.process.isMC, syst_name, syst_dir)

                if self.skip_processing:
                    selection = self.config.get_object_expression(
                        self.category, self.dataset.process.isMC, syst_name, syst_dir)

                if dataset_selection and dataset_selection != "1":
                    if selection != "1":
                        selection = join_root_selection(dataset_selection, selection, op="and")
                    else:
                        selection = dataset_selection
                
                # OR of region selection used for all regions processed. For performance only, to avoid computing variables for events which will never be used
                if None not in self.regions:
                    region_OR_selection = join_root_selection(list(
                        self.config.get_object_expression(region.selection, self.dataset.process.isMC, syst_name, syst_dir)
                        for region in self.regions
                    ), op="or")
                else:
                    region_OR_selection = "1"
                if region_OR_selection != "1":
                    selection = join_root_selection(selection, region_OR_selection, op="and")

                if selection != "1" and nentries[syst_key] > 0:
                    dfs[syst_key] = dfs[syst_key].Define("selection_dataset", selection).Filter("selection_dataset")

        histos_per_region = self.define_histograms(dfs, nentries)

        print(f"START EVENT LOOP with {self.request_cpus} threads")
        if self.request_cpus > 1: # in case of multithreading, start all the dataframes in parallel
            ROOT.RDF.RunGraphs([df.Count() for df in dfs.values()])

        outp = self.output() # this will return localized targets
        # we need to recover the region correspondence in self.output(), assuming the regions are ordered the same.
        for region, output_target in zip(self.regions, outp):
            histos_per_feature = histos_per_region[region]
            with GuardedTFile(create_file_dir(output_target.path), "RECREATE") as file_out:
                for feature, histos in histos_per_feature.items():
                    feature_dir = file_out.mkdir(feature) # make a folder for each feature, as it avoids having too many histograms in the file which makes it very slow to read
                    for histo in histos:
                        try:
                            histo = histo.GetValue() # trigger the RDataframe computation
                        except AttributeError: pass # in case no entries were in input, then a TH1 is directly created instead of a RResultPtr
                        #histo = histo.Clone()
                        if not histo.GetSumw2N() > 0:
                            histo.Sumw2() # doing that unconditionally prints lots of warnings
                        feature_dir.WriteObject(histo, histo.GetName())

class PrePlotWrapper(DatasetWrapperTask):
    """
    Wrapper task to run PrePlot over multiple categories and regions
    """

    category_names = law.CSVParameter(description="names of categories "
        "whose selection rules are applied")
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    def output(self):
        return { 
            (dataset.name, category_name) : PrePlot.vreq(self, dataset_name=dataset.name, category_name=category_name).output()
            for dataset in self.datasets
            for category_name in self.category_names
        }
    
    def requires(self):
        return { 
            (dataset.name, category_name) : PrePlot.vreq(self, dataset_name=dataset.name, category_name=category_name).requires()
            for dataset in self.datasets
            for category_name in self.category_names
        }
    
    def get_preplot_workflow_args(self, dataset, category_name):
        dataset_workflow_params = dataset.get_aux("preplot_htcondor_workflow_params", {})
        try:
            return dataset_workflow_params[category_name]
        except KeyError: # try to find a similar category
            for key, val in dataset_workflow_params.items():
                if key in category_name:
                    return val
            return {}
    
    def run(self):
        tasks_torun = []
        for dataset in self.datasets:
            for category_name in self.category_names:
                tasks_torun.append(PrePlot.vreq(self, dataset_name=dataset.name, category_name=category_name, **self.get_preplot_workflow_args(dataset, category_name)))
        yield tasks_torun

class MergePrePlot(DatasetTaskWithCategory):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def requires(self):
        return PrePlot.vreq(self, region_names=(self.region.name,))

    def output(self):
        if len(self.requires().branch_map) > 1:
            return self.local_target(f"data{self.requires().get_output_postfix(self.region)}.root")
        else:
            return self.input()["collection"][0][0] # take first region & first branch

    @law.decorator.localize(input=False)
    def run(self):
        print("Merging with hadd...")
        law.root.hadd_task(self, [t[0] for t in self.input()["collection"].targets.values()], self.output(), local=True)
            
class MergePrePlotWrapper(DatasetWrapperTask):
    """
    Wrapper task to run PrePlot over multiple categories and regions
    """
    category_names = law.CSVParameter(description="names of categories "
        "whose selection rules are applied")
    region_names = law.CSVParameter(description="list of regions to plot in")
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    def requires(self):
        return PrePlotWrapper.vreq(self)
    
    def output(self):
        return {
            dataset.name : {
                category_name : law.SiblingFileCollection([MergePrePlot.vreq(self, region_name=region_name, dataset_name=dataset.name, category_name=category_name).output() for region_name in self.region_names])
                for category_name in self.category_names
            } for dataset in self.datasets
        }
    
    def run(self):
        yield [MergePrePlot.vreq(self, region_name=region_name, dataset_name=dataset.name, category_name=category_name)
            for dataset in self.datasets
            for category_name in self.category_names
            for region_name in self.region_names
            ]

def histogram_integral_error(h, include_flow=False):
    error = c_double(0.)
    if include_flow:
        integral = h.IntegralAndError(0, h.GetNbinsX() + 1, error)
    else:
        integral = h.IntegralAndError(1, h.GetNbinsX(), error)
    return integral, error

class FeatureHistogram(ConfigTaskWithCategory, BasePlotTask, QCDABCDTask, ProcessGroupNameTask):
    """
    Makes ROOT histograms, summing the histograms obtained in the PrePlot tasks,
    rescales them if needed.
    Event weights are computed as follows : 
    w_i = (w_i from PrePlot) * cross-section * lumi / (sum of weights from MergeCategorizationStat)
    QCD estimation (from ABCD or sideband) is computed here if requested (--do-qcd)

    :param hide_data: whether to compute (False) or not compute (True) the data histograms
    :type hide_data: bool

    :param avoid_normalization: whether to avoid normalizing by cross section and initial 
        number of events
    :type avoid_normalization: bool

    :param propagate_syst_qcd: whether to propagate systematics to qcd background
    :type propagate_syst_qcd: bool
    """
    from_merge_preplot = luigi.BoolParameter(default=False, description="Use MergePrePlot output instead of PrePlot output, speeds up execution with lots of features & systematics")
    hide_data = luigi.BoolParameter(default=False, description="hide data events, default: True")
    avoid_normalization = luigi.BoolParameter(default=False, description="whether to avoid "
        "normalizing by cross section and initial number of events, default: False")
    propagate_syst_qcd = luigi.BoolParameter(default=True, description="whether to propagate systematics to qcd background, "
        "default: False")
    
    default_process_group_name = "default"

    additional_scaling = {"dummy": 1}  # Temporary fix, the DictParameter fails when empty

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # select processes and datasets
        assert self.process_group_name in self.config.process_group_names, f"Process group names {self.process_group_name} is not defined in config"
        assert not (self.do_qcd and self.do_sideband)

        self.feature_histogram_subclass_for_qcd = FeatureHistogram
        """ Class to use when requesting histograms for QCD control regions. Subclasses can override this to also produce plots for QCD control regions, 
        not just the raw ROOT histograms """

        # get QCD regions when requested
        self.qcd_regions = None
        """ Map from qcd region name to actual Region object. Keys are os_iso,ss_iso,os_inviso,ss_inviso and the special keys for the shape of QCD : 
          - symmetric QCD : shape1 (os_inviso) & shape2 (ss_iso) (the average of the 2 will be used)
          - single QCD : shape (os_inviso)
        """
        if self.do_qcd:
            wp = self.qcd_wp if self.qcd_wp != law.NO_STR else ""
            self.qcd_regions = self.config.get_qcd_regions(region=self.region, category=self.category,
                wp=wp, shape_region=self.shape_region, signal_region_wp=self.qcd_signal_region_wp,
                sym=self.qcd_sym_shape)

            # complain when no data is present
            if not any(dataset.process.isData for dataset in self.datasets):
                raise Exception("no real dataset passed for QCD estimation")
        self.sideband_regions = None
        if self.do_sideband:  # Several fixes may be needed later for this
            assert self.region.name == "signal"
            self.sideband_regions = {key: self.config.regions.get(key)
                for key in ["signal", "background"]}

        # obtain the list of systematics that apply to the normalization only if this is done
        self.norm_syst_list = []
        if self.store_systematics:
            weights = self.config.weights.total_events_weights
            for weight in weights:
                try:
                    feature = self.config.features.get(weight)
                    for syst in feature.systematics:
                        if syst not in self.norm_syst_list:
                            self.norm_syst_list.append(syst)
                except:  # weight not defined as a feature -> no syst available
                    continue

    def requires(self):
        """
        All requirements needed:

            * Histograms coming from the PrePlot task. "data" : {(dataset.name, category.name) :  }

            * Number of total events coming from the MergeCategorizationStats task \
              (to normalize MC histograms).

            * If estimating QCD, HistogramPlot for the three additional QCD regions needed.

        """

        reqs = {}
        if self.from_merge_preplot:
            reqs["data"] = OrderedDict(
                ((dataset.name, category.name), MergePrePlot.vreq(self,
                    dataset_name=dataset.name, category_name=self.get_data_category(category).name, region_name=self.region_name))
                for dataset, category in itertools.product(
                    self.datasets_to_run, self.expand_category())
            )
        else:
            reqs["data"] = OrderedDict(
                ((dataset.name, category.name), PrePlot.vreq(self,
                    dataset_name=dataset.name, category_name=self.get_data_category(category).name, region_names=[self.region_name]))
                for dataset, category in itertools.product(
                    self.datasets_to_run, self.expand_category())
            )
        if not self.avoid_normalization:
            reqs["stats"] = OrderedDict()
            for dataset in self.datasets_to_run:
                if dataset.process.isData:
                    continue
                reqs["stats"][dataset.name] = {}
                for elem in ["central"] + [f"{syst}_{d}"
                        for (syst, d) in itertools.product(self.norm_syst_list, directions)]:
                    syst = ""
                    d = ""
                    if "_" in elem:
                        syst = elem.split("_")[0]
                        d = elem.split("_")[1]
                    if dataset.get_aux("secondary_dataset", None):
                        reqs["stats"][dataset.name][elem] = MergeCategorizationStats.vreq(self,
                            dataset_name=dataset.get_aux("secondary_dataset"),
                            systematic=syst, systematic_direction=d)
                    else:
                        reqs["stats"][dataset.name][elem] = MergeCategorizationStats.vreq(self,
                            dataset_name=dataset.name, systematic=syst, systematic_direction=d)

        if self.do_qcd:
            reqs["qcd"] = {
                key: self.feature_histogram_subclass_for_qcd.req(self, region_name=region.name,
                    do_qcd=False,hide_data=False,_exclude=["shape_region",
                    "qcd_category_name", "qcd_sym_shape", "qcd_signal_region_wp"])
                for key, region in self.qcd_regions.items()
            }
            if self.qcd_category_name != "default":  # to be fixed
                raise ValueError("qcd_category_name is not currently implemented")
                reqs["qcd"]["ss_iso"] = FeaturePlot.vreq(self,
                    region_name=self.qcd_regions.ss_iso.name, category_name=self.qcd_category_name,
                    blinded=False, hide_data=False, do_qcd=False, stack=True, save_root=True,
                    save_pdf=True, save_yields=False, feature_names=(self.qcd_feature,),
                    bin_opt_version=law.NO_STR, remove_horns=self.remove_horns, _exclude=["feature_tags",
                        "shape_region", "qcd_category_name", "qcd_sym_shape", "bin_opt_version",
                        "qcd_signal_region_wp"])
                # reqs["qcd"]["os_inviso"] = FeaturePlot.vreq(self,
                    # region_name=self.qcd_regions.os_inviso.name, category_name=self.qcd_category_name,
                    # blinded=False, hide_data=False, do_qcd=False, stack=True, save_root=True,
                    # save_pdf=True, save_yields=True, feature_names=(self.qcd_feature,),
                    # remove_horns=self.remove_horns, _exclude=["feature_tags", "bin_opt_version"])
                reqs["qcd"]["ss_inviso"] = FeaturePlot.vreq(self,
                    region_name=self.qcd_regions.ss_inviso.name,
                    category_name=self.qcd_category_name,
                    blinded=False, hide_data=False, do_qcd=False, stack=True, save_root=True,
                    save_pdf=True, save_yields=False, feature_names=(self.qcd_feature,),
                    bin_opt_version=law.NO_STR, remove_horns=self.remove_horns, _exclude=["feature_tags",
                        "shape_region", "qcd_category_name", "qcd_sym_shape", "bin_opt_version",
                        "qcd_signal_region_wp"])

        if self.do_sideband:
            reqs["sideband"] = {
                key: self.req(self, region_name=region.name, do_sideband=False,
                    _exclude=["feature_tags", "shape_region",
                    "qcd_category_name", "qcd_sym_shape", "qcd_signal_region_wp"])
                for key, region in self.sideband_regions.items()
            }
        if self.optimization_method:
            from cmt.base_tasks.optimization import BayesianBlocksOptimization
            reqs["bin_opt"] = BayesianBlocksOptimization.vreq(self, plot_systematics=False)
        return reqs

    def get_output_postfix(self, key=None):
        """
        :return: string to be included in the output filenames
        :rtype: str
        """
        postfix = super().get_output_postfix()
        if self.process_group_name != self.default_process_group_name:
            postfix += "__pg_" + self.process_group_name
        if self.do_qcd:
            postfix += "__qcd"
        if self.do_sideband:
            postfix += "__sideband"
        if self.hide_data:
            postfix += "__nodata"
        return postfix

    def output(self):
        """
        Output files to be filled: pdf, png, root or json
        """
        # output definitions, i.e. key, file prefix, extension
        output_data = []
        output_data.append(("root", "root/", "root", dict()))
        output_data.append(("yields", "yields/", "json", dict()))
        return {
            key: law.SiblingFileCollection(OrderedDict(
                (feature.name, self.local_target("{}{}{}.{}".format(
                    prefix, feature.name, self.get_output_postfix(key), ext), **target_kwargs))
                for feature in self.features
            ))
            for key, prefix, ext, target_kwargs in output_data
        }

    def complete(self):
        """
        Task is completed when all output are present
        """
        return ConfigTaskWithCategory.complete(self)

    def get_norm_systematics(self):
        return self.config.get_norm_systematics(self.processes_datasets, self.region)

    def plot(self, feature, ifeat=0):
        """
        Performs the actual plotting.
        """
        ROOT = import_root()

        # helper to extract the qcd shape in a region
        def get_qcd(region, files, syst='', bin_limit=0.):
            """ helper to extract the qcd shape in a region, substracting MC background from data, removing negative bins.
            Any bin with content < bin_limit will be replaced to 1e-6 """
            d_hist = files[region].Get("histograms/" + self.data_names[0])
            if not d_hist:
                raise Exception("data histogram '{}' not found for region '{}' in tfile {}".format(
                    self.data_names[0], region, files[region]))

            b_hists = []
            for b_name in self.background_names:
                b_hist = files[region].Get("histograms/" + b_name + syst)
                if not b_hist:
                    raise Exception("background histogram '{}' not found in region '{}'".format(
                        b_name, region))
                b_hists.append(b_hist)

            qcd_hist = d_hist.Clone(randomize("qcd_" + region + syst))
            for hist in b_hists:
                qcd_hist.Add(hist, -1.) # this will trigger Sumw2 on qcd_hist, transforming Poisson errors to sqrt(N) for the data

            # removing negative bins
            for ibin in range(1, qcd_hist.GetNbinsX() + 1):
                if qcd_hist.GetBinContent(ibin) < bin_limit:
                    qcd_hist.SetBinContent(ibin, 1.e-6)

            return qcd_hist

        # helper to extract data and bkg histos in sideband and signal regions
        def get_sideband(region, files, bin_limit=0.):
            d_hist = files[region].Get("histograms/" + self.data_names[0]).Clone(
                randomize("bkg_" + region))
            if not d_hist:
                raise Exception("data histogram '{}' not found for region '{}' in tfile {}".format(
                    self.data_names[0], region, files[region]))

            b_hist = None
            for b_name in self.background_names:
                if not b_hist:
                    b_hist = files[region].Get("histograms/" + b_name).Clone(
                        randomize("bkg_" + region))
                else:
                    b_hist.Add(files[region].Get("histograms/" + b_name))

            return d_hist, b_hist

        def get_integral_and_error(hist):
            """ Compute the integral of bin contents in histogram h (incl. flow bins), as well as the error as sqrt(sum(sumw2 for every bin)). 
            Also computes compatibility with zero. """
            error = c_double(0.)
            integral = hist.IntegralAndError(0, hist.GetNbinsX() + 1, error)
            error = error.value
            compatible = True if integral <= 0 else False
            # compatible = True if integral - error <= 0 else False
            return integral, error, compatible

        def get_ratio(num, den):
            if den == 0:
                return 0
            return num / den

        # I think these are for the SIGNAL REGION ONLY
        background_hists = self.histos["background"]
        signal_hists = self.histos["signal"]
        data_hists = self.histos["data"]
        all_hists = self.histos["all"]

        # qcd shape files
        qcd_shape_files = None
        """ dict qcd_region -> file for FeatureHistogram of that region. qcd_region can be 'shape' in which case  """
        if self.do_qcd and not feature.get_aux("noData", False):
            qcd_shape_files = {}
            for key, region in self.qcd_regions.items():
                if self.qcd_category_name != "default":
                    my_feature = (feature.name
                        if "shape" in key or \
                            key == f"{self.config.qcd_var1.nominal}_{self.config.qcd_var2.inverted}"
                        else self.qcd_feature)
                else:
                    my_feature = feature.name
                qcd_shape_files[key] = ROOT.TFile.Open(self.input()["qcd"][key]["root"].targets[my_feature].path)

            # do the qcd extrapolation
            if "shape" in qcd_shape_files:
                qcd_hist = get_qcd("shape", qcd_shape_files).Clone(randomize("qcd"))
                n_qcd_hist, n_qcd_hist_error, n_qcd_hist_compatible = get_integral_and_error(qcd_hist)
                if not n_qcd_hist_compatible: # QCD estimation is not zero
                    qcd_hist.Scale(1. / n_qcd_hist)
            else: #sym shape
                qcd_hist = get_qcd("shape1", qcd_shape_files).Clone(randomize("qcd"))
                qcd_hist2 = get_qcd("shape2", qcd_shape_files).Clone(randomize("qcd"))
                n_qcd_hist1, n_qcd_hist1_error, n_qcd_hist1_compatible = get_integral_and_error(qcd_hist)
                n_qcd_hist2, n_qcd_hist2_error, n_qcd_hist2_compatible = get_integral_and_error(qcd_hist2)
                qcd_hist.Scale(1. / n_qcd_hist1)
                qcd_hist2.Scale(1. / n_qcd_hist2)
                qcd_hist.Add(qcd_hist2)
                qcd_hist.Scale(0.5)

            # here the errors are from Poisson uncertainty in data & MC stat uncertainties
            n_os_inviso, n_os_inviso_error, n_os_inviso_compatible = get_integral_and_error(
                get_qcd(f"{self.config.qcd_var1.nominal}_{self.config.qcd_var2.inverted}",
                qcd_shape_files, bin_limit=-999)) # C
            n_ss_iso, n_ss_iso_error, n_ss_iso_compatible = get_integral_and_error(
                get_qcd(f"{self.config.qcd_var1.inverted}_{self.config.qcd_var2.nominal}",
                qcd_shape_files, bin_limit=-999)) # B
            n_ss_inviso, n_ss_inviso_error, n_ss_inviso_compatible = get_integral_and_error(
                get_qcd(f"{self.config.qcd_var1.inverted}_{self.config.qcd_var2.inverted}",
                qcd_shape_files, bin_limit=-999)) # D
            # if not n_ss_iso or not n_ss_inviso:
            if n_os_inviso_compatible or n_ss_iso_compatible or n_ss_inviso_compatible: # one QCD CR has more MC than data
                print("****WARNING: QCD normalization failed (negative yield), Removing QCD!")
                qcd_scaling = 0.
                # qcd_inviso = 0.
                # qcd_inviso_error = 0.
                qcd_hist.Scale(qcd_scaling)
            else:
                # qcd_inviso = n_os_inviso / n_ss_inviso # C/D
                # qcd_inviso_error = qcd_inviso * math.sqrt(
                #     (n_os_inviso_error / n_os_inviso) * (n_os_inviso_error / n_os_inviso)
                #     + (n_ss_inviso_error / n_ss_inviso) * (n_ss_inviso_error / n_ss_inviso)
                #     # + 4 * (n_ss_inviso_error / n_ss_inviso) * (n_ss_inviso_error / n_ss_inviso)
                # )
                qcd_scaling = n_os_inviso * n_ss_iso / n_ss_inviso # C*B/D
                os_inviso_rel_error = n_os_inviso_error / n_os_inviso
                ss_iso_rel_error = n_ss_iso_error / n_ss_iso
                ss_inviso_rel_error = n_ss_inviso_error / n_ss_inviso
                new_errors_sq = []
                for ib in range(1, qcd_hist.GetNbinsX() + 1):
                    if qcd_hist.GetBinContent(ib) > 0:
                        bin_rel_error = qcd_hist.GetBinError(ib) / qcd_hist.GetBinContent(ib) # error on shape
                    else:
                        bin_rel_error = 0
                    new_errors_sq.append(bin_rel_error * bin_rel_error
                        + os_inviso_rel_error * os_inviso_rel_error
                        + ss_iso_rel_error * ss_iso_rel_error
                        + ss_inviso_rel_error * ss_inviso_rel_error)
                qcd_hist.Scale(qcd_scaling)
                # fix errors
                for ib in range(1, qcd_hist.GetNbinsX() + 1):
                    qcd_hist.SetBinError(ib, qcd_hist.GetBinContent(ib)
                        * math.sqrt(new_errors_sq[ib - 1]))

            # store and style
            yield_error = c_double(0.)
            qcd_hist.cmt_yield = qcd_hist.IntegralAndError(
                0, qcd_hist.GetNbinsX() + 1, yield_error)
            qcd_hist.cmt_yield_error = yield_error.value
            qcd_hist.cmt_bin_yield = []
            qcd_hist.cmt_bin_yield_error = []
            for ibin in range(1, qcd_hist.GetNbinsX() + 1):
                qcd_hist.cmt_bin_yield.append(qcd_hist.GetBinContent(ibin))
                qcd_hist.cmt_bin_yield_error.append(qcd_hist.GetBinError(ibin)) # the error on each bin is combination of : (data-MC) stat error from shape region & stat error on event count in B,C&D regions
            qcd_hist.cmt_scale = 1.
            qcd_hist.cmt_process_name = "qcd"
            background_hists.append(qcd_hist)
            all_hists.append(qcd_hist)

            if self.propagate_syst_qcd:
                print("****INFO: Propagating shape uncertainties to QCD")
                """ Computes exactly the same QCD histogram but from the systematic varied QCD control regions """
                for syst_dir in self.histos["shape"].keys():
                    syst_dir_name = "_{}".format(syst_dir)

                    # do the qcd extrapolation
                    if "shape" in qcd_shape_files:
                        qcd_hist_syst = get_qcd("shape", qcd_shape_files, syst=syst_dir_name).Clone(randomize("qcd"))
                        n_qcd_hist, n_qcd_hist_error, n_qcd_hist_compatible = get_integral_and_error(qcd_hist_syst)
                        if not n_qcd_hist_compatible:
                            qcd_hist_syst.Scale(1. / n_qcd_hist)
                    else: #sym shape
                        qcd_hist_syst = get_qcd("shape1", qcd_shape_files, syst=syst_dir_name).Clone(randomize("qcd"))
                        qcd_hist2 = get_qcd("shape2", qcd_shape_files, syst=syst_dir_name).Clone(randomize("qcd"))
                        n_qcd_hist1, n_qcd_hist1_error, n_qcd_hist1_compatible = get_integral_and_error(qcd_hist_syst)
                        n_qcd_hist2, n_qcd_hist2_error, n_qcd_hist2_compatible = get_integral_and_error(qcd_hist2)
                        qcd_hist_syst.Scale(1. / n_qcd_hist1)
                        qcd_hist2.Scale(1. / n_qcd_hist2)
                        qcd_hist_syst.Add(qcd_hist2)
                        qcd_hist_syst.Scale(0.5)

                    n_os_inviso, n_os_inviso_error, n_os_inviso_compatible = get_integral_and_error(
                        get_qcd(f"{self.config.qcd_var1.nominal}_{self.config.qcd_var2.inverted}",
                        qcd_shape_files, syst=syst_dir_name, bin_limit=-999)) # C
                    n_ss_iso, n_ss_iso_error, n_ss_iso_compatible = get_integral_and_error(
                        get_qcd(f"{self.config.qcd_var1.inverted}_{self.config.qcd_var2.nominal}",
                        qcd_shape_files, syst=syst_dir_name, bin_limit=-999)) # B
                    n_ss_inviso, n_ss_inviso_error, n_ss_inviso_compatible = get_integral_and_error(
                        get_qcd(f"{self.config.qcd_var1.inverted}_{self.config.qcd_var2.inverted}",
                        qcd_shape_files, syst=syst_dir_name, bin_limit=-999)) # D
                    # if not n_ss_iso or not n_ss_inviso:
                    if n_os_inviso_compatible or n_ss_iso_compatible or n_ss_inviso_compatible:
                        print(f"****WARNING: QCD normalization failed (negative yield), Removing QCD! for {syst_dir}")
                        qcd_scaling = 0.
                        # qcd_hist_syst.Scale(qcd_scaling)
                        for ib in range(1, qcd_hist_syst.GetNbinsX() + 1):
                            qcd_hist_syst.SetBinContent(ib, 1e-6)
                            #qcd_hist_syst.SetBinError(ib, 1e-6)
                    else:
                        qcd_scaling = n_os_inviso * n_ss_iso / n_ss_inviso # C*B/D
                        os_inviso_rel_error = n_os_inviso_error / n_os_inviso
                        ss_iso_rel_error = n_ss_iso_error / n_ss_iso
                        ss_inviso_rel_error = n_ss_inviso_error / n_ss_inviso
                        new_errors_sq = []
                        for ib in range(1, qcd_hist_syst.GetNbinsX() + 1):
                            if qcd_hist_syst.GetBinContent(ib) > 0:
                                bin_rel_error = qcd_hist_syst.GetBinError(ib) / qcd_hist_syst.GetBinContent(ib)
                            else:
                                bin_rel_error = 0
                            new_errors_sq.append(bin_rel_error * bin_rel_error
                                + os_inviso_rel_error * os_inviso_rel_error
                                + ss_iso_rel_error * ss_iso_rel_error
                                + ss_inviso_rel_error * ss_inviso_rel_error)
                        qcd_hist_syst.Scale(qcd_scaling)
                        # fix errors
                        for ib in range(1, qcd_hist_syst.GetNbinsX() + 1):
                            qcd_hist_syst.SetBinError(ib, qcd_hist_syst.GetBinContent(ib)
                                * math.sqrt(new_errors_sq[ib - 1]))

                    qcd_hist_syst.cmt_process_name = "qcd"
                    self.histos["shape"][syst_dir].append(qcd_hist_syst)

        # sideband files
        sideband_files = None
        if self.do_sideband:
            sideband_files = {}
            for key, region in self.sideband_regions.items():
                my_feature = feature.name
                sideband_files[key] = ROOT.TFile.Open(
                    self.input()["sideband"][key]["root"].targets[my_feature].path)

            # do the qcd extrapolation
            data_hist, bkg_hist_background_region = get_sideband("background", sideband_files)
            _, bkg_hist_signal_region = get_sideband("signal", sideband_files)
            n_bkg_background, n_bkg_background_error, _ = get_integral_and_error(
                bkg_hist_background_region)
            n_bkg_signal, n_bkg_signal_error, _ = get_integral_and_error(
                bkg_hist_signal_region)

            bkg_hist = data_hist.Clone(randomize("bkg"))
            n_bkg_background_rel_error = get_ratio(n_bkg_background_error, n_bkg_background)
            n_bkg_signal_rel_error = get_ratio(n_bkg_signal_error, n_bkg_signal)
            new_errors_sq = []
            for ib in range(1, bkg_hist.GetNbinsX() + 1):
                if bkg_hist.GetBinContent(ib) > 0:
                    bin_rel_error = bkg_hist.GetBinError(ib) / bkg_hist.GetBinContent(ib)
                else:
                    bin_rel_error = 0
                new_errors_sq.append(bin_rel_error * bin_rel_error
                    + n_bkg_background_rel_error * n_bkg_background_rel_error
                    + n_bkg_signal_rel_error * n_bkg_signal_rel_error)
            bkg_hist.Scale(n_bkg_signal / n_bkg_background)
            # fix errors
            for ib in range(1, bkg_hist.GetNbinsX() + 1):
                bkg_hist.SetBinError(ib, bkg_hist.GetBinContent(ib)
                    * math.sqrt(new_errors_sq[ib - 1]))

            # store and style
            yield_error = c_double(0.)
            bkg_hist.cmt_yield = bkg_hist.IntegralAndError(
                0, bkg_hist.GetNbinsX() + 1, yield_error)
            bkg_hist.cmt_yield_error = yield_error.value
            bkg_hist.cmt_bin_yield = []
            bkg_hist.cmt_bin_yield_error = []
            for ibin in range(1, bkg_hist.GetNbinsX() + 1):
                bkg_hist.cmt_bin_yield.append(bkg_hist.GetBinContent(ibin))
                bkg_hist.cmt_bin_yield_error.append(bkg_hist.GetBinError(ibin))
            bkg_hist.cmt_scale = 1.
            bkg_hist.cmt_process_name = "background"
            bkg_hist.SetTitle("Background")
            try:
                bkg_color = ROOT.TColor.GetColor(*self.config.processes.get("background").color)
            except:  # background process not defined in config
                bkg_color = ROOT.kRed
            self.setup_background_hist(bkg_hist, bkg_color)
            background_hists = [bkg_hist]
            all_hists.append(bkg_hist)

        all_hists += data_hists
        
        bkg_histo = None
        for hist in background_hists[::-1]:
            if not bkg_histo:
                bkg_histo = hist.Clone()
            else:
                bkg_histo.Add(hist.Clone())


        f = ROOT.TFile.Open(create_file_dir(
            self.output()["root"].targets[feature.name].path), "RECREATE")
        f.cd()

        hist_dir = f.mkdir("histograms")
        hist_dir.cd()
        for hist in all_hists:
            hist.Write(hist.cmt_process_name)
        if bkg_histo:
            bkg_histo.Write("background")

        if self.store_systematics:
            self.histos["bkg_histo_syst"].Write("bkg_histo_syst")
            for syst_dir, shape_hists in self.histos["shape"].items():
                for hist in shape_hists:
                    hist.Write("%s_%s" % (hist.cmt_process_name, syst_dir))
        f.Close()

        yields = OrderedDict()
        for hist in signal_hists + background_hists + data_hists:
            assert hist.cmt_process_name not in yields
            yields[hist.cmt_process_name] = {
                "Total yield": hist.cmt_yield,
                "Total yield error": hist.cmt_yield_error,
                "Entries": getattr(hist, "cmt_entries", hist.GetEntries()),
                "Binned yield": hist.cmt_bin_yield,
                "Binned yield error": hist.cmt_bin_yield_error,
            }
        if bkg_histo:
            yields["background"] = {
                "Total yield": sum([hist.cmt_yield for hist in background_hists]),
                "Total yield error": math.sqrt(sum([(hist.cmt_yield_error)**2
                    for hist in background_hists])),
                "Entries": getattr(bkg_histo, "cmt_entries", bkg_histo.GetEntries()),
                "Binned yield": [] if feature.get_aux("no_save_bin_yields", False) else [sum([hist.cmt_bin_yield[i] for hist in background_hists])
                    for i in range(0, background_hists[0].GetNbinsX())] ,
                "Binned yield error": [] if feature.get_aux("no_save_bin_yields", False) else [math.sqrt(sum([(hist.cmt_bin_yield_error[i])**2
                    for hist in background_hists]))
                    for i in range(0, background_hists[0].GetNbinsX())],
            }
        with open(create_file_dir(self.output()["yields"].targets[feature.name].path), "w") as f:
            json.dump(yields, f, indent=4)

    def get_nevents(self, inputs=None):
        """ Open MergeCategorizationStats outputs and load json files with nevents and nweightedevents (for normalization) 
        Arguments : inputs : results of self.input(), facultative, for caching
        Returns tuple :
         - nevents (event count or weights depending on self.apply_weights)
         - nweightedevents : sum of weights
         - nunweightedevents : event count
        """
        nevents, nweightedevents, nunweightedevents = {}, {}, {}
        if inputs is None:
            inputs = self.input() # this is quite slow so bring it outside the loop (maybe we could enable cache_requirements ?)
        for iproc, (process, datasets) in enumerate(self.processes_datasets.items()):
            if not process.isData and not self.avoid_normalization:
                for dataset in datasets:
                    nevents[dataset.name] = {}
                    nweightedevents[dataset.name] = {}
                    nunweightedevents[dataset.name] = {}
                    for elem in ["central"] + [f"{syst}_{d}"
                            for (syst, d) in itertools.product(self.norm_syst_list, directions)]:
                        inp = inputs["stats"][dataset.name][elem]
                        with open(inp.path) as f:
                            stats = json.load(f)
                            nweightedevents[dataset.name][elem] = stats["nweightedevents"]
                            nunweightedevents[dataset.name][elem] = stats["nevents"]
                            if self.apply_weights:
                                nevents[dataset.name][elem] = stats["nweightedevents"]
                            else:
                                nevents[dataset.name][elem] = stats["nevents"]
                            
        return nevents, nweightedevents, nunweightedevents

    def get_normalization_factor(self, dataset, elem):
        if not type(self.config.lumi_pb) == dict:
            lumi = self.config.lumi_pb 
        elif self.run_era != "":
            lumi = self.config.lumi_pb[dataset.runPeriod][self.run_era]
        else:
            lumi = sum(self.config.lumi_pb.get(dataset.runPeriod, {}).values())
        
        if dataset.get_aux("stitchingNormalization", False) and self.apply_weights:
            # Normalization for stitched datasets, where generator weights are scaled to their average
            # needs to be combined with appropriate stitching weights
            return dataset.xs * lumi / (self.nweightedevents[dataset.name][elem] / self.nunweightedevents[dataset.name][elem])
        else:
            return dataset.xs * lumi / self.nevents[dataset.name][elem]

    @law.decorator.notify
    @law.decorator.safe_output
    def run(self):
        """
        Splits processes into data, signal and background. Creates histograms from each process
        loading them from the input files. Scales the histograms and applies the correct format
        to them.
        Will populate self.histos with keys :
         - signal : list of histograms that have process.isSignal  in process_group_name
         - data : list of histograms, one for each of the processes in process_group_name that have process.isData
         - background : list of histograms that are in neither of the above, one per process in process_group_name
         - bkg_histo_syst : single histogram
        """
        ROOT = import_root()

        # create root tchains for inputs
        inputs = self.input()

        self.data_names = [p.name for p in self.processes_datasets.keys() if p.isData]
        self.signal_names = [p.name for p in self.processes_datasets.keys() if p.isSignal]
        self.background_names = [p.name for p in self.processes_datasets.keys()
            if not p.isData and not p.isSignal]

        if self.store_systematics:
            systematics = self.get_norm_systematics()

        self.nevents, self.nweightedevents, self.nunweightedevents = self.get_nevents(inputs)

        features = self.features

        systs_directions_perFeature = {}
        for ifeat, feature in enumerate(features):
            systs_directions_perFeature[feature] = [("central", "")]
            if self.store_systematics:
                shape_systematics = get_systs_plotting(self.config, feature, isMC=True, category=self.category, region=self.region)
                systs_directions_perFeature[feature] += list(itertools.product(shape_systematics, directions))

        from tqdm import tqdm
        # Optimization : open all root files just once and copy the histograms
        # then reloop and process them
        # histograms from the same dataset-category-feature are added together on the fly to reduced memory consumption when lots of input files
        input_histograms = defaultdict(lambda:defaultdict(dict))
        """ Nested dict : dataset -> category -> feature_name -> histogram
        nested defaultdict so inner dicts and list are automatically created if they do no exist
        """
        for dataset in tqdm(self.datasets_to_run, desc="Loading histograms"):
            for category in self.expand_category():
                if self.from_merge_preplot:
                    inp = [inputs["data"][(dataset.name, category.name)]] # there is only one input target
                else:
                    try:
                        inp = flatten(inputs["data"][(dataset.name, category.name)].collection.targets)
                    except AttributeError: # in case we use --branch, we get a LocalFileTarget rather than a SiblingFileCollection
                        inp = inputs["data"][(dataset.name, category.name)]
                for elem in inp:
                    with GuardedTFile(elem.path) as rootfile:
                        for ifeat, feature in enumerate(features):
                            if not dataset.process.isMC and feature.get_aux("noData", False):
                                continue
                            for (syst, d) in systs_directions_perFeature[feature]:
                                if syst != "central" and dataset.process.isData:
                                    continue
                                if self.do_sideband and not dataset.process.isData and not dataset.process.isSignal:
                                    continue

                                feature_syst_name = feature.name if syst == "central" else "%s_%s_%s" % (
                                        feature.name, syst, d)
                                try:
                                    feature_dir = rootfile.Get(feature.name)
                                    if not feature_dir:
                                        raise RuntimeError(f"FeaturePlot : Could not find feature {feature_syst_name} in PrePlot output for dataset {dataset.name} (category {category.name}). You should perhaps clear the PrePlot output. PrePlot file : {elem.path}") 
                                    histo = feature_dir.Get(feature_syst_name)
                                except AttributeError: # histo is actually a TH1 thus has no method Get, because this is a PrePlot without any directories
                                    histo = rootfile.Get(feature_syst_name)
                                if not histo:
                                    raise RuntimeError(f"FeaturePlot : Could not find feature {feature_syst_name} in PrePlot output for dataset {dataset.name} (category {category.name}). You should perhaps clear the PrePlot output. PrePlot file : {elem.path}") 
                                if feature_syst_name not in input_histograms[dataset][category]:
                                    histo.SetDirectory(0) # make sure the histogram stays after the file is closed
                                    input_histograms[dataset][category][feature_syst_name] = histo
                                #elif histo.GetEntries() != 0:
                                else:
                                    input_histograms[dataset][category][feature_syst_name].Add(histo)
                            
        for ifeat, feature in enumerate(tqdm(features)):
            binning_args, y_axis_adendum = self.get_binning(feature, ifeat)
            x_title = (str(feature.get_aux("x_title"))
                + (" [%s]" % feature.get_aux("units") if feature.get_aux("units") else ""))
            y_title = "Events" + y_axis_adendum
            hist_title = "; %s; %s" % (x_title, y_title)

            self.histos = {"background": [], "signal": [], "data": [], "all": []}
            """ Dictionnary holding all histograms. central histograms will be either in background, signal, or data. All central *MC* histograms will also be in 'all'. 'shape' is for shape uncertainties, will be a dict "{syst}_{syst_dir}" -> list of all processes """
            if self.store_systematics:
                self.histos["bkg_histo_syst"] = ROOT.TH1D(
                    randomize("syst"), hist_title, *binning_args)
                self.histos["shape"] = {}

            systs_directions = systs_directions_perFeature[feature]

            for (syst, d) in systs_directions:
                feature_name = feature.name if syst == "central" else "%s_%s_%s" % (
                    feature.name, syst, d)
                if syst != "central":
                    self.histos["shape"]["%s_%s" % (syst, d)] = []
                for iproc, (process, datasets) in enumerate(self.processes_datasets.items()):
                    if syst != "central" and process.isData:
                        continue
                    if not process.isMC and feature.get_aux("noData", False):
                        continue
                    if self.do_sideband and not process.isData and not process.isSignal:
                        continue
                    process_histo = ROOT.TH1D(randomize(process.name), hist_title, *binning_args)
                    process_histo.Sumw2()
                    for dataset in datasets:
                        dataset_histo = ROOT.TH1D(randomize("tmp"), hist_title, *binning_args)
                        dataset_histo.Sumw2()
                        for category in self.expand_category():
                            inp_histo = input_histograms[dataset][category][feature_name]
                            if inp_histo.GetEntries() != 0:
                                dataset_histo.Add(inp_histo)
                            if not process.isData and not self.avoid_normalization: # normalization of MC
                                elem = ("central" if syst == "central" or syst not in self.norm_syst_list
                                    else f"{syst}_{d}")
                                if self.nevents[dataset.name][elem] != 0:
                                    dataset_histo.Scale(self.get_normalization_factor(dataset, elem))
                                    scaling = dataset.get_aux("scaling", None)
                                    if scaling:
                                        print(" ### Scaling {} histo by {} +- {}".format(
                                            dataset.name, scaling[0], scaling[1]))
                                        old_errors = [dataset_histo.GetBinError(ibin)\
                                            / dataset_histo.GetBinContent(ibin)
                                            if dataset_histo.GetBinContent(ibin) != 0 else 0
                                            for ibin in range(1, dataset_histo.GetNbinsX() + 1)]
                                        new_errors = [
                                            math.sqrt(elem ** 2 + (scaling[1] / scaling[0]) ** 2)
                                            for elem in old_errors]
                                        dataset_histo.Scale(scaling[0])
                                        for ibin in range(1, dataset_histo.GetNbinsX() + 1):
                                            dataset_histo.SetBinError(
                                                ibin, dataset_histo.GetBinContent(ibin)
                                                    * new_errors[ibin - 1])

                        process_histo.Add(dataset_histo)
                        if self.store_systematics and not process.isData and not process.isSignal \
                                and syst == "central": 
                            dataset_histo_syst = dataset_histo.Clone()
                            for ibin in range(1, dataset_histo_syst.GetNbinsX() + 1):
                                # some datasets may not have any systematics
                                if dataset.name in systematics: # TODO this clearly does not work since systematics is a dict whise keys are normalization systematic names....
                                    dataset_histo_syst.SetBinError(ibin,
                                        float(dataset_histo.GetBinContent(ibin))\
                                            * systematics[dataset.name] # TODO understand why dataset and not process is used and why 
                                    )
                            self.histos["bkg_histo_syst"].Add(dataset_histo_syst)

                    if process.name in self.additional_scaling:
                        process_histo.Scale(self.additional_scaling[process.name])

                    yield_error = c_double(0.)
                    process_histo.cmt_process_name = process.name
                    process_histo.cmt_yield = process_histo.IntegralAndError(0,
                        process_histo.GetNbinsX() + 1, yield_error)
                    process_histo.cmt_yield_error = yield_error.value

                    process_histo.cmt_bin_yield = []
                    process_histo.cmt_bin_yield_error = []
                    if not feature.get_aux("no_save_bin_yields", False): # avoid saving bin yields for some feature that have a lot of bins, as it is time consuming
                        for ibin in range(1, process_histo.GetNbinsX() + 1):
                            process_histo.cmt_bin_yield.append(process_histo.GetBinContent(ibin))
                            process_histo.cmt_bin_yield_error.append(process_histo.GetBinError(ibin))

                    if syst == "central":
                        if process.isSignal:
                            process_histo.hist_type = "signal"
                            self.histos["signal"].append(process_histo)
                        elif process.isData:
                            process_histo.hist_type = "data"
                            process_histo.legend_style = "lp"
                            self.histos["data"].append(process_histo)
                        else:
                            process_histo.hist_type = "background"
                            self.histos["background"].append(process_histo)
                        if not process.isData: #or not self.hide_data:
                           self.histos["all"].append(process_histo)
                    else:
                        self.histos["shape"]["%s_%s" % (syst, d)].append(process_histo)

            self.plot(feature)

class FeatureHistogramWrapper(ConfigTask, law.WrapperTask):
    category_names = law.CSVParameter(description="names of categories "
        "whose selection rules are applied")
    region_names = law.CSVParameter(description="list of regions to plot")
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    def requires(self):
        return { 
            (category_name, region_name) : FeatureHistogram.vreq(self, category_name=category_name, region_name=region_name) 
            for category_name in self.category_names
            for region_name in self.region_names
        }

class EqualBinWidthTransformer:
    """ Helper tool that, given a model histogram, will change the x axis values to make the bins have equal width """
    def __init__(self, h_model):
        self.n_bins = h_model.GetNbinsX()
        self.h_model = h_model
                
    def convert(self, h):
        """ convert the histogram to fixed-width plotting binning """
        assert h.GetNbinsX() == self.n_bins
        new_h = ROOT.TH1D(randomize(h.GetName()), h.GetTitle(), self.n_bins, 0, self.n_bins)
        new_h.Set(h.GetNbinsX()+2, h.GetArray())
        new_h.GetSumw2().__assign__(h.GetSumw2())
        return new_h

    def convert_labels(self, dummy_hist):
        """ Set the histogram labels on the given dummy_hist """
        dummy_hist.GetXaxis().SetNdivisions(self.n_bins, 0, 0, False)
        for label_i in range(2, self.n_bins+2): # labels start at 1. Label=1 is 0 which is already correct
            # try to get enough decimals so that consecutive bin labels are different
            if (f"{self.h_model.GetXaxis().GetBinLowEdge(label_i):.3g}" == f"{self.h_model.GetXaxis().GetBinLowEdge(label_i-1):.3g}") or \
                (f"{self.h_model.GetXaxis().GetBinLowEdge(label_i):.3g}" == f"{self.h_model.GetXaxis().GetBinLowEdge(label_i+1):.3g}") :
                dummy_hist.GetXaxis().ChangeLabel(label_i, -1,-1,-1,-1,-1, f"{self.h_model.GetXaxis().GetBinLowEdge(label_i):.4g}")
            elif (f"{self.h_model.GetXaxis().GetBinLowEdge(label_i):.2g}" == f"{self.h_model.GetXaxis().GetBinLowEdge(label_i-1):.2g}") or \
                (f"{self.h_model.GetXaxis().GetBinLowEdge(label_i):.2g}" == f"{self.h_model.GetXaxis().GetBinLowEdge(label_i+1):.2g}") :
                dummy_hist.GetXaxis().ChangeLabel(label_i, -1,-1,-1,-1,-1, f"{self.h_model.GetXaxis().GetBinLowEdge(label_i):.3g}")
            else:
                dummy_hist.GetXaxis().ChangeLabel(label_i, -1,-1,-1,-1,-1, f"{self.h_model.GetXaxis().GetBinLowEdge(label_i):.2g}")
    
    def convert_labels_ratio(self, dummy_ratio_hist):
        """ not sure why this is different than above function. Probably the 2 could be the same """
        dummy_ratio_hist.GetXaxis().SetNdivisions(self.n_bins, 0, 0, False)
        for label_i in range(2, self.n_bins+2): # labels start at 1. Label=1 is 0 which is already correct
            dummy_ratio_hist.GetXaxis().ChangeLabel(label_i, -1,-1,-1,-1,-1, f"{self.h_model.GetXaxis().GetBinLowEdge(label_i):.2g}")


class HistogramBinMerger:
    """ Algorithm to merge histogram bins whilst keeping enough brackground MC events.
    Uses two successive methods, picking the first one that succeeds.
    Procedure 1: flatten signal distribution, in 10 bins. Check that every bin has at least 10 MC events.
    If failed, repeat procedure with 9 bins etc until 3 bins.
    If still failed, apply procedure 2: find the value X such that the sum across bins higher than X has at least 10 MC events
        and has at least 30% of the signal. Then make a 2-bin histogram split at X. (this is mainly for extreme signal/bkg separation)  
    """
    def __init__(self, signal_histo=None, bkg_histo=None, bins_txt_path=None):
        if signal_histo:
            assert not bins_txt_path
            self._compute_rebinning(signal_histo, bkg_histo)
        else:
            assert bins_txt_path
            self._load_bin_edges(bins_txt_path)

    def _compute_rebinning(self, h, bkg_histo):
        """ Use signal histogram h and background histogram bkg_histo to compute optimal binning """
        print(f" ### INFO: Merging bins for pretty plots, using signal {h.GetTitle()}")
        integral = h.Integral()

        if h.GetBinContent(0)!= 0 or h.GetBinContent(h.GetNbinsX()+1)!=0:
            print(f"## WARNING overflow bins compassing fraction of yield : { (h.GetBinContent(0)+h.GetBinContent(h.GetNbinsX()+1))/h.Integral(0, h.GetNbinsX()+1)}")

        if integral == 0:
            edges = [0, 1.0]
            nbins_real = 1

        else:
            success = False
            for nbins in range(10, 3, -1):
                edges = [1.]
                sig_yield = 0.0
                bkg_yield = 0.0
                bkg_error = 0.0
                quantile = integral
                i_bin = 10
                for i in range(h.GetNbinsX(), 1, -1):
                    if len(edges) == nbins: break
                    sig_yield += h.GetBinContent(i)
                    bkg_yield += bkg_histo.GetBinContent(i)
                    bkg_error += bkg_histo.GetBinError(i)**2
                    try:
                        bkg_stats = bkg_yield**2/bkg_error
                    except:
                        bkg_stats = 0
                    if len(edges) == 1 and bkg_stats < 10: continue
                    if sig_yield >= quantile / i_bin:
                        print(" ### INFO: Adding", h.GetXaxis().GetBinLowEdge(i))
                        edges.append(h.GetXaxis().GetBinLowEdge(i))
                        # compute the remaining yield to be subdivided
                        quantile = quantile - sig_yield
                        # move to the next bin to the left
                        i_bin = i_bin - 1
                        sig_yield = 0.0
                        bkg_yield = 0.0
                        bkg_error = 0.0
                edges.append(0.0)
                edges = edges[::-1]
                nbins_real = len(edges)-1

                # check that there are at least 10 bkg events in all bins
                n_bkg_stats = np.zeros(nbins_real)
                bkg_histo_rebin = bkg_histo.Rebin(nbins_real, f"h_test", np.array(edges))
                for ibin in range(1, nbins_real + 1):
                    try:
                        # number of equivalent unweighted bkg events
                        n_bkg_stats[ibin-1] = (bkg_histo_rebin.GetBinContent(ibin) / bkg_histo_rebin.GetBinError(ibin))**2
                    except:
                        n_bkg_stats[ibin-1] = 0
                n_bkg_passed = [n >= 10 for n in n_bkg_stats]

                if not all(n_bkg_passed):
                    print(" ### INFO: ", nbins, " not passing bkg requirement")
                    continue

                else:
                    print("Procedure 1 success")
                    success = True
                    break
            
            # if the first procedure failed
            if not success:
                print(" Procedure 2 :")
                edges = [1.0]
                sig_yield = 0.0
                bkg_yield = 0.0
                bkg_error = 0.0
                for i in range(h.GetNbinsX(), 1, -1):
                    sig_yield += h.GetBinContent(i)
                    bkg_yield += bkg_histo.GetBinContent(i)
                    bkg_error += bkg_histo.GetBinError(i)**2
                    try:
                        bkg_stats = bkg_yield**2/bkg_error
                    except:
                        bkg_stats = 0
                    #print(bkg_stats, sig_yield/integral)
                    if (bkg_stats > 10 and sig_yield/integral > 0.3):
                        edges.append(h.GetXaxis().GetBinLowEdge(i))
                        print(" ### INFO: Procedure 2 found border at ", h.GetXaxis().GetBinLowEdge(i), \
                                " with ", bkg_stats, " equivalent background events")
                        if len(edges) > 2: break
                        sig_yield = 0.0
                        bkg_yield = 0.0
                        bkg_error = 0.0
                if len(edges) <= 1:
                    raise RuntimeError(f"Procedure 2 failed. Final bkg statistics {bkg_stats}  - Final fraction of signal : {sig_yield/integral}")

                edges.append(0.)
                edges = edges[::-1]
                nbins_real = len(edges)-1
        self.edges = edges
        self.edges_array = np.array(self.edges)
        self.nbins_real = nbins_real
        return edges
    
    def save_bin_edges(self, path):
        np.savetxt(create_file_dir(path), self.edges_array)
    
    def _load_bin_edges(self, path):
        self.edges_array = np.loadtxt(path)
        self.edges = list(self.edges_array)
        self.nbins_real = len(self.edges)-1
    
    def rebin(self, h, inplace=False):
        """ rebin an histogram using the previously computed edges """
        rebin_process_histo = h.Rebin(self.nbins_real, "" if inplace else f"rebin_{h.GetTitle()}", self.edges_array)
        for histo_attr in ["hist_type", "cmt_process_name", "process_label", "legend_style"]:
            try:
                setattr(rebin_process_histo, histo_attr, getattr(h, histo_attr))
            except AttributeError: pass
        return rebin_process_histo

class FeatureHistogramRebin(FeatureHistogram):
    """ Rebins histograms from FeatureHistogram """
    merge_bins_region = luigi.Parameter(default=law.NO_STR, description="If set to a region name, and if merge_bins==True, instead of using the current region "
        "to compute the new binning, will load the binning output from the given region. Typically will put the corresponging os_iso region. "
        "Meant for ex. to plot the variable distribution in QCD CR with the same binning as that optimized for the SR.")
    merge_bins_signal = luigi.Parameter(default=law.NO_STR, description="If merge_bins=True, will use this signal name (process name) for rebinning, in case there are multiple signals. Default: pick first signal (in order of dataset definition)")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def requires(self):
        reqs = {"hists" : FeatureHistogram.vreq(self, _prefer_cli=["dataset_names", "dataset_tags", "skip_dataset_names", "skip_dataset_tags", "dataset_names_after_skip", "feature_names", "feature_tags"])}
        if self.merge_bins_region != law.NO_STR:
            reqs["bins"] = FeatureHistogramRebin.vreq(self, region_name=self.merge_bins_region)
        return reqs

    def get_output_postfix(self, key="pdf"):
        """
        :return: string to be included in the output filenames
        :rtype: str
        """
        postfix = super().get_output_postfix(key)
        if self.merge_bins_region != law.NO_STR:
            postfix += f"__merge_bins_from_{self.merge_bins_region}"
        return postfix

    def output(self):
        """
        Output files to be filled: pdf, png
        """
        # output definitions, i.e. key, file prefix, extension
        output_data = []
        output_data.append(("root", "root/", "root", dict()))
        output_data.append(("bins", "bins/", "txt", dict()))
        return {
            key: law.SiblingFileCollection(OrderedDict(
                (feature.name, self.local_target("{}{}{}.{}".format(
                    prefix, feature.name, self.get_output_postfix(key), ext), **target_kwargs))
                for feature in self.features
            ))
            for key, prefix, ext, target_kwargs in output_data
        } | {"yields" : self.input()["hists"]["yields"]} # adding the yields there for convenience so we can replace FeatureHistogram with FeatureHistogramRebin transparently

    #@law.decorator.safe_output # do not use as that will remove FeatureHistogram yields output !
    def run(self):
        inputs = self.input()
        if self.merge_bins_signal != law.NO_STR:
            signal_process = self.config.processes.get(self.merge_bins_signal)
        else:
            signal_processes = [process for process in self.processes_datasets.keys() if process.isSignal]
            if len(signal_processes) <= 0: raise ValueError(f"FeatureHistogramRebin needs one signal processes. {self.processes_datasets} were provided")
            if len(signal_processes) > 1:print(f"######### WARNING : merge_bins is designed for a single signal. Will pick the first signal. Signals: {signal_processes}")
            signal_process = signal_processes[0]

        for feature in self.features:
            with GuardedTFile(inputs["hists"]["root"].targets[feature.name].path) as f:
                def getFromFile(name):
                    h = f.Get("histograms/" + name)
                    if h:
                        return h
                    else:
                        raise RuntimeError(f"Could not load histogram name {name} from file {f}")
                
                if self.merge_bins_region == law.NO_STR:
                    # do not include QCD in the bkg histogram for bkg statistics, as it does not make sense
                    bkg_histo = None
                    for process in self.processes_datasets.keys():
                        assert process.name != "qcd"
                        if process.isSignal or process.isData: continue
                        print(process.name)
                        new_h = getFromFile(process.name)
                        if bkg_histo is None:
                            bkg_histo = new_h
                        else:
                            bkg_histo.Add(new_h)
                    histogram_bin_merger = HistogramBinMerger(signal_histo=getFromFile(signal_process.name), bkg_histo=bkg_histo)
                else:
                    histogram_bin_merger = HistogramBinMerger(bins_txt_path=inputs["bins"][feature.name].path)

                histogram_bin_merger.save_bin_edges(self.output()["bins"].targets[feature.name].path)

                with GuardedTFile(create_file_dir(self.output()["root"].targets[feature.name].path), "RECREATE") as out_f:
                    in_dir = f.Get("histograms")
                    out_dir = out_f.mkdir("histograms")
                    for hist_key in in_dir.GetListOfKeys():
                        if not hist_key.GetClassName().startswith("TH1"): continue
                        in_hist = in_dir.Get(hist_key.GetName())
                        rebinned_h = histogram_bin_merger.rebin(in_hist, inplace=True)
                        out_dir.WriteObject(rebinned_h, hist_key.GetName())

class FeaturePlot(ConfigTaskWithCategory, BasePlotTask, QCDABCDTask, ProcessGroupNameTask, FitBase):
    """
    Performs the actual histogram plotting: loads the histograms obtained in the PrePlot tasks,
    rescales them if needed and plots and saves them.

    Example command:

    ``law run FeaturePlot --version test --category-name etau --config-name ul_2018 \
--process-group-name etau --feature-names Htt_svfit_mass,lep1_pt,bjet1_pt,lep1_eta,bjet1_eta \
--workers 20 --PrePlot-workflow local --stack --hide-data False --do-qcd --region-name etau_os_iso\
--dataset-names tt_dl,tt_sl,dy_high,wjets,data_etau_a,data_etau_b,data_etau_c,data_etau_d \
--MergeCategorizationStats-version test_old``

    :param stack: whether to show all backgrounds stacked (True) or normalized to 1 (False)
    :type stack: bool


    :param hide_data: whether to show (False) or hide (True) the data histograms
    :type hide_data: bool

    :param normalize_signals: whether to normalize signals to the
        total background yield (True) or not (False)
    :type normalize_signals: bool

    :param avoid_normalization: whether to avoid normalizing by cross section and initial 
        number of events
    :type avoid_normalization: bool

    :param blinded: whether to blind data in specified regions. The blinding ranges are specified
        using the `blinded_range` parameter in the Feature definition. This parameter can include a
        list ([initial, final]) or a list of lists ([[init_1, fin_1], [init_2, fin_2], ...])
    :type blinded: bool

    :param save_png: whether to save plots in png
    :type save_png: bool

    :param save_pdf: whether to save plots in pdf
    :type save_pdf: bool

    :param process_group_name: name of the process grouping name
    :type process_group_name: str

    :param bins_in_x_axis: (NOT YET IMPLEMENTED) whether to plot histograms with the bin numbers
        in the x axis instead of the actual values
    :type bins_in_x_axis: bool

    :param plot_systematics: (NOT YET FULLY IMPLEMENTED) whether to plot histograms
        with their uncertainties
    :type plot_systematics: bool

    :param fixed_colors: whether to plot histograms with their defined colors (False) or fixed colors
        (True) starting from ``ROOT`` color ``2``.
    :type fixed_colors: bool

    :param log_y: whether to set y axis to log scale
    :type log_y: bool

    :param log_x: whether to set y axis to log scale
    :type log_x: bool
    
    :param include_fit: YAML file inside config folder (w/o extension) including input parameters
        for the fit
    :type include_fit: str

    :param run_period: parameter to run only over the datasets for the specified run period of a
        given year. It also allows you to plot the run period with the proper format.
    :type run_period: str

    :param run_era: parameter to run only over the era dataset that has been specified. The lumi labels 
        and the luminosity are also set accordingly.
    :type run_era: str

    """
    plot_qcd_control_regions = luigi.BoolParameter(default=None, description="make png and pdf plots "
        "for the QCD control regions as well, default: True when --do-qcd is True")
    stack = luigi.BoolParameter(default=False, description="when set, stack backgrounds, weight "
        "them with dataset and category weights, and normalize afterwards, default: False")
    show_ratio = luigi.BoolParameter(default=True, description="Allow, in the defined cases, that "
        "data/mc plot is shown, default: True")
    normalize_signals = luigi.BoolParameter(default=False, description="whether to normalize "
        "signals to the total bkg yield, default: True")
    multiply_signals_normalization = luigi.FloatParameter(default=-1., description="multiplicative factor to apply"
        " to signals for normalization. default: -1 (disabled). Not compatible with normalize_signals.")
    hide_signals = luigi.BoolParameter(default=False, description="Do not plot signals")
    blinded = luigi.BoolParameter(default=False, description="whether to blind bins above a "
        "certain feature value. Workfs with the Feature.blind_range attribute, which should be a tuple."
        " Either (min, max) blind range, or ([min, max], [min2, max2]) to blind several ranges. Bin center is used. "
        "default: False")
    save_png = luigi.BoolParameter(default=False, description="whether to save created histograms "
        "in png, default: True")
    save_pdf = luigi.BoolParameter(default=True, description="whether to save created histograms "
        "in pdf, default: True")
    bins_in_x_axis = luigi.BoolParameter(default=False, description="whether to show in the x axis "
        "bin numbers instead of the feature value, default: False")
    plot_systematics = luigi.BoolParameter(default=True, description="whether plot systematics, "
        "default: True")
    fixed_colors = luigi.BoolParameter(default=False, description="whether to use fixed colors "
        "for plotting, default: False")
    log_y = luigi.BoolParameter(default=False, description="set logarithmic scale for Y axis, "
        "default: False")
    histogram_minimum = luigi.FloatParameter(default=-1, description="Minimum value for histogram. Mostly useful for log-y."
        "default: -1 which corresponds to 0 in case log-y=False, 0.01 in case log-y=True")
    log_x = luigi.BoolParameter(default=False, description="set logarithmic scale for X axis, "
        "default: False")
    include_fit = luigi.Parameter(default="", description="fit to be included in the plots, "
        "default: None")
    run_period = luigi.Parameter(default="", description="plot only the specified period, "
        "default: None")
    run_era = luigi.Parameter(default="", description="plot only the specified era, "
        "default: None")
    equal_bin_width = luigi.BoolParameter(default=False, description="make all bins have equal plot width")
    merge_bins = luigi.BoolParameter(default=False, description="whether to merge bins according to "
        "background statistics (or signal flattening), default: False")
    merge_bins_region = luigi.Parameter(default=law.NO_STR, description="If set to a region name, and if merge_bins==True, instead of using the current region "
        "to compute the new binning, will load the binning output from the given region. Typically will put the corresponging os_iso region. "
        "Meant for ex. to plot the variable distribution in QCD CR with the same binning as that optimized for the SR.")
    merge_bins_signal = luigi.Parameter(default=law.NO_STR, description="If merge_bins=True, will use this signal name (process name) for rebinning, in case there are multiple signals. Default: pick first signal (in order of dataset definition)")
    rebin_factor = luigi.IntParameter(default=0, description="Rebin every histogram by this factor (calling TH1::Rebin). Default=0 (no rebinning)")
    
    hide_data = luigi.BoolParameter(default=True, description="hide data events, default: True")
    
    # # optimization parameters
    # bin_opt_version = luigi.Parameter(default=law.NO_STR, description="version of the binning "
        # "optimization task to use, not used when empty, default: empty")
    # n_start_bins = luigi.IntParameter(default=10, description="number of bins to be used "
        # "as initial value for scans, default: 10")
    # threshold = luigi.FloatParameter(default=-1., description="threshold per bin, "
        # "default: -1.")
    # threshold_method = luigi.ChoiceParameter(default="yield",
        # choices=("yield", "raw_events", "tt_dy", "tt_and_dy", "tt_dy_and_yield",
            # "tt_and_dy_and_yield", "rel_error"),
        # significant=False, description="threshold method to be used, default: yield")
    # region_to_optimize = luigi.Parameter(default=law.NO_STR, description="region used for the "
        # "optimization, default: same region as plot")

    # Not implemented yet
    bin_opt_version = None
    # remove_horns = False

    default_process_group_name = "default"

    additional_scaling = {"dummy": 1}  # Temporary fix, the DictParameter fails when empty

    def __init__(self, *args, **kwargs):
        super(FeaturePlot, self).__init__(*args, **kwargs)
        # select processes and datasets
        assert self.process_group_name in self.config.process_group_names, f"Process group names {self.process_group_name} is not defined in config"

        if self.do_qcd and (self.plot_qcd_control_regions is not False):
            # Run FeaturePlot for QCD control regions rather than just FeatureHistogram, so we have the png and pdf
            self.feature_histogram_subclass_for_qcd = FeaturePlot

    def requires(self):
        """
        Requires FeatureHistogram, and eventually rebinning & fit
        """
        reqs = {}
        feature_hists_kwargs = {"_prefer_cli":["hide_data"]}
        if self.plot_systematics:
            feature_hists_kwargs["store_systematics"] = True

        if self.merge_bins:
            reqs["hists"] = FeatureHistogramRebin.vreq(self, **feature_hists_kwargs, merge_bins_region=self.merge_bins_region)
        else:
            reqs["hists"] = FeatureHistogram.vreq(self, **feature_hists_kwargs)
        
        if self.include_fit:
            import yaml
            from cmt.utils.yaml_utils import ordered_load
            with open(self.retrieve_file("config/{}.yaml".format(self.include_fit))) as f:
                fit_params = ordered_load(f, yaml.SafeLoader)

            # what if the process_name use for fitting is not considered in the process_group_name?
            # in principle it should crash, but let's give it a chance by checking whether the
            # process_group_name it's actually a process. In that case, swap the process_name with
            # the process_group_name
            if fit_params["process_name"] not in [p.name for p in self.processes_datasets.keys()]:
                try:
                    process = self.config.processes.get(self.process_group_name)
                    fit_params["process_name"] = self.process_group_name
                except ValueError:
                    raise ValueError(f"{fit_params['process_name']} is not among the processes "
                        f"considered by the process_group_name {self.process_group_name} and "
                        f"{self.process_group_name} is not a process, so the fit can't be included.")

            from cmt.base_tasks.analysis import Fit
            params = ", ".join([f"{param}='{value}'"
                for param, value in fit_params.items() if param != "fit_parameters"])
            if "fit_parameters" in fit_params:
                params += ", fit_parameters={" + ", ".join([f"'{param}': '{value}'"
                for param, value in fit_params["fit_parameters"].items()]) + "}"
            reqs["fit"] = eval(f"Fit.vreq(self, {params}, _exclude=['include_fit'])")
        
        return reqs

    def get_output_postfix(self, key="pdf"):
        """
        :return: string to be included in the output filenames
        :rtype: str
        """
        postfix = super(FeaturePlot, self).get_output_postfix()
        if self.process_group_name != self.default_process_group_name:
            postfix += "__pg_" + self.process_group_name
        if self.do_qcd:
            postfix += "__qcd"
        if self.do_sideband:
            postfix += "__sideband"
        if self.hide_data:
            postfix += "__nodata"
        if self.blinded:
            postfix += "__blinded"
        if self.stack:
            postfix += "__stack"
        if self.include_fit:
            postfix += "__withfit"
        if self.log_y:
            postfix += "__logY"
        if self.log_x:
            postfix += "__logX"
        if self.hide_signals:
            postfix += "__hide_signals"
        elif (self.normalize_signals or self.multiply_signals_normalization > 0.):
            postfix += "__norm_sig"  
        if self.merge_bins:
            postfix += "__merge_bins"
        if self.merge_bins_region != law.NO_STR:
            postfix += f"__merge_bins_from_{self.merge_bins_region}"
        if self.equal_bin_width:
            postfix += "__equalBinWidth"
        if self.rebin_factor > 1:
            postfix += f"__rebin{self.rebin_factor}"
        return postfix

    def output(self):
        """
        Output files to be filled: pdf, png
        """
        # output definitions, i.e. key, file prefix, extension
        output_data = []
        if self.save_pdf:
            output_data.append(("pdf", "", "pdf", dict()))
        if self.save_png:
            output_data.append(("png", "", "png", dict()))
        return {
            key: law.SiblingFileCollection(OrderedDict(
                (feature.name, self.local_target("{}{}{}.{}".format(
                    prefix, feature.name, self.get_output_postfix(key), ext), **target_kwargs))
                for feature in self.features
            ))
            for key, prefix, ext, target_kwargs in output_data
        }

    def complete(self):
        """
        Task is completed when all output are present
        """
        return ConfigTaskWithCategory.complete(self)


    def setup_signal_hist(self, hist, color):
        """
        Method to apply signal format to an histogram
        """
        hist.hist_type = "signal"
        hist.legend_style = "l"
        hist.SetLineColor(color)
        hist.SetLineWidth(3)

    def setup_background_hist(self, hist, color):
        """
        Method to apply background format to an histogram
        """
        ROOT = import_root()
        hist.hist_type = "background"
        if self.stack:
            hist.SetLineColor(ROOT.kBlack)
            hist.SetLineWidth(1)
            hist.SetFillColor(color)
            hist.legend_style = "f"
        else:
            hist.SetLineColor(color)
            hist.legend_style = "l"
            hist.SetLineWidth(3)

    def setup_data_hist(self, hist, color):
        """
        Method to apply data format to an histogram
        """
        ROOT = import_root()
        hist.legend_style = "lp"
        hist.hist_type = "data"
        hist.SetMarkerStyle(20)
        hist.SetMarkerColor(color)
        hist.SetLineColor(color)
        hist.SetBinErrorOption((ROOT.TH1.kPoisson if self.stack else ROOT.TH1.kNormal))

    def plot(self, feature, ifeat=0, equal_bin_width_transformer=None):
        """
        Performs the actual plotting, for one feature. Histograms should be already loaded in self.histos
        """
        ROOT = import_root()
        import plotlib.root as r
        from plotting_tools.root import get_labels, Canvas, RatioCanvas

        # I think these are for the SIGNAL REGION ONLY
        background_hists = self.histos["background"]
        signal_hists = [] if self.hide_signals else self.histos["signal"]
        data_hists = self.histos["data"]
        all_hists = self.histos["all"]
        if self.plot_systematics:
            bkg_histo_syst = self.histos["bkg_histo_syst"]
        

        template_hist = all_hists[0].Clone(randomize("template"))
        template_hist.Reset("M") # clear out everything incl. min & max
        """ histogram meant to be cloned and used as a template for the binning arguments etc """

        n_bins = template_hist.GetNbinsX()
        if self.equal_bin_width:
            x_min, x_max = 0, template_hist.GetNbinsX()
        else:
            x_min, x_max = template_hist.GetXaxis().GetXmin(), template_hist.GetXaxis().GetXmax()
        _, y_axis_adendum = self.get_binning(feature, ifeat)
        x_title = (str(feature.get_aux("x_title"))
            + (" [%s]" % feature.get_aux("units") if feature.get_aux("units") else ""))
        y_title = ("Events" if self.stack else "Normalized Events") + y_axis_adendum
        hist_title = "; %s; %s" % (x_title, y_title)
        

        if not self.hide_data:
            all_hists += data_hists
        

        bkg_histo = None
        data_histo = None

        data_histo = None
        if not self.stack:
            for hist in all_hists:
                scale = 1. / (hist.Integral() or 1.)
                hist.Scale(scale)
                hist.scale = scale
            draw_hists = all_hists[::-1]
            for hist in background_hists[::-1]:
                if not bkg_histo:
                    bkg_histo = hist.Clone()
                else:
                    bkg_histo.Add(hist.Clone())
            for hist in data_hists:
                if not data_histo:
                    data_histo = hist.Clone()
                else:
                    data_histo.Add(hist.Clone())
            if bkg_histo:
                scale = 1. / (bkg_histo.Integral() or 1.)
                bkg_histo.Scale(scale)
                if self.plot_systematics:
                    scale = 1. / (bkg_histo_syst.Integral() or 1.)
                    bkg_histo_syst.Scale(scale)
            if data_histo:
                scale = 1. / (data_histo.Integral() or 1.)
                data_histo.Scale(scale)
        else:
            background_stack = ROOT.THStack(randomize("stack"), "")
            for hist in background_hists[::-1]:
                # hist.SetFillColor(ROOT.kRed)
                background_stack.Add(hist)
                if not bkg_histo:
                    bkg_histo = hist.Clone()
                else:
                    bkg_histo.Add(hist.Clone())
            background_stack.hist_type = "background"

            # Normalize signal histograms to background sum
            if (self.normalize_signals or self.multiply_signals_normalization>0.) and bkg_histo:
                for hist in signal_hists:
                    if self.normalize_signals:
                        signal_yield = hist.Integral()
                        scale = (bkg_histo.Integral() / signal_yield if signal_yield != 0 else 1.)
                    else:
                        scale = self.multiply_signals_normalization
                    hist.Scale(scale)
                    hist.cmt_scale = scale

            draw_hists = [background_stack] + signal_hists[::-1]
            if not self.hide_data:
                # blinding
                blinded_range = feature.get_aux("blinded_range", None)
                if self.blinded and not blinded_range: # blinding last bin only
                    for hist in data_hists:
                        for ib in (hist.GetNbinsX(),):
                            hist.SetBinContent(ib, -999)
                            hist.SetBinError(ib, 0)
                if blinded_range and self.blinded:
                    for hist in data_hists:
                        for ib in range(1, hist.GetNbinsX() + 1):
                            blind = False
                            if isinstance(blinded_range[0], list):
                                for iblinded_range in blinded_range:
                                    if (hist.GetBinCenter(ib) >= iblinded_range[0] and
                                            hist.GetBinCenter(ib) <= iblinded_range[1]):
                                        blind = True
                                        break
                            else:
                                if (hist.GetBinCenter(ib) >= blinded_range[0] and
                                        hist.GetBinCenter(ib) <= blinded_range[1]):
                                    blind = True

                            if blind:
                                hist.SetBinContent(ib, -999)
                                hist.SetBinError(ib, 0)
                                

                draw_hists.extend(data_hists[::-1])
                for hist in data_hists:
                    if not data_histo:
                        data_histo = hist.Clone()
                    else:
                        data_histo.Add(hist.Clone())

        # create a dummy histogram for plotting axes and stuff. clone from one of the process histograms to get the correct binning
        dummy_hist = template_hist.Clone(randomize("dummy"))
        dummy_hist.SetTitle(hist_title)
        # Draw
        self.show_ratio = self.show_ratio and not (self.hide_data or len(data_hists) == 0
            or len(background_hists) == 0)
        if not self.show_ratio:
            c = Canvas()
            if self.log_y:
                c.SetLogy()
            if self.log_x:
                c.SetLogx()
            label_scaling = 1
        else:
            c = RatioCanvas()
            dummy_hist.GetXaxis().SetLabelOffset(100)
            dummy_hist.GetXaxis().SetTitleOffset(100)
            c.get_pad(1).cd()
            if self.log_y:
                c.get_pad(1).SetLogy()
            if self.log_x:
                c.get_pad(1).SetLogx()
            label_scaling = self.config.label_size

        # r.setup_hist(dummy_hist, pad=c.get_pad(1))
        r.setup_hist(dummy_hist)
        if self.show_ratio:
            r.setup_y_axis(dummy_hist.GetYaxis(), pad=c.get_pad(1))
        else: # in case there is a ratio plot, labels are changed on ratio plot later. In case no ratio plot labels are changed here on dummy hist
            if self.equal_bin_width:
                equal_bin_width_transformer.convert_labels(dummy_hist)

        dummy_hist.GetYaxis().SetMaxDigits(4)
        # dummy_hist.GetYaxis().SetTitleOffset(1.22)
        maximum = max([hist.GetMaximum() for hist in draw_hists])
        if self.log_y:
            dummy_hist.SetMaximum(100 * maximum)
            if self.histogram_minimum > 0.:
                dummy_hist.SetMinimum(self.histogram_minimum)
            else:
                dummy_hist.SetMinimum(0.0011)
        else:
            dummy_hist.SetMaximum(1.35 * maximum)
            if self.histogram_minimum > 0.:
                dummy_hist.SetMinimum(self.histogram_minimum)
            else:
                dummy_hist.SetMinimum(0.001)

        # get text to plot inside the figure
        inner_text = self.config.get_inner_text_for_plotting(self.category, self.region)

        if (self.normalize_signals or self.multiply_signals_normalization>0.) and self.stack and signal_hists and bkg_histo:
            scale_text = []
            for hist in signal_hists:
                scale = hist.cmt_scale
                if scale != 1.:
                    if scale < 100:
                        scale = "{:.1f}".format(scale)
                    elif scale < 10000:
                        scale = "{}".format(int(round(scale)))
                    else:
                        scale = "{:.2e}".format(scale).replace("+0", "").replace("+", "")
                    scale_text.append("{} x{}".format(hist.process_label, scale))
            inner_text.append("#scale[0.75]{{{}}}".format(",  ".join(scale_text)))

        if maximum > 1e4 and not self.log_y:
            upper_left_offset = 0.05
        else:
            upper_left_offset = 0.0

        if not (self.hide_data or not len(data_hists) > 0) or self.stack:
            if not type(self.config.lumi_fb) == dict:
                upper_right="{}, {:.1f} ".format(
                    self.config.year,
                    self.config.lumi_fb,
                ) + "fb^{-1} " + "({} TeV)".format(self.config.ecm)
            else:
                if self.run_era != "":
                    era_label = self.run_era
                    lumi_label = self.config.lumi_fb[self.config.get_run_period_from_run_era(self.run_era)][self.run_era]
                else:
                    era_label = self.run_period
                    if self.run_period != "":
                        lumi_label = sum(self.config.lumi_fb.get(self.run_period, {}).values())
                    else:
                        lumi_label = sum(sum(period_dict.values()) for period_dict in self.config.lumi_fb.values())
                upper_right="{} {}, {:.1f} ".format(
                    self.config.year,
                    era_label,
                    lumi_label,
                ) + "fb^{-1} " + "({} TeV)".format(self.config.ecm)
        else:
            upper_right="{} Simulation ({} TeV)".format(
                self.config.year,
                self.config.ecm,
            )

        m = max([hist.GetMaximum() for hist in draw_hists]) if not self.log_y else None

        draw_labels = get_labels(
            upper_left=self.config.upper_left_text,
            upper_left_offset=upper_left_offset,
            upper_right=upper_right,
            scaling=label_scaling,
            inner_text=inner_text,
            max=m
        )

        dummy_hist.Draw()

        for ih, hist in enumerate(draw_hists):
            option = "HIST,SAME" if hist.hist_type != "data" else "P0,E0,SAME" # TODO make this work
            hist.Draw(option)

        for label in draw_labels:
            label.Draw("same")

        # Define entries object to be used later when filling the legend
        # Can be updated with the fits and the uncertainty bands
        entries = [(hist, hist.process_label, hist.legend_style) for hist in all_hists]

        if self.show_ratio:
            dummy_ratio_hist = all_hists[0].Clone(randomize("dummy"))
            dummy_ratio_hist.Reset("M")
            dummy_ratio_hist.SetTitle(hist_title)
            r.setup_hist(dummy_ratio_hist, pad=c.get_pad(2),
                props={"Minimum": 0.25, "Maximum": 1.75})
            r.setup_y_axis(dummy_ratio_hist.GetYaxis(), pad=c.get_pad(2),
                props={"Ndivisions": 3})
            dummy_ratio_hist.GetYaxis().SetTitle("Data / MC")
            dummy_ratio_hist.GetXaxis().SetTitleOffset(3)
            # dummy_ratio_hist.GetYaxis().SetTitleOffset(1.22)

            data_graph = hist_to_graph(data_histo, remove_zeros=False, errors=True,
                asymm=True, overflow=False, underflow=False,
                attrs=["cmt_process_name", "cmt_hist_type", "cmt_legend_style"]) # Poisson errors
            bkg_graph = hist_to_graph(bkg_histo, remove_zeros=False, errors=True,
                asymm=True, overflow=False, underflow=False,
                attrs=["cmt_process_name", "cmt_hist_type", "cmt_legend_style"]) # 

            ratio_graph = ROOT.TGraphAsymmErrors(n_bins)
            mc_unc_graph = ROOT.TGraphErrors(n_bins)
            setattr(mc_unc_graph, "title", "MC")
            r.setup_graph(ratio_graph, props={"MarkerStyle": 20, "MarkerSize": 0.5})
            r.setup_graph(mc_unc_graph, props={"FillStyle": 3004, "LineColor": 0,
                "MarkerColor": 0, "MarkerSize": 0., "FillColor": ROOT.kGray + 2})
            entries.append((mc_unc_graph, mc_unc_graph.title, "f"))
            if self.plot_systematics:
                syst_graph = hist_to_graph(bkg_histo_syst, remove_zeros=False, errors=True,
                    asymm=True, overflow=False, underflow=False,
                    attrs=["cmt_process_name", "cmt_hist_type", "cmt_legend_style"])
                syst_unc_graph = ROOT.TGraphErrors(n_bins)
                setattr(syst_unc_graph, "title", "Norm. syst.")
                r.setup_graph(syst_unc_graph, props={"FillStyle": 3005, "LineColor": 0,
                    "MarkerColor": 0, "MarkerSize": 0., "FillColor": ROOT.kRed + 2})
                # entries.append((syst_unc_graph, syst_unc_graph.title, "f"))
                all_unc_graph = ROOT.TGraphErrors(n_bins)
                setattr(all_unc_graph, "title", "All")
                entries.append((all_unc_graph, all_unc_graph.title, "f"))
                r.setup_graph(all_unc_graph, props={"FillStyle": 3007, "LineColor": 0,
                    "MarkerColor": 0, "MarkerSize": 0., "FillColor": ROOT.kBlue + 2})

            for i in range(n_bins):
                x, d, b = c_double(0.), c_double(0.), c_double(0.)
                data_graph.GetPoint(i, x, d)
                bkg_graph.GetPoint(i, x, b)
                d = d.value
                b = b.value
                if b == 0: # in case d==0 we can still set a ratio uncertainty using Poisson=0 prediction with b
                    ratio_graph.SetPoint(i, x, EMPTY)
                else:
                    ratio_graph.SetPoint(i, x, d / b)
                    ratio_graph.SetPointEYhigh(i, data_graph.GetErrorYhigh(i) / b)
                    ratio_graph.SetPointEYlow(i, data_graph.GetErrorYlow(i) / b)
                # set the mc uncertainty
                if b == 0:
                    mc_unc_graph.SetPoint(i, x, EMPTY)
                else:
                    mc_unc_graph.SetPoint(i, x, 1.)
                    mc_error = bkg_graph.GetErrorYhigh(i) / b
                    mc_unc_graph.SetPointError(i, dummy_ratio_hist.GetBinWidth(i + 1) / 2.,
                        mc_error)
                    if self.plot_systematics:
                    # syst only
                        syst_unc_graph.SetPoint(i, x, 1.)
                        syst_error = syst_graph.GetErrorYhigh(i) / b
                        syst_unc_graph.SetPointError(i, dummy_ratio_hist.GetBinWidth(i + 1) / 2.,
                            syst_error)
                        # syst + stat
                        all_unc_graph.SetPoint(i, x, 1.)
                        tot_unc = math.sqrt(mc_error ** 2 + syst_error ** 2)
                        all_unc_graph.SetPointError(i, dummy_ratio_hist.GetBinWidth(i + 1) / 2.,
                            tot_unc)

            c.get_pad(2).cd()
            if self.equal_bin_width:
                equal_bin_width_transformer.convert_labels_ratio(dummy_ratio_hist)
            dummy_ratio_hist.Draw()
            mc_unc_graph.Draw("2,SAME")
            if not self.hide_data:
                ratio_graph.Draw("PE0Z,SAME") # P:marker, E:errors ?, 0:draw error bars even when point is outside range, Z:do not draw small horizaontal lines at end of error bars. See https://root.cern.ch/doc/master/classTGraphPainter.html
            if self.plot_systematics:
                # syst_unc_graph.Draw("2,SAME")
                all_unc_graph.Draw("2,SAME")

            lines = []
            for y in [0.5, 1.0, 1.5]: # drawing horizontal dashed lines on ratio panel
                l = ROOT.TLine(x_min, y, x_max, y)
                r.setup_line(l, props={"NDC": False, "LineStyle": 3, "LineWidth": 1,
                    "LineColor": 1})
                lines.append(l)
            for line in lines:
                line.Draw("same")

        if self.show_ratio:
            c.get_pad(1).cd()

        # Draw fit if required
        fits = []
        if self.include_fit:
            with open(self.input()["fit"][feature.name]["json"].path) as f:
                d = json.load(f)
            d = d[""]

            fit_task = self.requires()["fit"]
            x_range = fit_task.x_range
            x = ROOT.RooRealVar("x", "x", float(x_range[0]), float(x_range[1]))
            l = ROOT.RooArgList(x)
            xframe = x.frame()
            process_name = fit_task.process_name
            if self.stack:
                for hist in all_hists:
                    if hist.cmt_process_name == process_name:
                        data = ROOT.RooDataHist("data_obs", "data_obs", l, hist)
                        data.plotOn(xframe,
                            ROOT.RooFit.MarkerColor(ROOT.TColor.GetColorTransparent(0, 0.0)),
                            ROOT.RooFit.LineColor(ROOT.TColor.GetColorTransparent(0, 0.0)))

            colors = [2, 3, 4, 6]
            if fit_task.method != "envelope":
                fit, _ = self.get_fit(fit_task.method, d, x)
                fit.plotOn(xframe, ROOT.RooFit.LineColor(colors[0]), ROOT.RooFit.Name(f"{fit_task.method} fit"))
                entries.append((f"{fit_task.method} fit", f"{fit_task.method} fit", "l"))
            else:
                for im, method in enumerate(fit_task.functions):
                    fit, _ = self.get_fit(method.strip(), d, x)
                    fits.append(fit)
                    name = fit_task.functions[im].strip() + " fit"
                    color = colors[im]
                    fits[-1].plotOn(xframe, ROOT.RooFit.LineColor(color), ROOT.RooFit.Name(name))
                    entries.append((name, name, "l"))
            xframe.Draw("same")

        n_entries = len(entries)
        if n_entries <= 4:
            n_cols = 1
            col_width = getattr(self.config, "single_column_width", 0.2)
        elif n_entries <= 8:
            n_cols = 2
            col_width = getattr(self.config, "double_column_width", 0.15)
        else:
            n_cols = 3
            col_width = 0.1
        n_rows = math.ceil(n_entries / float(n_cols))
        row_width = 0.06
        legend_x2 = 0.88
        legend_x1 = legend_x2 - n_cols * col_width
        legend_y2 = 0.88
        legend_y1 = legend_y2 - n_rows * row_width

        legend = ROOT.TLegend(legend_x1, legend_y1, legend_x2, legend_y2)
        legend.SetBorderSize(0)
        legend.SetNColumns(n_cols)
        for entry in entries:
            legend.AddEntry(*entry)
        legend.Draw("same")

        ROOT.gPad.RedrawAxis() # otherwise the tick marks get hidden behind the histograms plotted with SAME

        outputs = []
        if self.save_png:
            outputs.append(self.output()["png"].targets[feature.name].path)
        if self.save_pdf:
            outputs.append(self.output()["pdf"].targets[feature.name].path)
        with root_setIgnoreLevel(ROOT.kWarning):# silence canvas print messages
            for output in outputs:
                c.SaveAs(create_file_dir(output))

    @law.decorator.notify
    @law.decorator.safe_output
    def run(self):
        """ Loads histograms from ROOT files, then plots them as pdf/png """
        ROOT = import_root()
        ROOT.gStyle.SetOptStat(0)

        inputs = self.input()

        if self.fixed_colors:
            colors = list(range(2, 2 + len(self.processes_datasets.keys())))

        
        for ifeat, feature in enumerate(self.features):
            with GuardedTFile(inputs["hists"]["root"].targets[feature.name].path) as f:
                self.histos = {"background": [], "signal": [], "data": [], "all": []}

                def getFromFile(name):
                    h = f.Get("histograms/" + name)
                    if h:
                        return h
                    else:
                        raise RuntimeError(f"Could not load histogram name {name} from file {f}")
                
                if self.equal_bin_width:
                    equal_bin_width_transformer = EqualBinWidthTransformer(getFromFile(next(iter(self.processes_datasets.keys())).name))
                else:
                    equal_bin_width_transformer = None
                
                for iproc, process in enumerate(self.processes_datasets.keys()):
                    if process.isData and self.hide_data:
                        continue
                    if not process.isMC and feature.get_aux("noData", False):
                        continue
                    process_histo = getFromFile(process.name)

                    if self.rebin_factor > 1:
                        process_histo = process_histo.Rebin(self.rebin_factor)

                    if self.equal_bin_width:
                        process_histo = equal_bin_width_transformer.convert(process_histo)

                    process_histo.process_label = str(process.label)
                    process_histo.cmt_process_name = process.name

                    if self.fixed_colors:
                        color = colors[iproc]
                    elif type(process.color) == tuple:
                        color = ROOT.TColor.GetColor(*process.color)
                    else:
                        color = process.color

                    if process.isSignal:
                        self.setup_signal_hist(process_histo, color)
                        self.histos["signal"].append(process_histo)
                    elif process.isData:
                        self.setup_data_hist(process_histo, color)
                        self.histos["data"].append(process_histo)
                    else:
                        self.setup_background_hist(process_histo, color)
                        self.histos["background"].append(process_histo)
                    if not process.isData: #or not self.hide_data:
                        self.histos["all"].append(process_histo)
                
                if self.do_qcd and not feature.get_aux("noData", False):
                    qcd_hist = getFromFile("qcd")
                    qcd_hist.SetTitle("QCD")
                    if self.rebin_factor > 1:
                        qcd_hist = qcd_hist.Rebin(self.rebin_factor)
                    if self.equal_bin_width:
                        qcd_hist = equal_bin_width_transformer.convert(qcd_hist)
                    qcd_hist.process_label = "QCD"
                    qcd_c = tuple([255, 87, 215])
                    qcd_color = ROOT.TColor.GetColor(*qcd_c)
                    self.setup_background_hist(qcd_hist, qcd_color)
                    self.histos["background"].append(qcd_hist)
                    self.histos["all"].append(qcd_hist)
                
                if self.do_sideband:
                    bkg_hist = getFromFile("background")
                    bkg_hist.SetTitle("Background")
                    if self.rebin_factor > 1:
                        bkg_hist = bkg_hist.Rebin(self.rebin_factor)
                    if self.equal_bin_width:
                        bkg_hist = equal_bin_width_transformer.convert(bkg_hist)
                    bkg_hist.process_label = "Background"
                    try:
                        bkg_color = ROOT.TColor.GetColor(*self.config.processes.get("background").color)
                    except:  # background process not defined in config
                        bkg_color = ROOT.kRed
                    self.setup_background_hist(bkg_hist, bkg_color)
                    self.histos["background"].append(bkg_hist)
                    self.histos["all"].append(bkg_hist)

                if self.plot_systematics:
                    bkg_histo_syst = getFromFile("bkg_histo_syst")
                    if self.rebin_factor > 1:
                        bkg_histo_syst = bkg_histo_syst.Rebin(self.rebin_factor)
                    if self.equal_bin_width:
                        bkg_histo_syst = equal_bin_width_transformer.convert(bkg_histo_syst)
                    self.histos["bkg_histo_syst"] = bkg_histo_syst

                # if self.merge_bins:
                #     if len(self.histos["signal"]) > 1: print(f"######### WARNING : merge_bins is designed for a single signal. Will pick the first signal")
                #     if not self.merge_bins_region:
                #         histogram_bin_merger = HistogramBinMerger(signal_histo=self.histos["signal"][0], bkg_histo=getFromFile("background"))
                #     else:
                #         histogram_bin_merger = HistogramBinMerger(bins_txt_path=self.vreq(region_name=self.merge_bins_region).output()["bins"][feature.name].path)

                #     new_histos = {}
                #     for key, h_list in self.histos.items():
                #         if isinstance(h_list, list):
                #             new_histos[key] = [histogram_bin_merger.rebin(h) for h in h_list]
                #         else:
                #             new_histos[key] = histogram_bin_merger.rebin(h_list)
                #     self.histos = new_histos

                #     histogram_bin_merger.save_bin_edges(self.output()["bins"].targets[feature.name].path)

                try:
                    self.plot(feature, equal_bin_width_transformer=equal_bin_width_transformer)
                except Exception as e:
                    raise RuntimeError(f"An exception occured during plotting of feature {feature.name}") from e


class FeaturePlotWrapper(ConfigTask, law.WrapperTask):
    """
    Wrapper task to run FeaturePlot over multiple categories and regions
    """
    category_names = law.CSVParameter(description="names of categories "
        "whose selection rules are applied")
    region_names = law.CSVParameter(description="names of regions "
        "to apply live, default: empty")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def requires(self):
        return {
            (category_name, region_name) : FeaturePlot.vreq(self, category_name=category_name, region_name=region_name)
            for region_name in self.region_names
            for category_name in self.category_names
        }

#####################################################################################################
#####################################################################################################
#####################################################################################################

class FeaturePlotSyst(FeatureHistogram):
    """
    Performs the histogram plotting with up and down variations due to systematics:
    loads the histograms obtained in the FeatureHistogram task, rescales them if needed
    and plots and saves them.

    Example command:

    ``law run FeaturePlotSyst --version test --category-name etau --config-name ul_2018 \
--process-group-name etau --feature-names lep1_pt,lep1_eta \
--workers 20 --PrePlot-workflow local --stack --hide-data False --do-qcd --region-name etau_os_iso\
--dataset-names tt_dl,tt_sl,dy_high,wjets,data_etau_a,data_etau_b,data_etau_c,data_etau_d \
--MergeCategorizationStats-version test_old``

    """
    stack = luigi.BoolParameter(default=False, description="when set, stack backgrounds, weight "
        "them with dataset and category weights, and normalize afterwards, default: False")
    save_png = luigi.BoolParameter(default=False, description="whether to save created histograms "
        "in png, default: True")
    save_pdf = luigi.BoolParameter(default=True, description="whether to save created histograms "
        "in pdf, default: True")
    log_y = luigi.BoolParameter(default=False, description="set logarithmic scale for Y axis, "
        "default: False")
    histogram_minimum = luigi.FloatParameter(default=-1, description="Minimum value for histogram. Mostly useful for log-y."
        "default: -1 which corresponds to 0 in case log-y=False, 0.01 in case log-y=True")
    merge_bins = luigi.BoolParameter(default=False, description="whether to merge bins according to "
        "background statistics (or signal flattening), default: False")
    merge_bins_region = luigi.Parameter(default=law.NO_STR, description="If set to a region name, and if merge_bins==True, instead of using the current region "
        "to compute the new binning, will load the binning output from the given region. Typically will put the corresponging os_iso region. "
        "Meant for ex. to plot the variable distribution in QCD CR with the same binning as that optimized for the SR.")
    equal_bin_width = luigi.BoolParameter(default=False, description="make all bins have equal plot width")
    log_x = luigi.BoolParameter(default=False, description="set logarithmic scale for X axis, "
        "default: False")
    run_period = luigi.Parameter(default="", description="plot only the specified period, "
        "default: None")
    run_era = luigi.Parameter(default="", description="plot only the specified era, "
        "default: None")
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def requires(self):
        """
        Needs as input the root file provided by the FeaturePlot task
        """
        if self.merge_bins:
            return FeatureHistogramRebin.vreq(self, merge_bins_region=self.merge_bins_region)
        else:
            return FeatureHistogram.vreq(self)
    
    def get_output_postfix(self, key="pdf"):
        """
        :return: string to be included in the output filenames
        :rtype: str
        """
        postfix = super().get_output_postfix(key)
        if self.stack:
            postfix += "__stack"
        if self.log_y:
            postfix += "__logY"
        if self.log_x:
            postfix += "__logX"
        if self.merge_bins:
            postfix += "__merge_bins"
        if self.merge_bins_region != law.NO_STR:
            postfix += f"__merge_bins_from_{self.merge_bins_region}"
        if self.equal_bin_width:
            postfix += "__equalBinWidth"
        return postfix

    
    def output(self):
        """
        Output files to be filled: pdf or png
        """

        output_data = []
        if self.save_pdf:
            output_data.append(("pdf", "", "pdf"))
        if self.save_png:
            output_data.append(("png", "", "png"))

        process_names = [process.name for process in self.processes_datasets.keys()
                        if not process.isData]
        if self.do_qcd:
            process_names.append("qcd")

        out = {
            key: law.SiblingFileCollection(OrderedDict(
                ("%s_%s_%s" %(process_name, feature.name, syst),
                    self.local_target("{}{}_{}_{}{}.{}".format(
                    prefix, process_name, feature.name, syst, self.get_output_postfix(key), ext)))
                for feature in self.features
                for syst in get_systs_plotting(self.config, feature, isMC=True, category=self.category, region=self.region)
                for process_name in process_names
            ))
            for key, prefix, ext in output_data
        }

        return out

    def setup_syst_hist(self, hist, dir):
        ROOT = import_root()
        if dir == "central":
            hist.SetFillStyle(0)
            hist.SetLineColor(1)
            hist.legend_style = "l"
            hist.SetLineWidth(2)
        if dir == "up":
            hist.SetFillStyle(0)
            hist.SetLineColor(ROOT.kAzure)
            hist.legend_style = "l"
            hist.SetLineWidth(2)
            # hist.SetLineStyle(4)
        if dir == "down":
            hist.SetFillStyle(0)
            hist.SetLineColor(ROOT.kMagenta)
            hist.legend_style = "l"
            hist.SetLineWidth(2)
            # hist.SetLineStyle(8)

    def get_syst_label(self, shape_syst):
        if self.config.systematics.get(shape_syst).get_aux("label"):
            return self.config.systematics.get(shape_syst).get_aux("label")
        else:
            return shape_syst

    def plot(self, feature, equal_bin_width_transformer=None):
        ROOT = import_root()
        import plotlib.root as r
        from plotting_tools.root import get_labels, Canvas, RatioCanvas

        for process, p_label in zip(self.process_names, self.process_labels):
            # central histogram for each process
            histo_syst_central = self.histos[process]["central"]
            if not histo_syst_central:
                raise RuntimeError(f"Bugged histogram for {process} central")
            self.setup_syst_hist(histo_syst_central, "central")

            for shape_syst in self.shape_syst_list:
                # up and down variation histograms for each systematic
                histo_syst_up = self.histos[process]["%s_up" %shape_syst]
                histo_syst_down = self.histos[process]["%s_down" %shape_syst]
                self.setup_syst_hist(histo_syst_up, "up")
                self.setup_syst_hist(histo_syst_down, "down")

                draw_hists = [histo_syst_central, histo_syst_up, histo_syst_down]

                shape_syst_label = self.get_syst_label(shape_syst)
                entries = [ (histo_syst_central, "Nominal", histo_syst_central.legend_style),
                            (histo_syst_up, shape_syst_label + " Up", histo_syst_up.legend_style),
                            (histo_syst_down, shape_syst_label + " Down", histo_syst_down.legend_style)]

                n_entries = len(entries)
                if n_entries <= 4:
                    n_cols = 1
                elif n_entries <= 8:
                    n_cols = 2
                else:
                    n_cols = 3
                if len(shape_syst_label) < 5:
                    col_width = 0.20
                elif len(shape_syst_label) < 12:
                    col_width = 0.30
                else:
                    col_width = 0.40
                n_rows = math.ceil(n_entries / float(n_cols))
                row_width = 0.06
                legend_x2 = 0.88
                legend_x1 = legend_x2 - n_cols * col_width
                legend_y2 = 0.88
                legend_y1 = legend_y2 - n_rows * row_width

                legend = ROOT.TLegend(legend_x1, legend_y1, legend_x2, legend_y2)
                legend.SetBorderSize(0)
                legend.SetNColumns(1)
                for entry in entries:
                    legend.AddEntry(*entry)

                _, y_axis_adendum = self.get_binning(feature)
                x_title = (str(feature.get_aux("x_title"))
                    + (" [%s]" % feature.get_aux("units") if feature.get_aux("units") else ""))
                y_title = ("Events" if self.stack else "Normalized Events") + y_axis_adendum
                hist_title = "; %s; %s" % (x_title, y_title)
                template_hist = histo_syst_central.Clone(randomize("template"))
                """ histogram meant to be cloned and used as a template for the binning arguments etc """
                template_hist.Reset("M") # clear out everything incl. min & max

                n_bins = template_hist.GetNbinsX()
                if self.equal_bin_width:
                    x_min, x_max = 0, template_hist.GetNbinsX()
                else:
                    x_min, x_max = template_hist.GetXaxis().GetXmin(), template_hist.GetXaxis().GetXmax()

                dummy_hist = template_hist.Clone(randomize("template"))

                # Draw
                c = RatioCanvas()
                dummy_hist.GetXaxis().SetLabelOffset(100)
                dummy_hist.GetXaxis().SetTitleOffset(100)
                c.get_pad(1).cd()
                if self.log_y:
                    c.get_pad(1).SetLogy()
                if self.log_x:
                    c.get_pad(1).SetLogx()
                label_scaling = self.config.label_size

                r.setup_hist(dummy_hist)
                if self.equal_bin_width:
                    equal_bin_width_transformer.convert_labels(dummy_hist)
                r.setup_y_axis(dummy_hist.GetYaxis(), pad=c.get_pad(1))
                dummy_hist.GetYaxis().SetMaxDigits(4)
                dummy_hist.GetYaxis().SetTitleOffset(1.22)
                maximum = max([hist.GetMaximum() for hist in draw_hists])
                if self.log_y:
                    dummy_hist.SetMaximum(100 * maximum)
                    if self.histogram_minimum > 0.:
                        dummy_hist.SetMinimum(self.histogram_minimum)
                    else:
                        dummy_hist.SetMinimum(0.0011)
                else:
                    dummy_hist.SetMaximum(1.25 * maximum)
                    if self.histogram_minimum > 0.:
                        dummy_hist.SetMinimum(self.histogram_minimum)
                    else:
                        dummy_hist.SetMinimum(0.001)

                inner_text=[self.category.label + " category"]
                if self.region:
                    if isinstance(self.region.label, list):
                        inner_text += self.region.label
                    else:
                        inner_text.append(self.region.label)
                inner_text.append(p_label)

                if maximum > 1e4 and not self.log_y:
                    upper_left_offset = 0.05
                else:
                    upper_left_offset = 0.0

                upper_right="{} , {:.1f} ".format(
                    self.config.year,
                    self.config.lumi_fb,
                ) + "fb^{-1} " + "({} TeV)".format(self.config.ecm)

                m = max([hist.GetMaximum() for hist in draw_hists]) if not self.log_y else None

                draw_labels = get_labels(
                    upper_left_offset=upper_left_offset,
                    upper_right=upper_right,
                    scaling=label_scaling,
                    inner_text=inner_text,
                    max=m
                )

                dummy_hist.Draw()
                histo_syst_central.Draw("HIST,SAME")
                histo_syst_up.Draw("HIST,SAME")
                histo_syst_down.Draw("HIST,SAME")

                for label in draw_labels:
                    label.Draw("same")
                legend.Draw("same")

                dummy_ratio_hist = template_hist.Clone(randomize("dummy"))
                dummy_ratio_hist.SetTitle(hist_title)
                r.setup_hist(dummy_ratio_hist, pad=c.get_pad(2),
                    props={"Minimum": 0.75, "Maximum": 1.25})
                r.setup_y_axis(dummy_ratio_hist.GetYaxis(), pad=c.get_pad(2),
                    props={"Ndivisions": 5})
                dummy_ratio_hist.GetYaxis().SetTitle("Ratio")
                dummy_ratio_hist.GetXaxis().SetTitleOffset(3)
                dummy_ratio_hist.GetYaxis().SetTitleOffset(1.22)

                ratio_hist_up = template_hist.Clone(randomize("ratio_hist_up"))
                ratio_hist_up.SetTitle(hist_title)
                ratio_hist_down = template_hist.Clone(randomize("ratio_hist_up"))
                ratio_hist_down.SetTitle(hist_title)
                self.setup_syst_hist(ratio_hist_up, "up")
                self.setup_syst_hist(ratio_hist_down, "down")
                ratio_hist_up.SetLineStyle(0)
                ratio_hist_down.SetLineStyle(0)

                mc_unc_graph = ROOT.TGraphErrors(n_bins)
                r.setup_graph(mc_unc_graph, props={"FillStyle": 3004, "LineColor": 0,
                    "MarkerColor": 0, "MarkerSize": 0., "FillColor": ROOT.kGray + 2})

                for i in range(n_bins+1):
                    x = histo_syst_central.GetBinCenter(i)
                    y = histo_syst_central.GetBinContent(i)
                    sigma_x = histo_syst_central.GetBinWidth(i)/2.
                    sigma_y = histo_syst_central.GetBinError(i)
                    if histo_syst_central.GetBinContent(i) != 0:
                        ratio_hist_up.SetBinContent(i, histo_syst_up.GetBinContent(i)/histo_syst_central.GetBinContent(i))
                        ratio_hist_down.SetBinContent(i, histo_syst_down.GetBinContent(i)/histo_syst_central.GetBinContent(i))
                        mc_unc_graph.SetPoint(i, x, 1.)
                        mc_unc_graph.SetPointError(i, sigma_x, sigma_y/y)
                    else:
                        ratio_hist_up.SetBinContent(i, EMPTY)
                        ratio_hist_down.SetBinContent(i, EMPTY)
                        mc_unc_graph.SetPoint(i, x, EMPTY)

                c.get_pad(2).cd()
                if self.equal_bin_width:
                    equal_bin_width_transformer.convert_labels_ratio(dummy_ratio_hist)
                dummy_ratio_hist.Draw()
                ratio_hist_up.Draw("SAME")
                ratio_hist_down.Draw("SAME")
                mc_unc_graph.Draw("2,SAME")

                lines = []
                for y in [0.5, 1.0, 1.5]:
                    l = ROOT.TLine(x_min, y, x_max, y)
                    r.setup_line(l, props={"NDC": False, "LineStyle": 3, "LineWidth": 1,
                        "LineColor": 1})
                    lines.append(l)
                for line in lines:
                    line.Draw("same")

                outputs = []
                if self.save_png:
                    outputs.append(self.output()["png"].targets["%s_%s_%s" %(process, feature.name, shape_syst)].path)
                if self.save_pdf:
                    outputs.append(self.output()["pdf"].targets["%s_%s_%s" %(process, feature.name, shape_syst)].path)
                with root_setIgnoreLevel(ROOT.kWarning):# silence canvas print messages
                    for output in outputs:
                        c.SaveAs(create_file_dir(output))

    def run(self):
        """
        Splits the processes into data and non-data. Per feature, loads the input histograms,
        creates the output plots with up and down variations.
        """

        ROOT = import_root()
        ROOT.gStyle.SetOptStat(0)
        ROOT.TH1.AddDirectory(False) # whenever we create a new histogram we don't want it to be destroyed when the current file is closed

        inputs = self.input()

        self.process_names = [process.name for process in self.processes_datasets.keys() if not process.isData]
        self.process_labels = [process.label for process in self.processes_datasets.keys() if not process.isData]

        if self.do_qcd:
            self.process_names.append("qcd")
            self.process_labels.append("QCD")
        
        for feature in self.features:
            self.shape_syst_list = get_systs_plotting(self.config, feature, isMC=True, category=self.category, region=self.region) \
                + self.config.get_weights_systematics(self.config.weights[self.category.name], True)

            tf = ROOT.TFile.Open(inputs["root"].targets[feature.name].path)

            def getFromFile(name):
                h = tf.Get("histograms/" + name)
                if h:
                    h.SetDirectory(0) # make sure the histogram is kept when closing the file
                    return h
                else:
                    raise RuntimeError(f"Could not load histogram name {name} from file {tf}")

            if self.equal_bin_width:
                equal_bin_width_transformer = EqualBinWidthTransformer(getFromFile(self.process_names[0]))
                transform = lambda h: equal_bin_width_transformer.convert(h)
            else:
                equal_bin_width_transformer = None
                transform = lambda h:h

            self.histos = {}
            for process in self.process_names:
                self.histos[process] = {}
                self.histos[process]["central"] = transform(getFromFile(process))

                for shape_syst in self.shape_syst_list:
                    self.histos[process]["%s_up" %shape_syst] = transform(getFromFile("%s_%s_up" % (process, shape_syst)))
                    self.histos[process]["%s_down" %shape_syst] = transform(getFromFile("%s_%s_down" % (process, shape_syst)))

            tf.Close()

            self.plot(feature, equal_bin_width_transformer=equal_bin_width_transformer)

#################################################################################################################################
#################################################################################################################################
#################################################################################################################################

class BasePlot2DTask(BasePlotTask):
    def get_features(self):
        features = []
        for feature_pair_name in self.feature_names:
            try:
                feature_pair_names = feature_pair_name.split(":")
            except:
                print("%s cannot be considered for a 2D plot" % feature_pair_name)
                continue
            feature_pair = []
            for feature_name in feature_pair_names:
                for feature in self.config.features:
                    if feature.name == feature_name:
                        feature_pair.append(feature)
                        break
            if len(feature_pair) == 2:
                features.append(tuple(feature_pair))

        if len(features) == 0:
            raise ValueError("No features were included. Did you spell them correctly?")
        return features

    def get_binning(self, feature):
        binning_args = []
        for feature_elem in feature:
            if isinstance(feature_elem.binning, tuple):
                binning_args += feature_elem.binning
            else:
                binning_args += [len(feature_elem.binning) - 1, np.array(feature_elem.binning)]
        return tuple(binning_args), ""


class PrePlot2D(PrePlot, BasePlot2DTask):
    def get_syst_list(self):
        if len(self.region_names) == 0:
            self.region_names = [self.region_name]
        assert len(self.region_names) == 1
        if self.skip_processing:
            return {self.region : []}, []
        syst_list = []
        isMC = self.dataset.process.isMC
        for feature_pair in self.features:
            for feature in feature_pair:
                systs =  get_systs_plotting(self.config, feature, isMC=True, category=self.category, region=self.region)
                for syst in systs:
                    if syst in syst_list:
                        continue
                    try:
                        systematic = self.config.systematics.get(syst)
                        if self.category.name in systematic.get_aux("affected_categories", []):
                            syst_list.append(syst)
                    except:
                        continue
        return {self.region: syst_list}, syst_list

    def define_histograms(self, dfs, nentries):
        """Book the histogram computations
        dfs is Dict syst_key -> region -> RDF node 
        Output is dict(region -> list of histograms)
        """
        ROOT = import_root()
        histos = []
        isMC = self.dataset.process.isMC

        for feature_pair in self.features:
            binning_args, _ = self.get_binning(feature_pair)
            x_title = (str(feature_pair[0].get_aux("x_title"))
                + (" [%s]" % feature_pair[0].get_aux("units")
                if feature_pair[0].get_aux("units") else ""))
            y_title = (str(feature_pair[1].get_aux("x_title"))
                + (" [%s]" % feature_pair[1].get_aux("units")
                if feature_pair[1].get_aux("units") else ""))

            title = "; %s; %s; Events" % (x_title, y_title)
            systs = list(set(get_systs_plotting(self.config, feature_pair[0], isMC=True, category=self.category, region=self.region)
                +get_systs_plotting(self.config, feature_pair[1], isMC=True, category=self.category, region=self.region) 
            ))
            systs_directions = [("central", "")]
            if isMC and self.store_systematics:
                systs_directions += list(itertools.product(systs, directions))

            # loop over systematics and up/down variations
            for syst_name, direction in systs_directions:
                # Select the appropriate RDF (input file) depending on the syst and direction
                key = "central"
                if f"{syst_name}_{direction}" in dfs:
                    key = f"{syst_name}_{direction}"
                df = dfs[key]#[self.region_name]

                # apply selection if needed
                feat_df = df
                for ifeat, feature in enumerate(feature_pair):
                    if feature.selection:
                        feat_df = feat_df.Define(
                            "feat%s_selection" % ifeat, self.config.get_object_expression(
                                feature.selection, isMC, syst_name, direction)).Filter(
                            "feat%s_selection" % ifeat)

                # define tag just for histograms's name
                if syst_name != "central" and isMC:
                    tag = "_%s" % syst_name
                    if direction != "":
                        tag += "_%s" % direction
                else:
                    tag = ""
                feature_expressions = [
                    self.config.get_object_expression(
                        feature_pair[0], isMC, syst_name, direction),
                    self.config.get_object_expression(
                        feature_pair[1], isMC, syst_name, direction),
                ]
                feature_name = "_".join([feature.name for feature in feature_pair]) + tag
                hist_base = ROOT.TH2D(feature_name, title, *binning_args)
                if nentries[key] > 0:
                    hmodel = ROOT.RDF.TH2DModel(hist_base)
                    histos.append(
                        feat_df.Define(
                            "weight", "{}".format(self.get_weight(
                                self.category.name, syst_name, direction))
                            ).Define("var1", feature_expressions[0]).Define("var2", feature_expressions[1]
                            ).Histo2D(hmodel, "var1", "var2", "weight")
                    )
                else:  # no entries available, append empty histogram
                    histos.append(hist_base)
        return {self.region : histos}


class FeaturePlot2D(FeaturePlot, BasePlot2DTask):

    """
    Performs the actual histogram plotting: loads the histograms obtained in the PrePlot2D tasks,
    rescales them if needed and plots and saves them.

    Example command:

    ``law run FeaturePlot2D --version test --category-name etau --config-name ul_2018 \
--process-group-name etau --feature-names lep1_pt:lep1_eta,Hbb_mass:Htt_svfit_mass \
--workers 20 --PrePlot2D-workflow local --stack --hide-data False --do-qcd --region-name etau_os_iso\
--dataset-names tt_dl,tt_sl,dy_high,wjets,data_etau_a,data_etau_b,data_etau_c,data_etau_d \
--MergeCategorizationStats-version test_old``

    :param log_z: whether to set y axis to log scale
    :type log_z: bool
    """

    log_z = luigi.BoolParameter(default=False, description="set logarithmic scale for Z axis, "
        "default: False")

    def requires(self):
        """
        All requirements needed:

            * Histograms coming from the PrePlot2D task.

            * Number of total events coming from the MergeCategorizationStats task \
              (to normalize MC histograms).

            * If estimating QCD, FeaturePlot2D for the three additional QCD regions needed.

        """

        reqs = {}
        reqs["data"] = OrderedDict(
            ((dataset.name, category.name), PrePlot2D.vreq(self,
                dataset_name=dataset.name, category_name=self.get_data_category(category).name, region_names=[self.region_name]))
            for dataset, category in itertools.product(
                self.datasets_to_run, self.expand_category())
        )
        if not self.avoid_normalization:
            reqs["stats"] = OrderedDict()
            for dataset in self.datasets_to_run:
                if dataset.process.isData:
                    continue
                reqs["stats"][dataset.name] = {}
                for elem in ["central"] + [f"{syst}_{d}"
                        for (syst, d) in itertools.product(self.norm_syst_list, directions)]:
                    syst = ""
                    d = ""
                    if "_" in elem:
                        syst = elem.split("_")[0]
                        d = elem.split("_")[1]
                    if dataset.get_aux("secondary_dataset", None):
                        reqs["stats"][dataset.name][elem] = MergeCategorizationStats.vreq(self,
                            dataset_name=dataset.get_aux("secondary_dataset"),
                            systematic=syst, systematic_direction=d)
                    else:
                        reqs["stats"][dataset.name][elem] = MergeCategorizationStats.vreq(self,
                            dataset_name=dataset.name, systematic=syst, systematic_direction=d)

        if self.do_qcd:
            reqs["qcd"] = {
                key: self.req(self, region_name=region.name, blinded=False, hide_data=False,
                    do_qcd=False, stack=True, save_pdf=True,
                    remove_horns=False,_exclude=["feature_tags", "shape_region",
                    "qcd_category_name", "qcd_sym_shape", "qcd_signal_region_wp"])
                for key, region in self.qcd_regions.items()
            }
            if self.qcd_category_name != "default":
                reqs["qcd"]["ss_iso"] = FeaturePlot2D.vreq(self,
                    region_name=self.qcd_regions.ss_iso.name, category_name=self.qcd_category_name,
                    blinded=False, hide_data=False, do_qcd=False, stack=True,  
                    save_pdf=True, feature_names=(self.qcd_feature,),
                    bin_opt_version=law.NO_STR, remove_horns=self.remove_horns, _exclude=["feature_tags",
                        "shape_region", "qcd_category_name", "qcd_sym_shape", "bin_opt_version",
                        "qcd_signal_region_wp"])
                # reqs["qcd"]["os_inviso"] = FeaturePlot2D.vreq(self,
                    # region_name=self.qcd_regions.os_inviso.name, category_name=self.qcd_category_name,
                    # blinded=False, hide_data=False, do_qcd=False, stack=True,  
                    # save_pdf=True, feature_names=(self.qcd_feature,),
                    # remove_horns=self.remove_horns, _exclude=["feature_tags", "bin_opt_version"])
                reqs["qcd"]["ss_inviso"] = FeaturePlot2D.vreq(self,
                    region_name=self.qcd_regions.ss_inviso.name,
                    category_name=self.qcd_category_name,
                    blinded=False, hide_data=False, do_qcd=False, stack=True,  
                    save_pdf=True, feature_names=(self.qcd_feature,),
                    bin_opt_version=law.NO_STR, remove_horns=self.remove_horns, _exclude=["feature_tags",
                        "shape_region", "qcd_category_name", "qcd_sym_shape", "bin_opt_version",
                        "qcd_signal_region_wp"])

        return reqs

    def output(self):
        """
        Output files to be filled: pdf, png, root or json
        """
        def get_feature_name(feature_pair):
            return "_".join([feature.name for feature in feature_pair])

        # output definitions, i.e. key, file prefix, extension
        output_data = []
        output_data.append(("root", "root/", "root"))
        output_data.append(("yields", "yields/", "json"))
        out = {
            key: law.SiblingFileCollection(OrderedDict(
                (get_feature_name(feature_pair), self.local_target("{}{}{}.{}".format(
                    prefix, get_feature_name(feature_pair),
                    self.get_output_postfix(key), ext)))
                for feature_pair in self.features
            ))
            for key, prefix, ext in output_data
        }
        output_data = []
        if self.save_pdf:
            output_data.append(("pdf", "", "pdf"))
        if self.save_png:
            output_data.append(("png", "", "png"))
        out.update({
            key: law.SiblingFileCollection(OrderedDict(
                (get_feature_name(feature_pair), OrderedDict(
                    (process.name, self.local_target("{}{}{}_{}.{}".format(
                        prefix, get_feature_name(feature_pair),
                        self.get_output_postfix(key), process.name, ext)))
                    for process in self.processes_datasets
                    if not process.isData or not self.hide_data
                ))
                for feature_pair in self.features
            ))
            for key, prefix, ext in output_data
        })
        return out

    def plot(self, feature):
        """
        Performs the actual plotting.
        """
        ROOT = import_root()
        import plotlib.root as r
        from plotting_tools.root import get_labels, Canvas, RatioCanvas

        # helper to extract the qcd shape in a region
        def get_qcd(region, files, bin_limit=0.):
            d_hist = files[region].Get("histograms/" + self.data_names[0])
            if not d_hist:
                raise Exception("data histogram '{}' not found for region '{}' in tfile {}".format(
                    self.data_names[0], region, files[region]))

            b_hists = []
            for b_name in self.background_names:
                b_hist = files[region].Get("histograms/" + b_name)
                if not b_hist:
                    raise Exception("background histogram '{}' not found in region '{}'".format(
                        b_name, region))
                b_hists.append(b_hist)

            qcd_hist = d_hist.Clone(randomize("qcd_" + region))
            for hist in b_hists:
                qcd_hist.Add(hist, -1.)

            # removing negative bins
            for ibinx, ibiny in itertools.product(
                    range(1, qcd_hist.GetNbinsX() + 1),
                    range(1, qcd_hist.GetNbinsY() + 1)):
                if qcd_hist.GetBinContent(ibinx, ibiny) < bin_limit:
                    qcd_hist.SetBinContent(ibinx, ibiny, 1.e-6)

            return qcd_hist

        def get_integral_and_error(hist):
            error = c_double(0.)
            integral = hist.IntegralAndError(
                0, hist.GetNbinsX() + 1,
                0, hist.GetNbinsY() + 1, error)
            error = error.value
            compatible = True if integral <= 0 else False
            # compatible = True if integral - error <= 0 else False
            return integral, error, compatible

        background_hists = self.histos["background"]
        signal_hists = self.histos["signal"]
        data_hists = self.histos["data"]
        all_hists = self.histos["all"]
        if self.plot_systematics:
            bkg_histo_syst = self.histos["bkg_histo_syst"]

        binning_args, _ = self.get_binning(feature)
        x_title = (str(feature[0].get_aux("x_title"))
            + (" [%s]" % feature[0].get_aux("units") if feature[0].get_aux("units") else ""))
        y_title = (str(feature[1].get_aux("x_title"))
            + (" [%s]" % feature[1].get_aux("units") if feature[1].get_aux("units") else ""))
        z_title = ("Events" if self.stack else "Normalized Events")
        hist_title = "; %s; %s; %s" % (x_title, y_title, z_title)

        feature_pair_name = "_".join([elem.name for elem in feature])
        # qcd shape files
        qcd_shape_files = None
        if self.do_qcd:
            qcd_shape_files = {}
            for key, region in self.qcd_regions.items():
                if self.qcd_category_name != "default":
                    my_feature = (feature_pair_name if "shape" in key or key == "os_inviso"
                        else self.qcd_feature)
                else:
                    my_feature = feature_pair_name
                qcd_shape_files[key] = ROOT.TFile.Open(self.input()["qcd"][key]["root"].targets[my_feature].path)

            # do the qcd extrapolation
            if "shape" in qcd_shape_files:
                qcd_hist = get_qcd("shape", qcd_shape_files).Clone(randomize("qcd"))
                qcd_hist.Scale(1. / qcd_hist.Integral())
            else:  #sym shape
                qcd_hist = get_qcd("shape1", qcd_shape_files).Clone(randomize("qcd"))
                qcd_hist2 = get_qcd("shape2", qcd_shape_files).Clone(randomize("qcd"))
                qcd_hist.Scale(1. / qcd_hist.Integral())
                qcd_hist2.Scale(1. / qcd_hist2.Integral())
                qcd_hist.Add(qcd_hist2)
                qcd_hist.Scale(0.5)

            n_os_inviso, n_os_inviso_error, n_os_inviso_compatible = get_integral_and_error(
                get_qcd("os_inviso", qcd_shape_files, -999))
            n_ss_iso, n_ss_iso_error, n_ss_iso_compatible = get_integral_and_error(
                get_qcd("ss_iso", qcd_shape_files, -999))
            n_ss_inviso, n_ss_inviso_error, n_ss_inviso_compatible = get_integral_and_error(
                get_qcd("ss_inviso", qcd_shape_files, -999))
            # if not n_ss_iso or not n_ss_inviso:
            if n_os_inviso_compatible or n_ss_iso_compatible or n_ss_inviso_compatible:
                print("****WARNING: QCD normalization failed (negative yield), Removing QCD!")
                qcd_scaling = 0.
                qcd_inviso = 0.
                qcd_inviso_error = 0.
                qcd_hist.Scale(qcd_scaling)
            else:
                qcd_inviso = n_os_inviso / n_ss_inviso
                qcd_inviso_error = qcd_inviso * math.sqrt(
                    (n_os_inviso_error / n_os_inviso) * (n_os_inviso_error / n_os_inviso)
                    + (n_ss_inviso_error / n_ss_inviso) * (n_ss_inviso_error / n_ss_inviso)
                )
                qcd_scaling = n_os_inviso * n_ss_iso / n_ss_inviso
                os_inviso_rel_error = n_os_inviso_error / n_os_inviso
                ss_iso_rel_error = n_ss_iso_error / n_ss_iso
                ss_inviso_rel_error = n_ss_inviso_error / n_ss_inviso
                new_errors_sq = {}
                for ibinx, ibiny in itertools.product(
                        range(1, qcd_hist.GetNbinsX() + 1),
                        range(1, qcd_hist.GetNbinsY() + 1)):
                    if qcd_hist.GetBinContent(ibinx, ibiny) > 0:
                        bin_rel_error = (qcd_hist.GetBinError(ibinx, ibiny)
                            / qcd_hist.GetBinContent(ibinx, ibiny))
                    else:
                        bin_rel_error = 0
                    new_errors_sq[(ibinx, ibiny)] = (bin_rel_error * bin_rel_error
                        + os_inviso_rel_error * os_inviso_rel_error
                        + ss_iso_rel_error * ss_iso_rel_error
                        + ss_inviso_rel_error * ss_inviso_rel_error)
                qcd_hist.Scale(qcd_scaling)
                # fix errors
                for ibinx, ibiny in itertools.product(
                        range(1, qcd_hist.GetNbinsX() + 1),
                        range(1, qcd_hist.GetNbinsY() + 1)):
                    qcd_hist.SetBinError(ibinx, ibiny, qcd_hist.GetBinContent(ibinx, ibiny)
                        * math.sqrt(new_errors_sq[(ibinx, ibiny)]))

            # store and style
            yield_error = c_double(0.)
            qcd_hist.cmt_yield = qcd_hist.IntegralAndError(
                0, qcd_hist.GetNbinsX() + 1, 0, qcd_hist.GetNbinsY() + 1, yield_error)
            qcd_hist.cmt_yield_error = yield_error.value
            qcd_hist.cmt_bin_yield = []
            qcd_hist.cmt_bin_yield_error = []
            for ibiny in range(1, qcd_hist.GetNbinsY() + 1):
                qcd_hist.cmt_bin_yield.append([
                    qcd_hist.GetBinContent(ibinx, ibiny)
                    for ibinx in range(1, qcd_hist.GetNbinsX() + 1)
                ])
                qcd_hist.cmt_bin_yield_error.append([
                    qcd_hist.GetBinError(ibinx, ibiny)
                    for ibinx in range(1, qcd_hist.GetNbinsX() + 1)
                ])
            qcd_hist.cmt_scale = 1.
            qcd_hist.cmt_process_name = "qcd"
            qcd_hist.process_label = "QCD"
            qcd_hist.SetTitle("QCD")
            background_hists.append(qcd_hist)
            all_hists.append(qcd_hist)

        if not self.hide_data:
            all_hists += data_hists

        bkg_histo = None
        if not self.stack:
            for hist in all_hists:
                scale = 1. / (hist.Integral() or 1.)
                hist.Scale(scale)
                hist.scale = scale
            draw_hists = all_hists
        else:
            draw_hists = all_hists
            for hist in background_hists:
                if not bkg_histo:
                    bkg_histo = hist.Clone()
                else:
                    bkg_histo.Add(hist.Clone())
            if not self.hide_data:
                draw_hists.extend(data_hists)

        dummy_hist = ROOT.TH2F(randomize("dummy"), hist_title, *binning_args)
        r.setup_hist(dummy_hist)
        dummy_hist.GetZaxis().SetMaxDigits(4)
        dummy_hist.GetZaxis().SetTitleOffset(1.22)  # FIXME

        if self.log_z:
            dummy_hist.SetMinimum(0.0011)
        else:
            dummy_hist.SetMinimum(0.001)

        inner_text=[self.category.label + " category"]
        if self.region:
            if isinstance(self.region.label, list):
                inner_text += self.region.label
            else:
                inner_text.append(self.region.label)

        if not (self.hide_data or not len(data_hists) > 0) or self.stack:
            ### Old implementation ###
            #upper_right="{}, {} TeV ({:.1f} ".format(
            #    self.config.year,
            #    self.config.ecm,
            #    self.config.lumi_fb
            #) + "fb^{-1})"
            ### New implementation: NOT TESTED ###
            if not type(self.config.lumi_fb) == dict:
                upper_right="{} , {:.1f} ".format(
                    self.config.year,
                    self.config.lumi_fb,
                ) + "fb^{-1} " + "({} TeV)".format(self.config.ecm)
            else:
                if self.run_era != "":
                    era_label = self.run_era
                    lumi_label = self.config.lumi_fb[self.config.get_run_period_from_run_era(self.run_era)][self.run_era]
                else:
                    era_label = self.run_period
                    if self.run_period != "":
                        lumi_label = sum(self.config.lumi_pb.get(self.run_period, {}).values())
                    else:
                        lumi_label = sum(sum(period_dict.values()) for period_dict in self.config.lumi_pb.values())
                upper_right="{} {}, {:.1f} ".format(
                    self.config.year,
                    era_label,
                    lumi_label,
                ) + "fb^{-1} " + "({} TeV)".format(self.config.ecm)
        else:
            upper_right="{} Simulation ({} TeV)".format(
                self.config.year,
                self.config.ecm,
            )

        draw_labels = get_labels(
            upper_right=upper_right,
            inner_text=inner_text
        )

        for ih, hist in enumerate(draw_hists):
            c = Canvas()
            if self.log_z:
                c.SetLogz()
            dummy_hist.SetMaximum(hist.GetMaximum())
            dummy_hist.Draw()
            hist.Draw("COLZ,SAME")
            for label in draw_labels:
                label.Draw("same")

            outputs = []
            if self.save_png:
                outputs.append(self.output()["png"].targets[feature_pair_name][hist.cmt_process_name].path)
            if self.save_pdf:
                outputs.append(self.output()["pdf"].targets[feature_pair_name][hist.cmt_process_name].path)
            with root_setIgnoreLevel(ROOT.kWarning):# silence canvas print messages
                for output in outputs:
                    c.SaveAs(create_file_dir(output))

        f = ROOT.TFile.Open(create_file_dir(
            self.output()["root"].targets[feature_pair_name].path), "RECREATE")
        f.cd()
        # c.Write("canvas") #FIXME

        hist_dir = f.mkdir("histograms")
        hist_dir.cd()
        for hist in all_hists:
            hist.Write(hist.cmt_process_name)
        if bkg_histo:
            bkg_histo.Write("background")

        if self.store_systematics:
            for syst_dir, shape_hists in self.histos["shape"].items():
                for hist in shape_hists:
                    hist.Write("%s_%s" % (hist.cmt_process_name, syst_dir))
        f.Close()

        yields = OrderedDict()
        for hist in signal_hists + background_hists + data_hists:
            yields[hist.cmt_process_name] = {
                "Total yield": hist.cmt_yield,
                "Total yield error": hist.cmt_yield_error,
                "Entries": getattr(hist, "cmt_entries", hist.GetEntries()),
                "Binned yield": hist.cmt_bin_yield,
                "Binned yield error": hist.cmt_bin_yield_error,
            }
        if bkg_histo:
            yields["background"] = {
                "Total yield": sum([hist.cmt_yield for hist in background_hists]),
                "Total yield error": math.sqrt(sum([(hist.cmt_yield_error)**2 for hist in background_hists])),
                "Entries": getattr(bkg_histo, "cmt_entries", bkg_histo.GetEntries()),
                "Binned yield": [[
                    sum([hist.cmt_bin_yield[j][i] for hist in background_hists])
                    for i in range(0, background_hists[0].GetNbinsX())]
                    for j in range(0, background_hists[0].GetNbinsY())],
                "Binned yield error": [[
                    math.sqrt(sum([(hist.cmt_bin_yield_error[j][i]) ** 2
                        for hist in background_hists]))
                    for i in range(0, background_hists[0].GetNbinsX())]
                    for j in range(0, background_hists[0].GetNbinsY())],
            }
        with open(create_file_dir(self.output()["yields"].targets[feature_pair_name].path), "w") as f:
            json.dump(yields, f, indent=4)

    @law.decorator.notify
    @law.decorator.safe_output
    def run(self):
        """
        Splits processes into data, signal and background. Creates histograms from each process
        loading them from the input files. Scales the histograms and applies the correct format
        to them.
        """

        ROOT = import_root()
        ROOT.gStyle.SetOptStat(0)

        # create root tchains for inputs
        inputs = self.input()

        self.data_names = [p.name for p in self.processes_datasets.keys() if p.isData]
        self.signal_names = [p.name for p in self.processes_datasets.keys() if p.isSignal]
        self.background_names = [p.name for p in self.processes_datasets.keys()
            if not p.isData and not p.isSignal]

        self.nevents, self.nweightedevents, self.nunweightedevents = self.get_nevents()

        for feature_pair in self.features:
            self.histos = {"background": [], "signal": [], "data": [], "all": []}

            binning_args, _ = self.get_binning(feature_pair)
            x_title = (str(feature_pair[0].get_aux("x_title"))
                + (" [%s]" % feature_pair[0].get_aux("units")
                if feature_pair[0].get_aux("units") else ""))
            y_title = (str(feature_pair[1].get_aux("x_title"))
                + (" [%s]" % feature_pair[1].get_aux("units")
                if feature_pair[1].get_aux("units") else ""))
            z_title = ("Events" if self.stack else "Normalized Events")
            hist_title = "; %s; %s: %s" % (x_title, y_title, z_title)

            systs_directions = [("central", "")]
            if self.plot_systematics:
                self.histos["bkg_histo_syst"] = ROOT.TH2D(
                    randomize("syst"), hist_title, *binning_args)
            if self.store_systematics:
                self.histos["shape"] = {}
                shape_systematics =list(set(
                    get_systs_plotting(self.config, feature_pair[0], isMC=True, category=self.category, region=self.region) 
                    + get_systs_plotting(self.config, feature_pair[0], isMC=True, category=self.category, region=self.region) 
                ))

                systs_directions += list(itertools.product(shape_systematics, directions))

            feature_pair_name = "_".join([feature.name for feature in feature_pair])
            for (syst, d) in systs_directions:
                feature_name = feature_pair_name if syst == "central" else "%s_%s_%s" % (
                    feature_pair_name, syst, d)
                if syst != "central":
                    self.histos["shape"]["%s_%s" % (syst, d)] = []
                for iproc, (process, datasets) in enumerate(self.processes_datasets.items()):
                    if syst != "central" and process.isData:
                        continue
                    process_histo = ROOT.TH2D(randomize(process.name), hist_title, *binning_args)
                    process_histo.process_label = str(process.label)
                    process_histo.cmt_process_name = process.name
                    process_histo.Sumw2()
                    for dataset in datasets:
                        dataset_histo = ROOT.TH2D(randomize("tmp"), hist_title, *binning_args)
                        dataset_histo.Sumw2()
                        for category in self.expand_category():
                            try:
                                inp = flatten(inputs["data"][(dataset.name, category.name)].collection.targets)
                            except AttributeError: # in case we use --branch, we get a LocalFileTarget rather than a SiblingFileCollection
                                inp = inputs["data"][(dataset.name, category.name)]
                            for elem in inp:
                                rootfile = ROOT.TFile.Open(elem.path)
                                histo_in_file = rootfile.Get(feature_name)
                                if histo_in_file == None: # checking histo_in_file for nullptr
                                    raise RuntimeError(f"Could not find feature {feature_name} in file {elem.path}. Perhaps a systematic was added after running PrePlot/Categoriation ?")
                                histo = copy(histo_in_file)
                                rootfile.Close()
                                dataset_histo.Add(histo)
                            if not process.isData and not self.avoid_normalization:
                                elem = ("central"
                                    if syst == "central" or syst not in self.norm_syst_list
                                    else f"{syst}_{d}")
                                if self.nevents[dataset.name][elem] != 0:
                                    dataset_histo.Scale(
                                        self.get_normalization_factor(dataset, elem))
                                    scaling = dataset.get_aux("scaling", None)
                                    if scaling:
                                        print(" ### Scaling {} histo by {} +- {}".format(
                                            dataset.name, scaling[0], scaling[1]))
                                        old_errors = [[dataset_histo.GetBinError(ibinx, ibiny)\
                                                / dataset_histo.GetBinContent(ibinx, ibiny)
                                                if dataset_histo.GetBinContent(ibinx, ibiny) != 0
                                                else 0
                                                for ibinx in range(1, dataset_histo.GetNbinsX() + 1)]
                                            for ibiny in range(1, dataset_histo.GetNbinsY() + 1)
                                        ]
                                        new_errors = [[
                                                math.sqrt(elem ** 2 + (scaling[1] / scaling[0]) ** 2)
                                                for elem in line]
                                            for line in old_errors]
                                        dataset_histo.Scale(scaling[0])
                                        for ibinx, ibiny in itertools.product(
                                                range(1, dataset_histo.GetNbinsX() + 1),
                                                range(1, dataset_histo.GetNbinsY() + 1)):
                                            dataset_histo.SetBinError(
                                                ibinx, ibiny, dataset_histo.GetBinContent(ibinx, ibiny)
                                                    * new_errors[ibiny - 1][ibinx - 1])

                        process_histo.Add(dataset_histo)
                        if self.plot_systematics and not process.isData and not process.isSignal \
                                and syst == "central":
                            dataset_histo_syst = dataset_histo.Clone()
                            self.histos["bkg_histo_syst"].Add(dataset_histo_syst)

                    yield_error = c_double(0.)
                    process_histo.cmt_yield = process_histo.IntegralAndError(
                        0, process_histo.GetNbinsX() + 1,
                        0, process_histo.GetNbinsY() + 1, yield_error)
                    process_histo.cmt_yield_error = yield_error.value

                    process_histo.cmt_bin_yield = []
                    process_histo.cmt_bin_yield_error = []
                    for ibiny in range(1, process_histo.GetNbinsY() + 1):
                        process_histo.cmt_bin_yield.append(
                            [process_histo.GetBinContent(ibinx, ibiny)
                            for ibinx in range(1, process_histo.GetNbinsX() + 1)]
                        )
                        process_histo.cmt_bin_yield_error.append(
                            [process_histo.GetBinError(ibinx, ibiny)
                            for ibinx in range(1, process_histo.GetNbinsX() + 1)]
                        )

                    if syst == "central":
                        if process.isSignal:
                            self.histos["signal"].append(process_histo)
                        elif process.isData:
                            self.histos["data"].append(process_histo)
                        else:
                            self.histos["background"].append(process_histo)
                        if not process.isData: #or not self.hide_data:
                           self.histos["all"].append(process_histo)
                    else:
                        self.histos["shape"]["%s_%s" % (syst, d)].append(process_histo)

            self.plot(feature_pair)
