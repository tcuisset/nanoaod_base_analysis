EGM
===

Installation
------------

``git clone https://gitlab.cern.ch/cms-phys-ciemat/egm-corrections.git Corrections/EGM``

RDFModules
----------

.. toctree::
    :maxdepth: 3

    electron 
