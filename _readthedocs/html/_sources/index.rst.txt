Welcome to nanoaod-base-analysis's documentation!
=================================================

This code aims to process NanoAOD datasets, allowing to generate different root files, histograms and plots with the desired selection of events, variables and branches. 

Developed by the CIEMAT CMS group. It's a Python and RDataFrame based framework that uses `law <https://github.com/riga/law>`_ to perform multiple CMS analyses.

Therefore, it is only required to create a configuration with specific settings of your analysis to start using the code with law commands.



.. note::

   This project is under active development.

Contents
--------

.. toctree::
   :maxdepth: 3

   Structure
   Installation
   Configuration
   Environment
   Tasks
   api/index
   modules/index
   faq/index
